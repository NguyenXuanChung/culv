import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { ChangePassComponent } from './views/change-pass/change-pass.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'info',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'admin',
        loadChildren: () => import('./views/admin/admin.module').then(m => m.AdminModule)
      },
      {
        path: 'vb',
        loadChildren: () => import('./views/vb/vb.module').then(m => m.VbModule)
      },
      {
        path: 'thongtinbh',
        loadChildren: () => import('./views/ttbh/ttbh.module').then(m => m.TtbhModule)
      },
      {
        path: 'luong',
        loadChildren: () => import('./views/luong/luong.module').then(m => m.LuongModule)
      },
      {
        path: 'dangkynghi',
        loadChildren: () => import('./views/dangKyNghi/dangKyNghi.module').then(m => m.DangKyNghiModule)
      },
      {
        path: 'pheduyetchamcong',
        loadChildren: () => import('./views/pdcc/pdcc.module').then(m => m.PdccModule)
      },
      {
        path: 'chamcong',
        loadChildren: () => import('./views/cc/cc.module').then(m => m.CcModule)
      },
      {
        path: 'base',
        loadChildren: () => import('./views/base/base.module').then(m => m.BaseModule)
      },
      {
        path: 'buttons',
        loadChildren: () => import('./views/buttons/buttons.module').then(m => m.ButtonsModule)
      },
      {
        path: 'info',
        loadChildren: () => import('./views/info/info.module').then(m => m.InfoModule)
      },
      {
        path: 'quytrinh',
        loadChildren: () => import('./views/qt/qt.module').then(m => m.QtModule)
      },
      {
        path: 'charts',
        loadChildren: () => import('./views/chartjs/chartjs.module').then(m => m.ChartJSModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'icons',
        loadChildren: () => import('./views/icons/icons.module').then(m => m.IconsModule)
      },
      {
        path: 'notifications',
        loadChildren: () => import('./views/notifications/notifications.module').then(m => m.NotificationsModule)
      },
      {
        path: 'theme',
        loadChildren: () => import('./views/theme/theme.module').then(m => m.ThemeModule)
      },
      {
        path: 'widgets',
        loadChildren: () => import('./views/widgets/widgets.module').then(m => m.WidgetsModule)
      },
      {
        path: 'change-pass',
        loadChildren: () => import('./views/change-pass/change-pass.module').then(m => m.ChangePassModule)
      }
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
