// import { DonViService } from './../../../services/don-vi.service';
// import { TuyenQuanService } from './../../../services/tq.service';
// import { Component, OnInit, ViewChild } from '@angular/core';
// import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
// import { MatTableDataSource } from '@angular/material/table';
// import { MatPaginator } from '@angular/material/paginator';
// import { MatSort } from '@angular/material/sort';
// import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';

// import * as moment from 'moment';
// import { map, startWith, isEmpty } from 'rxjs/operators';

// // Translate
// import { CommonService } from './../../../services/common.service';
// import { StorageService } from '../../../services/storage.service';
// import { AuthConstants } from '../../../config/auth-constants';
// import { Router, ActivatedRoute } from '@angular/router';
// import { CheckableSettings, CheckedState } from '@progress/kendo-angular-treeview';
// import { Observable, of } from 'rxjs';
// import * as dateFormat from 'dateformat';
// import { TqDqtvService } from '../../../services/tq-dqtv.service';
// import { DqtvService } from '../../../services/dqtv.service';
// import { DialogUpdateCongDanComponent } from '../../tq/congdan/dialog-update-congdan.component';
// import { KtService } from '../../../services/kt.service';
// import { DialogUpdateDsCaNhanComponent } from './dialog-update-dsCaNhan.component';

// // tslint:disable-next-line: class-name
// interface select {
//   value: number;
//   display: string;
// }
// @Component({
//   selector: 'app-themdscanhan',
//   templateUrl: './themCaNhan.component.html'
// })
// export class ThemCaNhanComponent implements OnInit {
//   displayedColumns = ['index', 'hoTen', 'cmnd', 'ngaySinh', 'id_capBac', 'id_donViQuanLy', 'actions'];
//   dataSource: MatTableDataSource<any>;
//   typeStateCongDans: any[];
//   phanLoaiLucLuongs: any[];
//   lucLuongs: any[] = [];
//   selectedRow: boolean;
//   @ViewChild(MatPaginator) paginator: MatPaginator;
//   @ViewChild(MatSort) sort: MatSort;
//   searchForm: FormGroup;
//   permission: any;
//   checkArray: any[] = [];
//   donVis: any[] = [];
//   donViTvs: any[] = [];
//   public id_donViTvs: any[] = [];
//   public id_donVis: any[] = [];
//   public id_donViPers: any[] = [];
//   public disabledKeys: any[] = [];
//   expandedKeys: any[] = [];

//   get fs() { return this.searchForm.controls; }
//   form: FormGroup;
//   obj: any = [];
//   id_dotKhenThuong: string = null;
//   constructor(
//     private common: CommonService,
//     private fb: FormBuilder,
//     private tuyenQuanService: TuyenQuanService,
//     public dialog: MatDialog,
//     private router: Router,
//     private route: ActivatedRoute,
//     private donVi: DonViService,
//     private storageService: StorageService,
//     private dmTqService: TqDqtvService,
//     private ktService: KtService
//   ) {
//     this.storageService.get(AuthConstants.PERMISSION).then(res => {
//       this.permission = res;
//     });
//     this.storageService.get(AuthConstants.AUTH).then(res => {
//       res.id_donVis.split(',').forEach(element => {
//         this.id_donViPers.push(parseInt(element, 10));
//         this.id_donVis.push(parseInt(element, 10));
//       });
//     });
//     this.route.paramMap.subscribe(params => {
//       if (params.get('id')) {
//         this.id_dotKhenThuong = params.get('id');
//         this.createSearchForm();
//         setTimeout(() => {
//           this.getDotKhenThuong();
//         }, 100);
//       }
//     });

//   }

//   ngOnInit() {
//     if (!this.permission.KT._view) {
//       this.router.navigate(['/']);
//     }
//     this.loadCombo();
//   }

//   createSearchForm() {
//     const date = new Date();
//     const today = moment().format('YYYY-MM-DD');
//     const firstDay = dateFormat(new Date(date.getFullYear(), 0, 1), 'yyyy-mm-dd');
//     const lastDay = dateFormat(new Date(date.getFullYear(), 11, 31), 'yyyy-mm-dd');
//     this.searchForm = this.fb.group(
//       {
//         id_dotKhenThuong: this.id_dotKhenThuong,
//         get: 0,
//         id_donViQuanLys: null,
//         id_type_state: null,
//         id_donViTvs: null,
//         content: '',
//         ngay: [today, [Validators.required]],
//       });
//   }

//   onSearch() {
//     const id_donVis = [];
//     this.id_donViPers.forEach((element, i) => {
//       const index: number = this.id_donVis.indexOf(element);
//       if (index !== -1) {
//         id_donVis.push(element);
//       }
//     });
//     this.id_donVis = id_donVis;
//     if (this.id_donVis.length === 0) {
//       this.id_donVis = this.id_donViPers;
//     }
//     this.fs.id_donViQuanLys.setValue(this.id_donVis.toString());

//     this.fs.id_donViTvs.setValue(this.id_donViTvs.toString());
//     this.getCongDan();
//   }

//   getCongDan() {
//     this.obj = [];
//     this.ktService.getDsCaNhan(this.searchForm.value, this.common.KT.View).subscribe(res => {
//       if (res.error) {
//         this.dataSource = new MatTableDataSource();
//       } else {
//         res.data.forEach(element => {
//           if (element.latest === 1) {
//             this.obj.push({ checked: false, ...element });
//           } else {
//             this.obj.push(element);
//           }
//         });
//         this.dataSource = new MatTableDataSource(this.obj);
//         this.dataSource.paginator = this.paginator;
//         this.dataSource.sort = this.sort;
//       }
//     });
//   }

//   openDetailDialog(data: any): void {
//     // const dialogRef = this.dialog.open(DialogDetailGiamSatComponent, {
//     //   height: 'auto',
//     //   width: '70%',
//     //   data: data
//     // });
//     // dialogRef.afterClosed().subscribe(result => {
//     // });
//   }
//   openUpdateDialog(data: any): void {
//     const dialogRef = this.dialog.open(DialogUpdateCongDanComponent, {
//       height: '80%',
//       width: '70%',
//       data: data
//     });
//     dialogRef.afterClosed().subscribe(result => {
//       this.onSearch();
//       if (result) {
//         this.common.messageRes(result);
//       }
//     });
//   }
//   openDeleteDialog(data: any): void {
//     const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
//       width: '350px',
//       data: { delete: 1, name: data.hoTen }
//     });
//     dialogRef.afterClosed().subscribe(result => {
//       if (result) {
//         console.log('Yes clicked');
//         this.tuyenQuanService.deleteCongDan({ id: data.id }, this.common.KT.Delete)
//           .subscribe(res => {
//             if (res.error) {
//               this.common.messageErr(res);
//             } else {
//               this.onSearch();
//               this.common.messageRes(res.message);
//             }
//           });
//       }
//     });
//   }
//   OpenDialogUpdateDs() {
//     this.checkArray = [];
//     this.obj.forEach(element => {
//       if (element.checked === true) {
//         this.checkArray.push(element.id_congDan);
//       }
//     });
//     if (this.checkArray.length === 0) {
//       this.common.messageErr({ error: { message: 'tq.validate.chonCongDan' } });
//       return;
//     }
//     const dialogRef = this.dialog.open(DialogUpdateDsCaNhanComponent, {
//       width: '350px',
//       data: { id_dotKhenThuong: this.id_dotKhenThuong, id_congDans: this.checkArray.toString() }
//     });
//     dialogRef.afterClosed().subscribe(result => {
//       if (result) {
//         this.onSearch();
//         this.common.messageRes(result);
//       }
//     });
//   }
//   getDotKhenThuong() {
//     this.ktService.getDotKhenThuong({ id: this.id_dotKhenThuong }, this.common.KT.View).subscribe(res => {
//       if (res.error) {
//         this.router.navigate(['/kt/dotkhenthuong']);
//         this.common.messageErr(res);
//       } else {
//         this.onSearch();
//       }
//     });
//   }
//   checkAll(data: any) {
//     if (data === true) {
//       this.obj.forEach(element => {
//         if (element.checked === false) {
//           element.checked = true;
//         }
//       });
//     } else {
//       this.obj.forEach(element => {
//         if (element.checked === true) {
//           element.checked = false;
//         }
//       });
//     }
//   }

//   loadCombo() {
//     this.dmTqService.getDonViTv({}, this.common.KT.View).subscribe(res => {
//       if (!res.error) {
//         this.donViTvs = res.data;
//       }
//     });
//     const today = moment().format('YYYY/MM/DD');
//     this.donVi.getTreeToValue({ id_trungTam: 1, tuNgay: today, denNgay: today }, this.common.KT.View)
//       .subscribe((res: any) => {
//         if (res.error) {
//           this.common.messageErr(res);
//         } else {
//           this.donVis = res.data;
//           this.getAllParentTextProperties(this.donVis, this.donVis[0]);
//         }
//       });
//   }
//   public get checkableSettings(): CheckableSettings {
//     return {
//       checkChildren: true,
//       checkParents: false,
//       enabled: true,
//       mode: 'multiple',
//       checkOnClick: true
//     };
//   }
//   getAllParentTextProperties(items: Array<any>, itemParent: any) {
//     items.forEach(i => {
//       if (i.hasPermission || i.id_capTren === 0) {
//         itemParent.hasChildPermission = 1;
//         const index: number = this.disabledKeys.indexOf(itemParent.data);
//         if (index !== -1) {
//           this.disabledKeys.splice(index, 1);
//         }
//         this.expandedKeys.push(itemParent.data);
//       } else if (!i.hasPermission) {
//         this.disabledKeys.push(i.data);
//       }
//       if (i.children.length) {
//         this.getAllParentTextProperties(i.children, i);
//       }
//     });
//   }
//   public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
//   public hasChildren(dataItem: any): boolean {
//     return dataItem.children && dataItem.children.length > 0;
//   }
//   onSelectedRow(row: any) {
//     if (!this.selectedRow) {
//       this.selectedRow = row;
//     } else {
//       this.selectedRow = row;
//     }
//   }
// }
