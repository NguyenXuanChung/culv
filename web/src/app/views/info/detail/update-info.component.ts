import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
// Translate
import { CommonService } from './../../../services/common.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';
import { KtService } from '../../../services/kt.service';


@Component({
  selector: 'app-update-info',
  templateUrl: './update-info.component.html',
})
export class UpdateInfoComponent implements OnInit {
  submitted = false;
  isInputDate = false;
  // convenience getter for easy access to form fields
  tqForm: FormGroup;
  numbers: number[];
  capQuyetDinhs: any[];
  phanLoais: any[];
  danhHieuHinhThucs: any[];

  // convenience getter for easy access to form fields
  get f() { return this.tqForm.controls; }

  constructor(
    private common: CommonService,
    private ktService: KtService,
    private fb: FormBuilder,
  ) {
    const date = new Date();
    this.numbers = Array(100).fill(0).map((x, i) => (i + date.getFullYear() - 95));
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }

  createForm() {
    const date = new Date();
    this.tqForm = this.fb.group(
      {
        nam: [date.getFullYear(), [Validators.required]],
        name: [null, [Validators.required]],
        id_phanLoai: [null, [Validators.required]],
        typeKhenThuong: [null, [Validators.required]],
        id_capQuyetDinh: [null],
        id_danhHieuHinhThuc: [null],
        ngayLapDs: [null],
        soQd: [null],
        ngayQd: [null],
        ghiChu: [null],
      });
  }
  onSubmit() {
    if (this.f.ngayLapDs.value === '') {
      this.f.ngayLapDs.setValue(null);
    }
    if (this.f.ngayQd.value === '') {
      this.f.ngayQd.setValue(null);
    }
    this.submitted = true;
    // stop here if form is invalid
    if (this.tqForm.invalid) {
      return;
    }
    // if (!this.data) {
    //   this.ktService.insertDotKhenThuong(this.tqForm.value, this.common.KT.Update)
    //     .subscribe(res => {
    //       if (res.error) {
    //         this.common.messageErr(res);
    //       } else {
    //         // this.dialogRef.close(res.message);
    //       }
    //     });
    // } else {
    //   // this.ktService.editDotKhenThuong({
    //   //   data: this.tqForm.value,
    //   //   condition: { id: this.data.id }
    //   // }, this.common.KT.Update)
    //   //   .subscribe(res => {
    //   //     if (res.error) {
    //   //       this.common.messageErr(res);
    //   //     } else {
    //   //       this.dialogRef.close(res.message);
    //   //     }
    //   //   });
    // }
  }
  loadCombo() {
    this.ktService.getCapQuyetDinh({}, this.common.KT.View).subscribe(res => {
      if (!res.error) {
        this.capQuyetDinhs = res.data;
      }
    });
    this.ktService.getPhanLoai({}, this.common.KT.View).subscribe(res => {
      if (!res.error) {
        this.phanLoais = res.data;
      }
    });
  }
  onChangeHinhThuc() {
    this.ktService.getDanhHieuHinhThuc({
      id_phanLoai: this.f.id_phanLoai.value,
      typeKhenThuong: this.f.typeKhenThuong.value,
      id_capQuyetDinh: this.f.id_capQuyetDinh.value,
    }, this.common.KT.View).subscribe(res => {
      if (!res.error) {
        this.danhHieuHinhThucs = res.data;
      }
    });
  }
}
