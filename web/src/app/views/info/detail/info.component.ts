import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { CheckableSettings, CheckedState } from '@progress/kendo-angular-treeview';

import * as moment from 'moment';
import * as dateFormat from 'dateformat';
import { environment } from './../../../../environments/environment';

// Translate
import { CommonService } from '../../../services/common.service';
import { Observable, of } from 'rxjs';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { TuyenQuanService } from '../../../services/tq.service';
import { DqtvService } from '../../../services/dqtv.service';
import { KtService } from '../../../services/kt.service';
import { AuthService } from '../../../services/auth.service';
import { InfoService } from '../../../services/info.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html'
})
export class InfoComponent implements OnInit {
  displayedCongTacs = ['index', 'tuNgay', 'denNgay', 'donViCongTac', 'chucDanh', 'chucVu', 'tenLoaiCongTac'];
  dataCongTac: MatTableDataSource<any>;
  displayedHopDongs = ['index', 'soHopDong', 'tuNgay', 'denNgay', 'ngayHieuLuc', 'chucDanh', 'action'];
  dataHopDong: MatTableDataSource<any>;
  displayedGiamTrus = ['index', 'hoTen', 'ngaySinh', 'maSoThue', 'cmnd', 'quanHe', 'tuThang', 'denThang'];
  dataGiamTru: MatTableDataSource<any>;
  displayedKhenThuongs = ['index', 'loaiKhenThuong', 'khenThuong', 'nam', 'soQuyetDinh', 'ngayRaQuyetDinh', 'ghiChu'];
  dataKhenThuong: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  searchForm: FormGroup;
  permission: any;
  numbers: number[];
  phanLoais: any[];
  info: any;
  get fs() { return this.searchForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private infoService: InfoService,
    private authService: AuthService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
    // this.storageService.get(AuthConstants.AUTH).then(res => {
    //   this.info = res;
    // });
  }

  ngOnInit() {
    this.loadCombo();
    this.createSearchForm();
    this.onSearch();
  }

  createSearchForm() {
    const date = new Date();
    this.searchForm = this.fb.group(
      {
        nam: [date.getFullYear()],
        typeKhenThuong: [null],
        id_phanLoai: [null],
      });
  }

  onSearch() {
    this.getInfo();
  }
  getInfo() {
    this.authService.getUserInfo(this.common.Portal_Info.View)
    .subscribe(res => {
      if (res.error) {
        // this.common.messageErr(res);
        // this.dataSource = new MatTableDataSource();
      } else {
       this.info = res.userInfo;
      }
    });
  }
  loadCombo() {
    this.infoService.getGiamTruGiaCanh({}, this.common.Portal_Info.View).subscribe(res => {
      if (!res.error) {
        console.log(res.data);
        this.dataGiamTru = new MatTableDataSource(res.data);
        this.dataGiamTru.paginator = this.paginator;
        this.dataGiamTru.sort = this.sort;
      }
    });
    this.infoService.getQuaTrinhCongTac({}, this.common.Portal_Info.View).subscribe(res => {
      if (!res.error) {
        console.log(res.data);
        this.dataCongTac = new MatTableDataSource(res.data);
        this.dataCongTac.paginator = this.paginator;
        this.dataCongTac.sort = this.sort;
      }
    });
    this.infoService.getQuaTrinhHopDong({}, this.common.Portal_Info.View).subscribe(res => {
      if (!res.error) {
        console.log(res.data);
        this.dataHopDong = new MatTableDataSource(res.data);
        this.dataHopDong.paginator = this.paginator;
        this.dataHopDong.sort = this.sort;
      }
    });
    this.infoService.getKhenThuongKyLuat({}, this.common.Portal_Info.View).subscribe(res => {
      if (!res.error) {
        console.log(res.data);
        this.dataKhenThuong = new MatTableDataSource(res.data);
        this.dataKhenThuong.paginator = this.paginator;
        this.dataKhenThuong.sort = this.sort;
      }
    });
  }
  openUpdateDialog(data: any): void {
    // const dialogRef = this.dialog.open(DialogUpdateDotKhenThuongComponent, {
    //   width: '700px',
    //   data: data
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.onSearch();
    //     this.common.messageRes(result);
    //   }
    // });
  }
  openDeleteDialog(data: any): void {
    // const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
    //   width: '350px',
    //   data: { delete: 1, name: data.name }
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     console.log('Yes clicked');
    //     this.ktService.deleteDotKhenThuong({ id: data.id },
    //       this.common.KT.Delete).subscribe(res => {
    //         if (res.error) {
    //           this.common.messageErr(res);
    //         } else {
    //           this.onSearch();
    //           this.common.messageRes(res.message);
    //         }
    //       });
    //   }
    // });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
  getUrlImg(data) {
    let url;
    if (!data.AnhCaNhan) {
      url = 'assets/img/avatars/NoAvatar.jpg';
    } else {
      url = `${environment.apiUrl}files/canbo/${data.CanBo_ID}/${data.AnhCaNhan}`;
    }
    return url;
  }
}
