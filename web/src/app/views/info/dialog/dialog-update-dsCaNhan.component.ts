import { KtService } from './../../../services/kt.service';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';


@Component({
  selector: 'app-dialog-update-dscanhan',
  templateUrl: './dialog-update-dsCaNhan.component.html',
})
export class DialogUpdateDsCaNhanComponent implements OnInit {
  submitted = false;
  isInputDate = false;
  // convenience getter for easy access to form fields

  tqForm: FormGroup;
  congDans: any[] = [];
  typeStateCongDans: any[];
  // convenience getter for easy access to form fields
  get f() { return this.tqForm.controls; }

  constructor(
    private common: CommonService,
    private ktService: KtService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogUpdateDsCaNhanComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.tqForm = this.fb.group(
      {
        id_congDans: [this.data.id_congDans],
        id_dotKhenThuong: [this.data.id_dotKhenThuong],
      });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.tqForm.invalid) {
      return;
    }
    this.ktService.updateDs(this.tqForm.value
      , this.common.KT.Update)
      .subscribe(res => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.dialogRef.close(res.message);
        }
      });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
