import { AppModule } from './../../app.module';
import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe, DecimalPipe } from '@angular/common';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { AngularMaterial } from '../../angular-material';
import { MomentModule } from 'ngx-moment';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { EditorModule } from 'primeng/editor';
import { QuillModule } from 'ngx-quill';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MatTabsModule } from '@angular/material/tabs';
import { TreeModule } from 'primeng/tree';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { QtRoutingModule } from './qt-routing.module';
import { MatMenuModule } from '@angular/material/menu';
import { BoNhiemTdvComponent } from './bonhiem/boNhiemTdv.component';
import { UpdateBoNhiemTdvComponent } from './bonhiem/update-boNhiemTdv.component';
import { StateActionComponent } from '../shared/state-action/state-action.component';
import { DialogDetailBoNhiemComponent } from './bonhiem/dialog-detail-boNhiem.component';
import { UsdOnlyDirective } from '../../services/usd-only.directive';
export function translateHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    BoNhiemTdvComponent,
    UpdateBoNhiemTdvComponent,
    StateActionComponent,
    DialogDetailBoNhiemComponent,
    UsdOnlyDirective
  ],
  entryComponents: [
    StateActionComponent,
    DialogDetailBoNhiemComponent,
  ],
  imports: [
    CommonModule,
    QtRoutingModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: translateHttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    AngularMaterial,
    MomentModule,
    InputTextareaModule,
    EditorModule,
    QuillModule.forRoot({
      modules: {
        syntax: false,
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'],
          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
          [{ 'list': 'ordered' }, { 'list': 'bullet' }],
          [{ 'indent': '-1' }, { 'indent': '+1' }],
          ['blockquote', 'code-block']
        ]
      }
    }),
    TreeViewModule,
    BsDropdownModule.forRoot(),
    MatTabsModule,
    TreeModule,
    MatAutocompleteModule,
    FlexLayoutModule,
    MatCardModule,
    MatMenuModule
  ],
  providers: [ CurrencyPipe, DecimalPipe ],
})
export class QtModule { }
