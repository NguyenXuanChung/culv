import { QuyTrinhService } from '../../../services/quyTrinh.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';
// Translate
import { CommonService } from '../../../services/common.service';
import { Observable, of } from 'rxjs';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { ActivatedRoute, Router } from '@angular/router';
import { DonViService } from '../../../services/don-vi.service';
import { DanhMucService } from '../../../services/danhmuc.service';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import * as dateFormat from 'dateformat';
import { DialogDetailBoNhiemComponent } from './dialog-detail-boNhiem.component';

@Component({
  selector: 'app-bonhiem-tdv',
  templateUrl: './boNhiemTdv.component.html'
})
export class BoNhiemTdvComponent implements OnInit {
  displayedColumns = ['index', 'ngayHieuLuc', 'maHoTenNhanVien', 'donVi', 'viTriHienTai', 'viTriDeXuat', 'trangThaiDeXuat', 'actions'];
  dataSource: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  donVis: any[] = [];
  public id_donVis: any[] = [];
  loaiDeXuats: any[] = [];
  searchForm: FormGroup;
  permission: any;
  get fs() { return this.searchForm.controls; }
  code: string = null;
  code_Portal_QuyTrinh: string = null;
  currentURL: string;
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private quyTrinhService: QuyTrinhService,
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private donViService: DonViService,
    private dmService: DanhMucService,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }
  ngOnInit() {
    this.currentURL = window.location.href;
    this.code = this.currentURL.split('/')[this.currentURL.split('/').length - 1];
    this.code_Portal_QuyTrinh = this.currentURL.split('/')[this.currentURL.split('/').length - 2];
    this.loadCombo();
    this.createSearchForm();
    this.onSearch();
  }
  createSearchForm() {
    const date = new Date();
    const today = moment().format('YYYY-MM-DD');
    const firstDay = dateFormat(new Date(date.getFullYear(), 0, 1), 'yyyy-mm-dd');
    const lastDay = dateFormat(new Date(date.getFullYear(), 11, 31), 'yyyy-mm-dd');
    this.searchForm = this.fb.group(
      {
        TuNgay: [firstDay],
        DenNgay: [lastDay],
        content: '',
        id_donVis: [null],
        loaiDeXuat: [null],
        Code_Portal_QuyTrinh: this.code_Portal_QuyTrinh,
        CodeState: this.code
      });
  }
  onSearch() {
    this.fs.id_donVis.setValue(this.id_donVis.toString());
    this.getGroup();
  }
  loadCombo() {
    const today = moment().format('YYYY/MM/DD');
    this.donViService.getTreeToValue({ id_trungTam: 1, ngay: today }, this.common.Login.View)
      .subscribe((res: any) => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.donVis = res.data;
        }
      });
    this.dmService.getLoaiDeXuat({}, this.common.Login.View)
      .subscribe((res: any) => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.loaiDeXuats = res.data;
        }
      });
  }
  getGroup() {
    const obj = [];
    this.quyTrinhService.getListQuyTrinhDeXuat(this.searchForm.value, this.common.Login.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource = new MatTableDataSource();
      } else {
        res.data.forEach(element => {
          obj.push({
            Portal_QuyTrinh_DeXuat_ID: element.Portal_QuyTrinh_DeXuat_ID,
            TrangThaiHienTai: element.TrangThaiHienTai,
            NguoiTao: element.NguoiTao,
            TenNhomQuyenTao: element.TenNhomQuyenTao,
            CodeState: element.CodeState,
            CodeStateBefore: element.CodeStateBefore,
            ...JSON.parse(element.Data)
          });
        });
        this.dataSource = new MatTableDataSource(obj);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  openDetailDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogDetailBoNhiemComponent, {
      width: '80%',
      height: '80vh',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.common.messageRes(result);
      }
    });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
  public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
  public hasChildren(dataItem: any): boolean {
    return dataItem.children && dataItem.children.length > 0;
  }
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'multiple',
      checkOnClick: true
    };
  }
}
