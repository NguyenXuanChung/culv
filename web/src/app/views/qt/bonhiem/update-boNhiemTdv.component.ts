import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from '../../../services/common.service';
import { QuyTrinhService } from '../../../services/quyTrinh.service';
import { SearchCanBoDialogComponent } from '../../shared/search-canBo-dialog/search-canBo-dialog.component';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import { Observable, of } from 'rxjs';
import * as moment from 'moment';
import * as dateFormat from 'dateformat';

import { DonViService } from '../../../services/don-vi.service';
import { DanhMucService } from '../../../services/danhmuc.service';
import { WorkFlowService } from '../../../services/workFlow.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-update-bonhiem-tdv',
  templateUrl: './update-boNhiemTdv.component.html',
})
export class UpdateBoNhiemTdvComponent implements OnInit {
  tdvdxBoNhiemForm: FormGroup;
  submitted = false;
  checkCode = false;
  donVis: any[] = [];
  public id_donVis: any[] = [];
  loaiDeXuats: any[] = [];
  chucDanhs: any[] = [];
  id_portal_quyTrinh: any = null;
  id_sys_workFlow_state: any = null;
  id_sys_workFlow: any = null;
  state: any = null;
  code: string = null;
  id_deXuat: string = null;
  noiDung: string = '';
  id_workFlow_state_action: any = null;
  id_sys_workFlow_next_state: any = null;
  isLoadStateAction: boolean = false;
  type: string = null;
  isDraft: boolean;
  code_Portal_QuyTrinh: string = null;
  currentURL: string;
  // convenience getter for easy access to form fields
  get f() { return this.tdvdxBoNhiemForm.controls; }
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    public dialog: MatDialog,
    private dmService: DanhMucService,
    private quyTrinhService: QuyTrinhService,
    private router: Router,
    private route: ActivatedRoute,
    private donViService: DonViService,
    private workFlowService: WorkFlowService
  ) {
    this.code = this.route.snapshot.paramMap.get('code');
    this.id_deXuat = this.route.snapshot.paramMap.get('id');
    this.type = this.route.snapshot.paramMap.get('type');
  }

  ngOnInit() {
    this.currentURL = window.location.href;
    if (this.currentURL.includes('bonhiem_tdv')) {
      this.code_Portal_QuyTrinh = 'bonhiem_tdv';
    } else if (this.currentURL.includes('bonhiem_qlns')) {
      this.code_Portal_QuyTrinh = 'bonhiem_qlns';
    }
    this.createForm();
    this.loadCombo();
  }
  createForm() {
    this.tdvdxBoNhiemForm = this.fb.group(
      {
        id_canBo: [null, [Validators.required]],
        maHoTenNhanVien: [null, [Validators.required]],
        loaiDeXuat: [null, [Validators.required]],
        tenLoaiDeXuat: [null, [Validators.required]],
        viTriHienTai: [null],
        viTriDeXuat: [null, [Validators.required]],
        tenViTriDeXuat: [null, [Validators.required]],
        donViHienTai: [null],
        donViDeXuat: [null, [Validators.required]],
        tenDonViDeXuat: [null, [Validators.required]],
        chiTieuHienTai: [null],
        chiTieuCongViecDatDuoc: [null],
        chiTieuCongViecMoi: [null],
        chiTieuMoi: [null],
        luongBHXH: [null],
        thuongHQVL: [null],
        phuCapViTri: [null],
        pcDienThoai: [null],
        phuCapXangXe: [null],
        pcKhac: [null],
        soLuongNhanVienQuanLy: [null],
        ngayHieuLucDeXuat: [null],
        lyDo: [null],
      });
    this.quyTrinhService.getDetailQuyTrinhDeXuat({
      Type: this.type,
      ID_PortalQuyTrinh_DeXuat: this.id_deXuat,
      CodeState: this.code,
    }, this.common.Login.View)
      .subscribe(res => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          if (!res.data) {

            if (this.code_Portal_QuyTrinh === 'bonhiem_qlns') {
              this.router.navigate(['/quytrinh/bonhiem_qlns/qlns_dexuat']);
            } else {
              this.router.navigate(['/quytrinh/bonhiem_tdv/dexuat']);
            }
            return;
          }
          console.log(res.data);
          this.isDraft = res.data.IsDraft;
          this.id_portal_quyTrinh = res.data.ID_Portal_QuyTrinh,
            this.id_sys_workFlow_state = res.data.ID_SYS_WorkFlow_State;
          this.state = res.data;
          this.id_workFlow_state_action = res.data.ID_SYS_WorkFlow_State_Action;
          this.noiDung = res.data.Comment;
          if (this.id_sys_workFlow_state) {
            this.isLoadStateAction = true;
          }
          if (res.data.NoEditAll === true) {
            this.tdvdxBoNhiemForm.disable();
          } else if (res.data.NoEdit && res.data.NoEdit.length > 0) {
            res.data.NoEdit.split(',').forEach(e => {
              this.tdvdxBoNhiemForm.get(e).disable();
            });
          }
          if (this.id_deXuat) {
            const data = JSON.parse(res.data.Data);
            this.tdvdxBoNhiemForm.setValue({
              id_canBo: data.id_canBo,
              maHoTenNhanVien: data.maHoTenNhanVien,
              loaiDeXuat: data.loaiDeXuat,
              tenLoaiDeXuat: data.tenLoaiDeXuat,
              viTriHienTai: data.viTriHienTai,
              viTriDeXuat: data.viTriDeXuat,
              tenViTriDeXuat: data.tenViTriDeXuat,
              donViHienTai: data.donViHienTai,
              donViDeXuat: data.donViDeXuat,
              tenDonViDeXuat: data.tenDonViDeXuat,
              chiTieuHienTai: data.chiTieuHienTai,
              chiTieuCongViecDatDuoc: data.chiTieuCongViecDatDuoc,
              chiTieuCongViecMoi: data.chiTieuCongViecMoi,
              chiTieuMoi: data.chiTieuMoi,
              luongBHXH: data.luongBHXH,
              thuongHQVL: data.thuongHQVL,
              phuCapViTri: data.phuCapViTri,
              pcDienThoai: data.pcDienThoai,
              phuCapXangXe: data.phuCapXangXe,
              pcKhac: data.pcKhac,
              soLuongNhanVienQuanLy: data.soLuongNhanVienQuanLy,
              ngayHieuLucDeXuat: data.ngayHieuLucDeXuat,
              lyDo: data.lyDo,
            });
            if (data.ngayHieuLucDeXuat) {
              this.f.ngayHieuLucDeXuat.setValue(dateFormat(data.ngayHieuLucDeXuat, 'yyyy-mm-dd'));
            }
            this.id_donVis.push(parseInt(data.donViDeXuat, 10));
          }
        }
      });
  }
  loadCombo() {
    const today = moment().format('YYYY/MM/DD');
    this.donViService.getTreeToValue({ id_trungTam: 1, ngay: today }, this.common.Login.View)
      .subscribe((res: any) => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.donVis = res.data;
        }
      });
    this.dmService.getChucDanh({}, this.common.Login.View)
      .subscribe((res: any) => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.chucDanhs = res.data;
        }
      });
    this.dmService.getLoaiDeXuat({}, this.common.Login.View)
      .subscribe((res: any) => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.loaiDeXuats = res.data;
        }
      });

    // this.quyTrinhService.getDetailQuyTrinhDeXuat({
    //   Type: this.type,
    //   ID_PortalQuyTrinh_DeXuat: this.id_deXuat,
    //   CodeState: this.code,
    // }, this.common.Login.View)
    //   .subscribe((res: any) => {
    //     if (res.error) {
    //       this.common.messageErr(res);
    //     } else {
    //       res.data.forEach(element => {
    //         if (element.Code === '' + this.code_Portal_QuyTrinh + '') {
    //           this.id_portal_quyTrinh = element.Portal_QuyTrinh_ID;
    //           this.id_sys_workFlow = element.ID_SYS_WorkFlow;
    //           this.workFlowService.getWorkFlowState({ ID_SYS_WorkFlow: element.ID_SYS_WorkFlow }, this.common.Login.View)
    //             .subscribe((resp: any) => {
    //               if (resp.error) {
    //                 this.common.messageErr(resp);
    //               } else {
    //                 resp.data.forEach(element1 => {
    //                   if (element1.Code === this.code) {
    //                     this.state = element1;
    //                     this.id_sys_workFlow_state = element1.SYS_WorkFlow_State_ID;
    //                     this.quyTrinhService.getActivity({
    //                       ID_Portal_QuyTrinh_DeXuat: this.id_deXuat,
    //                       ID_SYS_WorkFlow_State: this.id_sys_workFlow_state
    //                     }, this.common.Login.View)
    //                       .subscribe((resActivity: any) => {
    //                         if (resActivity.error) {
    //                           this.common.messageErr(resActivity);
    //                           this.isLoadStateAction = true;
    //                         } else {
    //                           this.id_workFlow_state_action = res.data[0].ID_WorkFlow_State_Action;
    //                           this.noiDung = res.data[0].Comment;
    //                           this.isLoadStateAction = true;
    //                         }
    //                       });
    //                     if (element1.NoEditAll === true) {
    //                       this.tdvdxBoNhiemForm.disable();
    //                     } else if (element1.NoEdit.length > 0) {
    //                       element1.NoEdit.split(',').forEach(e => {
    //                         this.tdvdxBoNhiemForm.get(e).disable();
    //                       });
    //                     }
    //                   }
    //                 });
    //               }
    //             });
    //         }
    //       });
    //     }
    //   });
  }
  onSubmit(isDarft: number) {
    this.f.donViDeXuat.setValue(this.id_donVis[0].toString());
    this.getTenDonVi(this.donVis, this.donVis[0]);
    this.getLoaiDeXuat(this.f.loaiDeXuat.value);
    this.getTenViTriDeXuat(this.f.viTriDeXuat.value);
    this.submitted = true;
    // stop here if form is invalid
    if (this.tdvdxBoNhiemForm.invalid) {
      return;
    }
    this.tdvdxBoNhiemForm.enable();
    if (this.type === 'insert') {
      this.quyTrinhService.insertDeXuat({
        ID_Portal_QuyTrinh: this.id_portal_quyTrinh,
        Data: JSON.stringify(this.tdvdxBoNhiemForm.value),
        ID_SYS_WorkFlow_Next_State: this.id_sys_workFlow_next_state,
        ID_SYS_WorkFlow_State: this.id_sys_workFlow_state,
        ID_SYS_WorkFlow_State_Action: this.id_workFlow_state_action,
        Comment: this.noiDung,
        Code: '',
        IsDraft: isDarft

      }, this.common.Login.View)
        .subscribe(res => {
          if (res.error) {
            if (this.state.NoEditAll === true) {
              this.tdvdxBoNhiemForm.disable();
            } else if (this.state.noEdit && this.state.NoEdit.length > 0) {
              this.state.NoEdit.split(',').forEach(e => {
                this.tdvdxBoNhiemForm.get(e).disable();
              });
            }
            this.common.messageErr(res);
          } else {
            this.common.messageRes(res.message);
            this.router.navigate(['/quytrinh/' + this.code_Portal_QuyTrinh + '/' + this.code]);
          }
        });
    } else if (this.type === 'update' && this.isDraft === false) {
      this.quyTrinhService.insertActivity({
        ID_Portal_QuyTrinh_DeXuat: this.id_deXuat,
        Data: JSON.stringify(this.tdvdxBoNhiemForm.value),
        ID_SYS_WorkFlow_Next_State: this.id_sys_workFlow_next_state,
        ID_SYS_WorkFlow_State: this.id_sys_workFlow_state,
        ID_SYS_WorkFlow_State_Action: this.id_workFlow_state_action,
        Comment: this.noiDung,
        IsDraft: isDarft

      }, this.common.Login.View)
        .subscribe(res => {
          if (res.error) {
            if (this.state.NoEditAll === true) {
              this.tdvdxBoNhiemForm.disable();
            } else if (this.state.noEdit && this.state.NoEdit.length > 0) {
              this.state.NoEdit.split(',').forEach(e => {
                this.tdvdxBoNhiemForm.get(e).disable();
              });
            }
            this.common.messageErr(res);
          } else {
            this.common.messageRes(res.message);
            this.router.navigate(['/quytrinh/' + this.code_Portal_QuyTrinh + '/' + this.code]);
          }
        });
    } else if (this.type === 'edit' || (this.type === 'update' && this.isDraft === true)) {
      this.quyTrinhService.editActivity({
        ID_Portal_QuyTrinh_DeXuat: this.id_deXuat,
        Data: JSON.stringify(this.tdvdxBoNhiemForm.value),
        ID_SYS_WorkFlow_Next_State: this.id_sys_workFlow_next_state,
        ID_SYS_WorkFlow_State: this.id_sys_workFlow_state,
        ID_SYS_WorkFlow_State_Action: this.id_workFlow_state_action,
        Comment: this.noiDung,
        IsDraft: isDarft
      }, this.common.Login.View)
        .subscribe(res => {
          if (res.error) {
            if (this.state.NoEditAll === true) {
              this.tdvdxBoNhiemForm.disable();
            } else if (this.state.noEdit && this.state.NoEdit.length > 0) {
              this.state.NoEdit.split(',').forEach(e => {
                this.tdvdxBoNhiemForm.get(e).disable();
              });
            }
            this.common.messageErr(res);
          } else {
            this.common.messageRes(res.message);
            this.router.navigate(['/quytrinh/' + this.code_Portal_QuyTrinh + '/' + this.code]);
          }
        });
    }
  }
  onNoClick(): void {
    console.log(JSON.stringify(this.tdvdxBoNhiemForm.value));
  }
  openDialogSearchCanBo(data: any): void {
    const dialogRef = this.dialog.open(SearchCanBoDialogComponent, {
      width: '80%',
      height: '80vh',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.f.maHoTenNhanVien.setValue(result.MaCanBo + '- ' + result.HoTen);
        this.f.viTriHienTai.setValue(result.TenChucDanh);
        this.f.donViHienTai.setValue(result.TenDonVi);
        this.f.id_canBo.setValue(result.CanBo_ID);
        // this.common.messageRes(result);
      }
    });
  }
  getTenDonVi(items: Array<any>, itemParent: any) {
    items.forEach(i => {
      if (i.data.toString() === this.f.donViDeXuat.value.toString()) {
        this.f.tenDonViDeXuat.setValue(i.label);
        return;
      }
      if (i.children.length) {
        this.getTenDonVi(i.children, i);
      }
    });
  }
  getTenViTriDeXuat(id) {
    let name = '';
    name = this.chucDanhs.find(element => element.ChucDanh_ID === id).TenChucDanh;
    this.f.tenViTriDeXuat.setValue(name);
  }
  getLoaiDeXuat(id) {
    let name = '';
    name = this.loaiDeXuats.find(element => element.DM_LoaiDeXuat_ID === id).Name;
    this.f.tenLoaiDeXuat.setValue(name);
  }
  loadStateAction(data) {
    this.id_workFlow_state_action = data.id_workFlow_state_action;
    this.noiDung = data.noiDung;
    this.id_sys_workFlow_next_state = data.id_sys_workFlow_next_state;
  }
  public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
  public hasChildren(dataItem: any): boolean {
    return dataItem.children && dataItem.children.length > 0;
  }
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'single',
      checkOnClick: true
    };
  }
}
