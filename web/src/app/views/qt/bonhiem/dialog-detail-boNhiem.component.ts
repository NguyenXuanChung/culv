import { QuyTrinhService } from './../../../services/quyTrinh.service';
import { TtbhService } from '../../../services/ttbh.service';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// Translate
import { CommonService } from '../../../services/common.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-dialog-detail-bonhiem',
  templateUrl: './dialog-detail-boNhiem.component.html',
})
export class DialogDetailBoNhiemComponent implements OnInit {
  displayedColumns = ['index', 'nguoiThucHien', 'quyTrinh', 'ngayThucHien', 'noiDung', 'trangThai'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  selectedRow = false;
  constructor(
    private common: CommonService,
    private quyTrinhService: QuyTrinhService,
    public dialogRef: MatDialogRef<DialogDetailBoNhiemComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.getChiTiet();
  }
  getChiTiet() {
    this.quyTrinhService.getActivity({
      ID_Portal_QuyTrinh_DeXuat: this.data.Portal_QuyTrinh_DeXuat_ID
    }, this.common.Login.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource = new MatTableDataSource();
      } else {
        console.log(res.data);
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
