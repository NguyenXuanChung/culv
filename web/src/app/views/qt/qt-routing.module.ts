import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BoNhiemTdvComponent } from './bonhiem/boNhiemTdv.component';
import { UpdateBoNhiemTdvComponent } from './bonhiem/update-boNhiemTdv.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'bonhiem_tdv/dexuat'
      },
      {
        path: 'bonhiem_tdv/dexuat',
        component: BoNhiemTdvComponent,
      },
      {
        path: 'bonhiem_tdv/qlns_xacnhan',
        component: BoNhiemTdvComponent,
      },
      {
        path: 'bonhiem_tdv/gdns_duyet',
        component: BoNhiemTdvComponent,
      },
      {
        path: 'bonhiem_tdv/xuatquyetdinh',
        component: BoNhiemTdvComponent,
      },
      {
        path: 'bonhiem_qlns/qlns_dexuat',
        component: BoNhiemTdvComponent,
      },
      {
        path: 'bonhiem_qlns/qlns_gdns_duyet',
        component: BoNhiemTdvComponent,
      },
      {
        path: 'bonhiem_qlns/qlns_xuatquyetdinh',
        component: BoNhiemTdvComponent,
      },
      {
        path: 'qt/bonhiem_tdv/:code/:type/:id',
        component: UpdateBoNhiemTdvComponent,
      },
      {
        path: 'qt/bonhiem_tdv/:code/:type',
        component: UpdateBoNhiemTdvComponent,
      },
      {
        path: 'qt/bonhiem_qlns/:code/:type/:id',
        component: UpdateBoNhiemTdvComponent,
      },
      {
        path: 'qt/bonhiem_qlns/:code/:type',
        component: UpdateBoNhiemTdvComponent,
      },
      // {
      //   path: 'bonhiem/tdv/dexuat/insert',
      //   component: UpdateBoNhiemTdvComponent,
      // },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QtRoutingModule { }
