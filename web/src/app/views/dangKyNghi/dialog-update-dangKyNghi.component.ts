import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
// Translate
import { CommonService } from './../../services/common.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';


@Component({
  selector: 'app-dialog-update-dangkynghi',
  templateUrl: './dialog-update-dangKyNghi.component.html',
})
export class DialogUpdateDangKyNghiComponent implements OnInit {
  submitted = false;
  isInputDate = false;
  // convenience getter for easy access to form fields
  tqForm: FormGroup;
  numbers: number[];
  capQuyetDinhs: any[];
  phanLoais: any[];
  danhHieuHinhThucs: any[];

  // convenience getter for easy access to form fields
  get f() { return this.tqForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogUpdateDangKyNghiComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
    const date = new Date();
    this.numbers = Array(100).fill(0).map((x, i) => (i + date.getFullYear() - 95));
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }

  createForm() {
    const date = new Date();
    this.tqForm = this.fb.group(
      {
        nam: [date.getFullYear(), [Validators.required]],
        name: [null, [Validators.required]],
        id_phanLoai: [null, [Validators.required]],
        typeKhenThuong: [null, [Validators.required]],
        id_capQuyetDinh: [null],
        id_danhHieuHinhThuc: [null],
        ngayLapDs: [null],
        soQd: [null],
        ngayQd: [null],
        ghiChu: [null],
      });
    if (this.data) {
      this.tqForm.setValue({
        'nam': parseInt(this.data.nam, 10),
        'name': this.data.name,
        'id_phanLoai': this.data.id_phanLoai,
        'typeKhenThuong': this.data.typeKhenThuong.toString(),
        'id_capQuyetDinh': this.data.id_capQuyetDinh,
        'id_danhHieuHinhThuc': this.data.id_danhHieuHinhThuc,
        'soQd': this.data.soQd,
        'ghiChu': this.data.ghiChu,
        'ngayLapDs': null,
        'ngayQd': null,
      });
      if (this.data.ngayLapDs) {
        this.f.ngayLapDs.setValue(dateFormat(this.data.ngayLapDs, 'yyyy-mm-dd'));
      }
      if (this.data.ngayQd) {
        this.f.ngayQd.setValue(dateFormat(this.data.ngayQd, 'yyyy-mm-dd'));
      }
      // this.onChangeHinhThuc();
    }
  }
  onSubmit() {
    if (this.f.ngayLapDs.value === '') {
      this.f.ngayLapDs.setValue(null);
    }
    if (this.f.ngayQd.value === '') {
      this.f.ngayQd.setValue(null);
    }
    this.submitted = true;
    // stop here if form is invalid
    if (this.tqForm.invalid) {
      return;
    }
    // if (!this.data) {
    //   this.ktService.insertDotKhenThuong(this.tqForm.value, this.common.KT.Update)
    //     .subscribe(res => {
    //       if (res.error) {
    //         this.common.messageErr(res);
    //       } else {
    //         this.dialogRef.close(res.message);
    //       }
    //     });
    // } else {
    //   this.ktService.editDotKhenThuong({
    //     data: this.tqForm.value,
    //     condition: { id: this.data.id }
    //   }, this.common.KT.Update)
    //     .subscribe(res => {
    //       if (res.error) {
    //         this.common.messageErr(res);
    //       } else {
    //         this.dialogRef.close(res.message);
    //       }
    //     });
    // }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  loadCombo() {
    // this.ktService.getCapQuyetDinh({}, this.common.KT.View).subscribe(res => {
    //   if (!res.error) {
    //     this.capQuyetDinhs = res.data;
    //   }
    // });
    // this.ktService.getPhanLoai({}, this.common.KT.View).subscribe(res => {
    //   if (!res.error) {
    //     this.phanLoais = res.data;
    //   }
    // });
  }
  // onChangeHinhThuc() {
  //   this.ktService.getDanhHieuHinhThuc({
  //     id_phanLoai: this.f.id_phanLoai.value,
  //     typeKhenThuong: this.f.typeKhenThuong.value,
  //     id_capQuyetDinh: this.f.id_capQuyetDinh.value,
  //   }, this.common.KT.View).subscribe(res => {
  //     if (!res.error) {
  //       this.danhHieuHinhThucs = res.data;
  //     }
  //   });
  // }
}
