import { TtbhService } from './../../services/ttbh.service';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// Translate
import { CommonService } from '../../services/common.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-dialog-detail-dangkynghi',
  templateUrl: './dialog-detail-dangKyNghi.component.html',
})
export class DialogDetailDangKyNghiComponent implements OnInit {
  displayedColumns = ['index', 'loaiDangKy', 'hoTen', 'ngayThucHien', 'ngay', 'lyDo'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private common: CommonService,
    private ttbhService: TtbhService,
    public dialogRef: MatDialogRef<DialogDetailDangKyNghiComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.getQuaTrinhBhnt();
  }
  getQuaTrinhBhnt() {
    this.ttbhService.getBhhc({ ID_CanBo_BaoHiem: this.data.ID_CanBo_BH }, this.common.DangKyNghi.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource = new MatTableDataSource();
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
