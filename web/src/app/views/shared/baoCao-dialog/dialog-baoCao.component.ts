import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
// Translate
import { CommonService } from '../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as moment from 'moment';
import * as dateFormat from 'dateformat';
import { CheckableSettings, CheckedState } from '@progress/kendo-angular-treeview';
import { Observable, of } from 'rxjs';
import { DonViService } from '../../../services/don-vi.service';
import { BaoCaoService } from '../../../services/baocao.service';
import { saveAs } from 'file-saver';
import { DanhMucService } from '../../../services/danhmuc.service';
import { TqDqtvService } from '../../../services/tq-dqtv.service';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';

@Component({
  selector: 'app-dialog-baocao',
  templateUrl: './dialog-baoCao.component.html',
})
export class DialogBaoCaoComponent implements OnInit {
  dotTqForm: FormGroup;
  submitted = false;
  falseDate = false;
  public id_donVis: any[] = [];
  public id_donViPers: any[] = [];
  public disabledKeys: any[] = [];
  expandedKeys: any[] = [];
  donVis: any[];
  tinhs: any[];
  public id_huyens: any[] = [];
  huyens: any[];
  public id_donViDbdvs: any[] = [];
  donViDbdvs: any[];
  tenHuyen: string = '';
  numbers: number[];
  dotTuyenQuans: any[];
  // convenience getter for easy access to form fields
  get f() { return this.dotTqForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private baoCaoService: BaoCaoService,
    private dmTqService: TqDqtvService,
    private tuyenQuanService: TuyenQuanService,
    private dmService: DanhMucService,
    private donVi: DonViService,
    private storageService: StorageService,
    public dialogRef: MatDialogRef<DialogBaoCaoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
    const date = new Date();
    this.numbers = Array(100).fill(0).map((x, i) => (i + date.getFullYear() - 95));
    this.storageService.get(AuthConstants.AUTH).then(res => {
      res.id_donVis.split(',').forEach(element => {
        this.id_donViPers.push(parseInt(element, 10));
      });
    });
  }

  ngOnInit() {
    this.loadCombo();
    this.createForm();
    if (this.data.searchDonViDbdv) {
      this.dmTqService.getDonViDbdv({}, this.common.BaoCaoChung.View)
        .subscribe((res: any) => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.donViDbdvs = res.data;
          }
        });
    }
    if (this.data.searchDonViQuanLy) {
      const today = moment().format('YYYY/MM/DD');
      this.donVi.getTreeToValue({ id_trungTam: 1, tuNgay: today, denNgay: today }, this.common.BaoCaoChung.View)
        .subscribe((res: any) => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.donVis = res.data;
            this.getAllParentTextProperties(this.donVis, this.donVis[0]);
          }
        });
    }
    if (this.data.searchHuyen) {
      this.getHuyen(this.data.id_tinh);
    }
  }
  loadTenHuyen() {
    this.huyens.forEach(element => {
      if (element.id.toString() === this.id_huyens.toString()) {
        this.tenHuyen = element.name;
      }
    });
  }
  loadDotTuyenQuan(e) {
    this.f.id_dotTuyenQuan.setValue(0);
    this.tuyenQuanService.getDotTuyenQuan({ nam: e }, this.common.BaoCaoChung.View).subscribe(res => {
      if (!res.error) {
        this.dotTuyenQuans = res.data;
        this.f.id_dotTuyenQuan.setValue(res.data[0].id);
      }
    });
  }
  createForm() {
    const date = new Date();
    const firstDay = dateFormat(new Date(date.getFullYear(), 0, 1), 'yyyy-mm-dd');
    const lastDay = dateFormat(new Date(date.getFullYear(), 11, 31), 'yyyy-mm-dd');
    this.dotTqForm = this.fb.group(
      {
        tenBaoCao: [this.data.tenBaoCao, [Validators.required]],
        tuNgay: [firstDay, [Validators.required]],
        denNgay: [lastDay, [Validators.required]],
        tuoiNam: [this.data.tuoiNam],
        tuoiNu: [this.data.tuoiNu],
        id_tinh: [this.data.id_tinh],
        id_type: [this.data.id_type],
        id_type_state: [this.data.id_type_state],
        id_type_beforeState: [this.data.id_type_beforeState],
        gioiTinh: [this.data.gioiTinh],
        id_donViQuanLys: [null, [Validators.required]],
        id_donViDbdvs: [null, [Validators.required]],
        id_huyens: [null, [Validators.required]],
        id_dotTuyenQuan: [null, [Validators.required]],
        content: [''],
        tuoi: [''],
        nam: [date.getFullYear()],
        id_trinhDoVanHoa: [''],
        id_trinhDoHocVan: [''],
      });
    if (this.data.searchDotTuyenQuan) {
      this.loadDotTuyenQuan(date.getFullYear());
    }
  }
  onSubmit() {
    if (this.id_huyens.length === 0 && this.data.searchHuyen > 0) {
      this.huyens.forEach(e => {
        this.id_huyens.push(e.id);
      });
    }
    this.f.id_huyens.setValue(this.id_huyens.toString());
    this.f.id_donViDbdvs.setValue(this.id_donViDbdvs.toString());
    if (this.f.id_dotTuyenQuan.value) {
      const dotTuyenQuan = this.dotTuyenQuans.find(x => x.id === this.f.id_dotTuyenQuan.value);
      this.f.tuNgay.setValue(dateFormat(dotTuyenQuan.tuNgay, 'yyyy-mm-dd'));
      this.f.denNgay.setValue(dateFormat(dotTuyenQuan.denNgay, 'yyyy-mm-dd'));
      const checkDate = new Date(this.f.tuNgay.value);
      this.f.tuNgay.setValue(dateFormat(new Date(checkDate.getFullYear(), checkDate.getMonth(), checkDate.getDate() - 1), 'yyyy-mm-dd'));
    }
    const denNgay = new Date(this.f.denNgay.value);
    const tuNgay = new Date(this.f.tuNgay.value);
    if (denNgay.getTime() < tuNgay.getTime() && this.data.tuNgay ) {
      this.falseDate = true;
    }
    if (this.data.searchNam === 1) {
      this.f.nam.setValue(denNgay.getFullYear());
    }
    if (this.data.searchDonViQuanLy > 0) {
      const id_donVis = [];
      this.id_donViPers.forEach((element, i) => {
        const index: number = this.id_donVis.indexOf(element);
        if (index !== -1) {
          id_donVis.push(element);
        }
        if (i === this.id_donViPers.length - 1) {
          this.id_donVis = id_donVis;
          if (this.id_donVis.length === 0) {
            this.id_donVis = this.id_donViPers;
          }
          this.f.id_donViQuanLys.setValue(this.id_donVis.toString());
        }
      });
    }
    if (this.data.searchDonViQuanLy !== 2) {
      this.dotTqForm.controls['id_donViQuanLys'].setErrors(null);
    }
    if (this.data.searchHuyen !== 2) {
      this.dotTqForm.controls['id_huyens'].setErrors(null);
    }
    if (this.data.searchDonViDbdv !== 2) {
      this.dotTqForm.controls['id_donViDbdvs'].setErrors(null);
    }
    if (this.data.searchDotTuyenQuan !== 2) {
      this.dotTqForm.controls['id_dotTuyenQuan'].setErrors(null);
    }
    if (this.data.tuNgay !== 1) {
      this.dotTqForm.controls['tuNgay'].setErrors(null);
    }
    if (this.data.denNgay !== 1) {
      this.dotTqForm.controls['denNgay'].setErrors(null);
    }
    // if(this.f.tuNgay)
    this.submitted = true;
    // stop here if form is invalid
    if (this.dotTqForm.invalid) {
      return;
    }
    if (this.data.template === 'congDan_tuyenQuan') {
      this.baoCaoService.getCongDan(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'TQ_01GNN') {
      this.baoCaoService.getTqGnn(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'TQ_15GNN') {
      this.baoCaoService.getTq15Gnn(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'PhuLuc3') {
      this.baoCaoService.getPhuLuc3(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'BCHQS_12HL') {
      this.baoCaoService.get12Hl(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DQTV_13HL') {
      this.baoCaoService.get13Hl(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'TQ_13GNN_SAU') {
      this.baoCaoService.getTq13GnnS(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'TQ_06GNN_Truoc') {
      this.baoCaoService.getTq06GnnT(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'TQ_12bGNN') {
      this.baoCaoService.getTq12bGnn(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'TQ_14GNN') {
      this.baoCaoService.getTq14Gnn(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'TQ_13GNN_Truoc') {
      this.baoCaoService.getTq13GnnT(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'CbDangCongTac') {
      this.baoCaoService.getCbDangCongTac(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'TQ_01bGNN') {
      this.baoCaoService.getTq01bGnn(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'TQ_SoTuyen_Huyen') {
      this.loadTenHuyen();
      if (this.id_huyens.length !== 1) {
        this.common.messageErr({ error: { message: 'qpan.thongBaoBaoCao' } });
        return;
      }
      this.baoCaoService.getSoTuyenHuyen({ tenHuyen: this.tenHuyen, ...this.dotTqForm.value }, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'TQ_SoTuyen_Tinh') {
      this.baoCaoService.getSoTuyenTinh(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'TQ_KhamTuyen') {
      this.baoCaoService.getKhamTuyen(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'TQ_KhamTuyen_KetQua') {
      this.baoCaoService.getKhamTuyenKetQua(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DBDV_M665') {
      this.baoCaoService.getDbdvM665(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DBDV_M650') {
      this.baoCaoService.getDbdvM650(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DBDV_M662a') {
      this.baoCaoService.getDbdvM662a(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DBDV_SapXep') {
      this.baoCaoService.getDbdvSapXep(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'SQDB_704a') {
      this.baoCaoService.getSqdb704a(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'SQDB_704aMau2P1') {
      this.baoCaoService.getSqdb704aMau2P1(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'SQDB_704aMau2P2') {
      this.baoCaoService.getSqdb704aMau2P2(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'SQDB_704aMau2P12') {
      this.baoCaoService.getSqdb704aMau2P12(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'SQDB_704aMau4') {
      this.baoCaoService.getSqdb704aMau4(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'SQDB_704aMau2CNQS') {
      this.baoCaoService.getSqdb704aMau2Cnqs(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'QNDB_ThucLuc') {
      this.baoCaoService.getQndbThucLuc(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'QNDB_TangGiam') {
      this.baoCaoService.getQndbTangGiam(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'QNDB_711') {
      this.baoCaoService.getQndb711(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'SQDB_704aMau3') {
      this.baoCaoService.getSqdb704aMau3(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'SQDB_704aMau2DP') {
      this.baoCaoService.getSqdb704aMau2DP(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DQTV_1_TCLL') {
      this.baoCaoService.getDqtv1Tcll(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DQTV_2_TCLL') {
      this.baoCaoService.getDqtv2Tcll(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DQTV_3_TCLL') {
      this.baoCaoService.getDqtv3Tcll(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DQTV_6_TCLL') {
      this.baoCaoService.getDqtv6Tcll(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DQTV_4_TCLL') {
      this.baoCaoService.getDqtv4Tcll(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DQTV_7_TCLL') {
      this.baoCaoService.getDqtv7Tcll(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DQTV_8_TCLL') {
      this.baoCaoService.getDqtv8Tcll(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DQTV_11_TCLL') {
      this.baoCaoService.getDqtv11Tcll(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DQTV_10_TCLL') {
      this.baoCaoService.getDqtv10Tcll(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DQTV_5_TCLL') {
      this.baoCaoService.getDqtv5Tcll(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'DQTV_14_TaC') {
      this.baoCaoService.getDqtv14Tac(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'PTKT_801QP') {
      this.baoCaoService.getPtkt801Qp(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'PTKT_808_DP') {
      this.baoCaoService.getPtkt808DP(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'PTKT_808_TW') {
      this.baoCaoService.getPtkt808TW(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'PTKT_M04') {
      this.baoCaoService.getPtktM04(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'PTKT_M11') {
      this.baoCaoService.getPtktM11(this.dotTqForm.value, this.common.BaoCaoChung.Export)
        .subscribe(res => {
          if (res.type === 'application/json') {
            this.common.messageErr({ 'error': { 'message': 'Not found data.' } });
          } else {
            saveAs(new File([res], res.type));
            this.dialogRef.close(res.message);
          }
        });
    } else if (this.data.template === 'QPAN_01') {
      this.loadTenHuyen();
      if (this.id_huyens.length !== 1) {
        this.common.messageErr({ error: { message: 'qpan.thongBaoBaoCao' } });
      } else {
        this.baoCaoService.getQpan01({ tenHuyen: this.tenHuyen, id_huyen: this.id_huyens, ...this.dotTqForm.value },
          this.common.BaoCaoChung.Export)
          .subscribe(res => {
            if (res.error) {
              this.common.messageErr(res);
            } else {
              saveAs(new File([res], res.type));
              this.dialogRef.close(res.message);
            }
          });
      }
    } else if (this.data.template === 'QPAN_02') {
      this.loadTenHuyen();
      if (this.id_huyens.length !== 1) {
        this.common.messageErr({ error: { message: 'qpan.thongBaoBaoCao' } });
      } else {
        this.baoCaoService.getQpan02({ tenHuyen: this.tenHuyen, id_huyen: this.id_huyens, ...this.dotTqForm.value },
          this.common.BaoCaoChung.Export)
          .subscribe(res => {
            if (res.error) {
              this.common.messageErr(res);
            } else {
              saveAs(new File([res], res.type));
              this.dialogRef.close(res.message);
            }
          });
      }
    } else if (this.data.template === 'QPAN_03') {
      this.loadTenHuyen();
      if (this.id_huyens.length !== 1) {
        this.common.messageErr({ error: { message: 'qpan.thongBaoBaoCao' } });
      } else {
        this.baoCaoService.getQpan03({ tenHuyen: this.tenHuyen, id_huyen: this.id_huyens, ...this.dotTqForm.value },
          this.common.BaoCaoChung.Export)
          .subscribe(res => {
            if (res.error) {
              this.common.messageErr(res);
            } else {
              saveAs(new File([res], res.type));
              this.dialogRef.close(res.message);
            }
          });
      }
    } else if (this.data.template === 'QPAN_04') {
      this.loadTenHuyen();
      if (this.id_huyens.length !== 1) {
        this.common.messageErr({ error: { message: 'qpan.thongBaoBaoCao' } });
      } else {
        this.baoCaoService.getQpan04({ tenHuyen: this.tenHuyen, id_huyen: this.id_huyens, ...this.dotTqForm.value },
          this.common.BaoCaoChung.Export)
          .subscribe(res => {
            if (res.error) {
              this.common.messageErr(res);
            } else {
              saveAs(new File([res], res.type));
              this.dialogRef.close(res.message);
            }
          });
      }
    } else if (this.data.template === 'QPAN_05') {
      this.loadTenHuyen();
      if (this.id_huyens.length !== 1) {
        this.common.messageErr({ error: { message: 'qpan.thongBaoBaoCao' } });
      } else {
        this.baoCaoService.getQpan05({ tenHuyen: this.tenHuyen, id_huyen: this.id_huyens, ...this.dotTqForm.value },
          this.common.BaoCaoChung.Export)
          .subscribe(res => {
            if (res.error) {
              this.common.messageErr(res);
            } else {
              saveAs(new File([res], res.type));
              this.dialogRef.close(res.message);
            }
          });
      }
    } else if (this.data.template === 'QPAN_PhuLuc1') {
      this.loadTenHuyen();
      if (this.id_huyens.length !== 1) {
        this.common.messageErr({ error: { message: 'qpan.thongBaoBaoCao' } });
      } else {
        this.baoCaoService.getQpanPhuLuc1({ tenHuyen: this.tenHuyen, id_huyen: this.id_huyens, ...this.dotTqForm.value },
          this.common.BaoCaoChung.Export)
          .subscribe(res => {
            if (res.error) {
              this.common.messageErr(res);
            } else {
              saveAs(new File([res], res.type));
              this.dialogRef.close(res.message);
            }
          });
      }
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  loadCombo() {

  }
  getHuyen(e) {
    this.dmService.getHuyenByDonVi({ id_tinh: e }, this.common.BaoCaoChung.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.huyens = res.data;
      }
    });
  }
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'multiple',
      checkOnClick: true
    };
  }
  getAllParentTextProperties(items: Array<any>, itemParent: any) {
    items.forEach(i => {
      if (i.hasPermission || i.id_capTren === 0) {
        itemParent.hasChildPermission = 1;
        const index: number = this.disabledKeys.indexOf(itemParent.data);
        if (index !== -1) {
          this.disabledKeys.splice(index, 1);
        }
        this.expandedKeys.push(itemParent.data);
      } else if (!i.hasPermission) {
        this.disabledKeys.push(i.data);
      }
      if (i.children.length) {
        this.getAllParentTextProperties(i.children, i);
      }
    });
  }
  public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
  public hasChildren(dataItem: any): boolean {
    return dataItem.children && dataItem.children.length > 0;
  }
}
