import { DonViService } from './../../../services/don-vi.service';
import { DmService } from './../../../services/dm.service';
import { Component, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// Translate
import { LanguageService } from './../../../services/language.service';
import { CbService } from '../../../services/cb.service';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import { Observable, of } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CommonService } from '../../../services/common.service';
import * as moment from 'moment';

@Component({
  selector: 'app-search-canbo-dialog',
  templateUrl: './search-canBo-dialog.component.html'
})
export class SearchCanBoDialogComponent {
  donVis: any[] = [];
  loaiHopDongs: any[] = [];
  hinhThucSuDungLaoDongs: any[] = [];
  public id_hinhThucSuDungLaoDongs: any[] = [];
  public id_donVis: any[] = [];
  public id_loaiHopDongs: any[] = [];
  displayedColumns = ['index', 'maCanBo', 'hoTen', 'gioiTinh', 'email', 'dienThoaiDD'];
  dataSource: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  searchForm: FormGroup;
  get fs() { return this.searchForm.controls; }
  constructor(
    public dialogRef: MatDialogRef<SearchCanBoDialogComponent>,
    private dmService: DmService,
    private canBoService: CbService,
    private common: CommonService,
    private fb: FormBuilder,
    private donViService: DonViService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }
  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.loadCombo();
    this.createSearchForm();
  }
  loadCombo() {
    const today = moment().format('YYYY/MM/DD');
    this.donViService.getTreeToValue({id_trungTam: 1, ngay: today}, this.common.Login.View)
      .subscribe((res: any) => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          console.log(res.data);
          this.donVis = res.data;
        }
      });
  }
  createSearchForm() {
    this.searchForm = this.fb.group(
      {
        id_donVis: [null],
        tenCanBo: [''],
      });
  }

  onSearch() {
    this.fs.id_donVis.setValue(this.id_donVis.toString());
    this.canBoService.getList(this.searchForm.value, this.common.Login.View)
      .subscribe((res: any) => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.dataSource = new MatTableDataSource(res.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  onSubmit() {
    this.dialogRef.close(this.selectedRow);
  }
  public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
  public hasChildren(dataItem: any): boolean {
    return dataItem.children && dataItem.children.length > 0;
  }
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'multiple',
      checkOnClick: true
    };
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
