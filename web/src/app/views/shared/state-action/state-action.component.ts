import { WorkFlowService } from './../../../services/workFlow.service';
import { Component, ElementRef, EventEmitter, Inject, Input, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonService } from '../../../services/common.service';
// Translate
import { LanguageService } from './../../../services/language.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-state-action',
  templateUrl: './state-action.component.html'
})
export class StateActionComponent {
  private _elementRef: ElementRef;
  @Output() newItemEvent = new EventEmitter<any>();
  @Input() id_workFlow_state: any;
  @Input() noiDung: any;
  @Input() id_workFlow_state_action: any;
  workFlowStateActions: any[] = [];
  searchForm: FormGroup;
  get f() { return this.searchForm.controls; }
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private workFlowService: WorkFlowService,
    ) { }
  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.createSearchForm();
    this.loadCombo();
  }
  loadCombo() {
    this.workFlowService.getWorkFlowStateAction({ID_SYS_WorkFlow_State: this.id_workFlow_state}, this.common.Login.View)
    .subscribe((res: any) => {
      console.log(res);
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.workFlowStateActions = res.data;
        if (this.id_workFlow_state_action) {
          this.pushData();
        }
      }
    });
  }
  createSearchForm() {
    this.searchForm = this.fb.group(
      {
        id_workFlow_state_action: [this.id_workFlow_state_action, [Validators.required]],
        id_sys_workFlow_next_state: [null],
        noiDung: [this.noiDung],
      });
  }
  pushData() {
    const stateAction = this.workFlowStateActions.find(e =>
      e.SYS_WorkFlow_State_Action_ID === this.f.id_workFlow_state_action.value).NextState;
      this.f.id_sys_workFlow_next_state.setValue(stateAction);
    this.newItemEvent.emit(this.searchForm.value);
  }
}
