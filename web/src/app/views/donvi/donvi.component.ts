import { Component, OnInit, ViewChild } from '@angular/core';
// Translate
import { CommonService } from '../../services/common.service';
import { DonViService } from './../../services/don-vi.service';
import { StorageService } from '../../services/storage.service';
import { AuthConstants } from '../../config/auth-constants';
import { Router } from '@angular/router';
import { TreeNode } from 'primeng/api/treenode';
import { DonViDialogComponent } from './donvi-dialog.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from './../shared/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-donvi',
  templateUrl: './donvi.component.html',
  styleUrls: ['./donvi.component.css']
})
export class DonviComponent implements OnInit {
  dsDonVis: TreeNode[];
  colsDonVi: any[];
  permission: any;
  loadTable1: boolean = true;
  allRelations: string;

  constructor(
    private common: CommonService,
    private donViService: DonViService,
    private router: Router,
    public dialog: MatDialog,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit(): void {
    this.loadTable1 = false;
    this.colsDonVi = [
      { field: 'tenDonVi', header: 'Tên', class: 'colName' },
      { field: 'maDonVi', header: 'Mã', class: 'colCode' },
      { field: 'ngayBatDau', header: 'Từ ngày', class: 'colDate' },
      { field: 'actions', header: 'Tùy chọn', class: 'colAction' }
    ];
    this.onSearch();
    if (!this.permission.CB_BCHQS._view) {
      this.router.navigate(['/']);
    }
  }

  setClass(data: any) {
    return { [`${data}`]: true };
  }

  onSearch() {
    this.getDonVi();
  }

  getDonVi() {
    this.donViService.getTreeNode({}, this.common.CB_BCHQS.View).subscribe(res => {
      this.loadTable1 = true;
      if (res.error) {
        this.common.messageErr(res);
        this.dsDonVis = [];
      } else {
        this.dsDonVis = res.data;
      }
    });
  }

  getAllRelations() {

  }

  openDonViDialog(data: any): void {
    const dialogRef = this.dialog.open(DonViDialogComponent, {
      width: '700px',
      height: '80%',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }

  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.body.tenDonVi }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.donViService.deleteDonVi({ donvi_id: data.body.donvi_id }, this.common.CB_BCHQS.Delete).subscribe(response => {
          if (response.error) {
            this.common.messageErr(response);
          } else {
            this.onSearch();
            this.common.messageRes(response.message);
          }
        });
      }
    });
  }
}
