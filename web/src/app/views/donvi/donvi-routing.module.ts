import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DonviComponent } from './donvi.component';

const routes: Routes = [
  {
    path: '',
    component: DonviComponent,
    data: {
      title: 'DonVi'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DonviRoutingModule { }
