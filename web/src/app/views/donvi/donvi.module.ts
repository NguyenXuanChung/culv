import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { AngularMaterial } from '../../angular-material';
import { MomentModule } from 'ngx-moment';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TreeModule } from 'primeng/tree';
import { TreeTableModule} from 'primeng/treetable';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// Dropdowns Component
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { DonviRoutingModule } from './donvi-routing.module';
import { DonviComponent } from './donvi.component';
import { DonViDialogComponent } from './donvi-dialog.component';

export function translateHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  imports: [
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: translateHttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BsDropdownModule.forRoot(),
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    AngularMaterial,
    MomentModule,
    MatProgressSpinnerModule,
    TreeModule,
    TreeTableModule,
    TreeViewModule,
    DonviRoutingModule
  ],
  entryComponents: [
    DonViDialogComponent
  ],
  declarations: [
    DonviComponent,
    DonViDialogComponent
  ]
})
export class DonviModule { }
