import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../services/common.service';
import { HscbService } from './../../services/hscb.service';
import { DonViService } from './../../services/don-vi.service';

import * as dateFormat from 'dateformat';
import * as moment from 'moment';
import { DanhMucService } from '../../services/danhmuc.service';
import { TqDqtvService } from '../../services/tq-dqtv.service';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-donvi-dialog',
  templateUrl: './donvi-dialog.component.html'
})
export class DonViDialogComponent implements OnInit {
  donViForm: FormGroup;
  lastUpdateForm: FormGroup;
  submitted = false;
  id_donvi: string;
  loaiHinhDoanhNghieps: select[] = [];
  nganHangs: select[] = [];

  capDonVis: any[];
  quyMoToChucs: any[];
  xas: any[];
  huyens: any[];
  tinhs: any[];
  // convenience getter for easy access to form fields
  get f() { return this.donViForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tqDqtvService: TqDqtvService,
    private dmService: DanhMucService,
    private donViService: DonViService,
    public dialogRef: MatDialogRef<DonViDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }
  createForm() {
    if (this.data.id_donvi) {
      this.id_donvi = this.data.id_donvi;
    }
    this.donViForm = this.fb.group(
      {
        id_donvi: [this.id_donvi, [Validators.required]],
        tenDonVi: ['', [Validators.required]],
        id_xa: [null],
        id_huyen: [null],
        id_tinh: [null, [Validators.required]],
        ngayBatDau: [moment().format('YYYY-MM-DD'), [Validators.required]],
        maDonVi: ['', [Validators.required]],
        id_capDonVi: [null, [Validators.required]],
        khoiCq: [null, [Validators.required]],
        id_quyMoToChuc: [null, [Validators.required]],
        id_diaPhuong: [null, [Validators.required]],
        donvi_id: [null],
        phanCap: [null],
      });
    if (this.data.body) {
      this.donViForm.setValue({
        'donvi_id': this.data.body.donvi_id,
        'id_donvi': this.data.body.id_capTren,
        'tenDonVi': this.data.body.tenDonVi,
        'maDonVi': this.data.body.maDonVi,
        'ngayBatDau': null,
        'id_xa': null,
        'id_tinh': null,
        'id_huyen': null,
        'id_capDonVi': this.data.body.id_capDonVi,
        'khoiCq': null,
        'id_quyMoToChuc': this.data.body.id_quyMoToChuc,
        'id_diaPhuong': this.data.body.id_diaPhuong,
        'phanCap': this.data.body.phanCap,
      });
      if (this.data.body.ngayBatDau) {
        this.f.ngayBatDau.setValue(dateFormat(this.data.body.ngayBatDau, 'yyyy-mm-dd', true));
      }
      if (this.data.body.id_capDonVi === 1) {
        this.f.id_tinh.setValue(this.f.id_diaPhuong.value);
      }
      if (this.data.body.khoiCq) {
        this.f.khoiCq.setValue(this.data.body.khoiCq);
      }
      if (this.data.body.id_capDonVi === 2) {
        this.f.id_huyen.setValue(this.f.id_diaPhuong.value);
        this.loadHuyenEdit(this.f.id_diaPhuong.value);
      }
      if (this.data.body.id_capDonVi === 3) {
        this.f.id_xa.setValue(this.f.id_diaPhuong.value);
        this.loadXaEdit(this.f.id_diaPhuong.value);
      }
    }
  }
  onSubmit() {
    this.submitted = true;
    if (this.f.id_capDonVi.value === 1) {
      this.f.id_diaPhuong.setValue(this.f.id_tinh.value);
    }
    if (this.f.id_capDonVi.value === 2) {
      this.f.id_diaPhuong.setValue(this.f.id_huyen.value);
    }
    if (this.f.id_capDonVi.value === 3) {
      this.f.id_diaPhuong.setValue(this.f.id_xa.value);
    }
    this.f.id_tinh.disable();
    this.f.id_huyen.disable();
    this.f.id_xa.disable();
    // stop here if form is invalid
    if (this.donViForm.invalid) {
      this.f.id_tinh.enable();
      this.f.id_huyen.enable();
      this.f.id_xa.enable();
      return;
    }

    if (!this.data.body) {
      this.donViService.insertDonVi(this.donViForm.value, this.common.CB_BCHQS.Insert)
        .subscribe(res => {
          if (res.error) {
            this.f.id_tinh.enable();
            this.f.id_huyen.enable();
            this.f.id_xa.enable();
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      // const tungaydonvi = dateFormat(this.data.body.ngayBatDau, 'yyyy-mm-dd', true);
      // const ngaybatdau = this.f.ngayBatDau.value;
      // if (tungaydonvi === ngaybatdau) {
        this.donViService.updateDonVi({
          data: this.donViForm.value,
          condition: { donVi_detail_id: this.data.body.donvi_detail_id }
        }, this.common.CB_BCHQS.Update)
          .subscribe(res => {
            if (res.error) {
              this.f.id_tinh.enable();
              this.f.id_huyen.enable();
              this.f.id_xa.enable();
              this.common.messageErr(res);
            } else {
              this.dialogRef.close(res.message);
            }
          });
      // } else {
      //   this.donViService.insertDonViQT(this.donViForm.value, this.common.CB_BCHQS.Insert)
      //     .subscribe(res => {
      //       if (res.error) {
      //         this.f.id_tinh.enable();
      //         this.f.id_huyen.enable();
      //         this.f.id_xa.enable();
      //         this.common.messageErr(res);
      //       } else {
      //         this.dialogRef.close(res.message);
      //       }
      //     });
      // }
    }
  }


  loadXaEdit(e) {
    this.dmService.getThx({ id_xa: e }, this.common.CB_BCHQS.Insert).subscribe(res => {
      if (res.error) {
      } else {
        this.f.id_huyen.setValue(res.data[0].id_huyen);
        this.f.id_tinh.setValue(res.data[0].id_tinh);
        this.getHuyen(res.data[0].id_tinh);
        this.getXa(res.data[0].id_huyen);
      }
    });
  }
  loadHuyenEdit(e) {
    this.dmService.getHuyen({ id: e }, this.common.CB_BCHQS.Insert).subscribe(res => {
      if (res.error) {
      } else {
        this.f.id_tinh.setValue(res.data[0].id_tinh);
        this.getXa(res.data[0].id);
        this.getHuyen(res.data[0].id_tinh);
      }
    });
  }
  loadHuyen(e) {
    this.f.id_xa.setValue(null);
    this.getHuyen(e);
  }
  loadXa(e) {
    this.f.id_xa.setValue(null);
    this.getXa(e);
  }
  getHuyen(e) {
    this.dmService.getHuyen({ id_tinh: e }, this.common.CB_BCHQS.Insert).subscribe(res => {
      if (res.error) {
      } else {
        this.huyens = res.data;
      }
    });
  }
  getXa(e) {
    this.dmService.getXa({ id_huyen: e }, this.common.CB_BCHQS.Insert).subscribe(res => {
      if (res.error) {
      } else {
        this.xas = res.data;
      }
    });
  }
  loadCombo() {
    this.dmService.getTinh({}, this.common.TQ_DQTV_ChucDanh.Insert).subscribe(res => {
      if (!res.error) {
        this.tinhs = res.data;
      }
    });
    this.dmService.getCapDonVi({}, this.common.TQ_DQTV_ChucDanh.Insert).subscribe(res => {
      if (!res.error) {
        this.capDonVis = res.data;
      }
    });
    this.tqDqtvService.getQuyMoToChuc({}, this.common.TQ_DQTV_ChucDanh.Insert).subscribe(res => {
      if (!res.error) {
        this.quyMoToChucs = res.data;
      }
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
