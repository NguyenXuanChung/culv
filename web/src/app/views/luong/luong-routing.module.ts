import { HoTroComponent } from './hoTro.component';
import { LuongThoaThuanComponent } from './luongThoaThuan.component';
import { LuongCoBanComponent } from './luongCoBan.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'luongcoban'
      },
      {
        path: 'luongcoban',
        component: LuongCoBanComponent,
      },
      {
        path: 'luongthoathuan',
        component: LuongThoaThuanComponent,
      },
      {
        path: 'hotro',
        component: HoTroComponent,
      },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LuongRoutingModule { }
