import { LuongService } from './../../services/luong.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';


// Translate
import { CommonService } from './../../services/common.service';
import { StorageService } from '../../services/storage.service';
import { AuthConstants } from '../../config/auth-constants';
import { Router } from '@angular/router';
import { DialogDetailCcComponent } from './dialog-detail-cc.component';

@Component({
  selector: 'app-cc',
  templateUrl: './cc.component.html'
})
export class CcComponent implements OnInit {
  displayedColumns = ['index', 'ngay', 'thu', 'gioVao1',
  'gioRa1', 'gioVao2', 'gioRa2', 'kyHieu', 'actions'];
  dataSource: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  searchForm: FormGroup;
  permission: any;
  info: any = [];
  get fs() { return this.searchForm.controls; }
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private luongService: LuongService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.createSearchForm();
    this.onSearch();
    if (!this.permission.ChamCong._view) {
      this.router.navigate(['/']);
    }
  }

  createSearchForm() {
    this.searchForm = this.fb.group(
      {
        content: [null],
        tuNgay: [null],
        denNgay: [null],
      });
  }
  openDetailDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogDetailCcComponent, {
      width: '700px',
      data: data
    });
  }
  onSearch() {
    this.fs.tuNgay.setValue(moment(this.fs.tuNgay.value).format('YYYY-MM-DD'));
    this.getLuongCoBan();
  }
  getLuongCoBan() {
    this.luongService.getHoTro(this.searchForm.value, this.common.ChamCong.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource = new MatTableDataSource();
      } else {
        console.log(res.data);
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
