import { VbService } from '../../../services/vb.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { CheckableSettings, CheckedState } from '@progress/kendo-angular-treeview';

import * as moment from 'moment';

// Translate
import { CommonService } from '../../../services/common.service';
import { Observable, of } from 'rxjs';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
// import { DialogUpdateVanBanComponent } from './dialog-update-vanBan.component';
import { AttachFilesDialogComponent } from '../../shared/attach-files-dialog/attach-files-dialog.component';
import { SearchCanBoDialogComponent } from '../../shared/search-canBo-dialog/search-canBo-dialog.component';

@Component({
  selector: 'app-vanbanquyche',
  templateUrl: './vanBanQuyChe.component.html'
})
export class VanBanQuyCheComponent implements OnInit {
  displayedColumns = ['index', 'tieuDe', 'ngayHieuLuc', 'ngayHetHieuLuc', 'nguoiKyQD', 'chucDanh', 'noiDung', 'trangThai', 'actions'];
  dataSource: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  searchForm: FormGroup;
  permission: any;
  info: any = null;
  get fs() { return this.searchForm.controls; }
  permissionAttachsFile: any;
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private vbService: VbService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
      this.storageService.get(AuthConstants.AUTH).then(res => {
      this.info = res;
    });
    this.permissionAttachsFile = common.VB_VanBanQuyChe.Update;
  }

  ngOnInit() {
    this.createSearchForm();
    this.onSearch();
    if (!this.permission.VB_VanBanQuyChe._view) {
      this.router.navigate(['/']);
    }
  }

  createSearchForm() {
    this.searchForm = this.fb.group(
      {
        id_donVi: this.info.ID_DonVi,
        id_chucDanh: this.info.ChucDanh_ID,
      });
  }

  onSearch() {
    this.getQuyChe();
  }
  getQuyChe() {
    this.vbService.getVanBanQuyChe(this.searchForm.value, this.common.VB_VanBanQuyChe.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource = new MatTableDataSource();
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  openUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(SearchCanBoDialogComponent, {
      width: '700px',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.vbService.deleteVb({ id: data.id }, this.common.VB_VanBanQuyChe.Update).subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.onSearch();
            this.common.messageRes(res.message);
          }
        });
      }
    });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
  openAttachFiles(data: any): void {
    const dialogRef = this.dialog.open(AttachFilesDialogComponent, {
      height: '80%',
      width: '700px',
      data: data
    });
  }
}
