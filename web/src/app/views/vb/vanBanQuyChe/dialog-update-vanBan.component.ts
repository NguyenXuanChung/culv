// import { VbService } from './../../services/vb.service';
// import { Component, OnInit, Inject, ViewChild } from '@angular/core';
// import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
// import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// // Translate
// import { CommonService } from './../../services/common.service';
// import { AttachFilesService } from '../../services/attach-files.service';
// import { MatTableDataSource } from '@angular/material/table';
// import { MatPaginator } from '@angular/material/paginator';
// import { MatSort } from '@angular/material/sort';
// import { saveAs } from 'file-saver';
// import { MatTabGroup } from '@angular/material/tabs';
// import * as dateFormat from 'dateformat';

// @Component({
//   selector: 'app-dialog-update-vanban',
//   templateUrl: './dialog-update-vanBan.component.html',
// })
// export class DialogUpdateVanBanComponent implements OnInit {
//   vbForm: FormGroup;
//   submitted = false;
//   @ViewChild(MatTabGroup) tabGroup: MatTabGroup;
//   // convenience getter for easy access to form fields
//   get f() { return this.vbForm.controls; }
//   public files: Array<File> = [];
//   displayedColumns = ['index', 'filename', 'actions'];
//   dataSource: MatTableDataSource<any>;
//   @ViewChild(MatSort) sort: MatSort;
//   @ViewChild(MatPaginator) paginator: MatPaginator;
//   loadListFiles: boolean = false;
//   fileInsert: Array<String> = [];

//   constructor(
//     private common: CommonService,
//     private fb: FormBuilder,
//     private vbService: VbService,
//     private attachFiles: AttachFilesService,
//     public dialogRef: MatDialogRef<DialogUpdateVanBanComponent>,
//     @Inject(MAT_DIALOG_DATA) public data: any
//   ) {
//     dialogRef.disableClose = true;
//   }

//   ngOnInit() {
//     this.createForm();
//   }
//   createForm() {
//     this.vbForm = this.fb.group(
//       {
//         name: ['', [Validators.required]],
//         noiDung: [''],
//         ngayBanHanh: [null],
//         ghiChu: [''],
//       });
//     if (this.data) {
//       this.vbForm.setValue({
//         'name': this.data.name,
//         'noiDung': this.data.noiDung,
//         'ngayBanHanh': null,
//         'ghiChu': this.data.ghiChu,
//       });
//       if (this.data.ngayBanHanh) {
//         this.f.ngayBanHanh.setValue(dateFormat(this.data.ngayBanHanh, 'yyyy-mm-dd'));
//       }
//       this.getAttachFile('vb_quanLyVanBan', this.data.id);
//     }
//   }
//   onSubmit() {
//     if (this.f.ngayBanHanh.value === '') {
//       this.f.ngayBanHanh.setValue(null);
//     }
//     this.submitted = true;
//     // stop here if form is invalid
//     if (this.vbForm.invalid) {
//       this.tabGroup.selectedIndex = 0;
//       return;
//     }
//     if (!this.data) {
//       this.vbService.insertVb(this.vbForm.value, this.common.QLVB.Insert)
//         .subscribe(res => {
//           if (res.error) {
//             this.common.messageErr(res);
//           } else {
//             if (this.files.length !== 0) {
//               this.common.messageRes(res.message);
//               this.insertAttachFile('vb_quanLyVanBan', res.data.id);
//             } else {
//               this.dialogRef.close(res.message);
//             }
//           }
//         });
//     } else {
//       this.vbService.editVb({
//         data: this.vbForm.value,
//         condition: { id: this.data.id }
//       }, this.common.QLVB.Update)
//         .subscribe(res => {
//           if (res.error) {
//             this.common.messageErr(res);
//           } else {
//             if (this.files.length !== 0) {
//               this.common.messageRes(res.message);
//               this.insertAttachFile('vb_quanLyVanBan', this.data.id);
//             } else {
//               this.dialogRef.close(res.message);
//             }          }
//         });
//     }

//   }
//   onNoClick(): void {
//     this.dialogRef.close();
//   }

//   // file dinh kem
//   onClick() {
//     const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
//     fileUpload.onchange = () => {
//       for (let index = 0; index < fileUpload.files.length; index++) {
//         const file = fileUpload.files[index];
//         this.files.push(file);
//       }
//     };
//     fileUpload.click();
//   }
//   cancelFile(file: File) {
//     const index = this.files.indexOf(file);
//     if (index > -1) {
//       this.files.splice(index, 1);
//     }
//   }
//   clickMethodDelete(data: any) {
//     if (confirm('You sure delete file' + data.filename)) {
//       this.deleteAttachFile(data);
//     }
//   }
//   getAttachFile(tableName: string, id_data: string) {
//     this.attachFiles.getAttachFilesDetail({
//       tableName: tableName,
//       id_data: id_data
//     }, this.common.QLVB.Insert).subscribe(res => {
//       if (res.error) {
//         this.common.messageErr(res);
//       } else {
//         this.loadListFiles = true;
//         this.dataSource = new MatTableDataSource(res.data);
//         this.dataSource.sort = this.sort;
//         this.dataSource.paginator = this.paginator;
//       }
//     });
//   }
//   insertAttachFile(tableName: string, id_data: string) {
//     this.fileInsert = [];
//     const fd = new FormData();
//     if (!this.files[0]) {
//       this.common.messageErr({ error: { message: 'No file uploaded' } });
//       return;
//     }
//     fd.append('attach', this.files[0], id_data + ',' + tableName);
//     for (let index = 0; index < this.files.length; index++) {
//       this.fileInsert.push(this.files[index].name);
//       fd.append('attach', this.files[index], this.files[index].name);
//     }
//     // tslint:disable-next-line: max-line-length
//     this.attachFiles.insertAttachFiles({ tableName: tableName, id_data: id_data, files:
//        this.fileInsert.toString() }, this.common.QLVB.Insert)
//       .subscribe(res1 => {
//         if (res1.error) {
//           this.common.messageErr(res1);
//         } else {
//           this.attachFiles.uploadAttachFiles(fd, this.common.QLVB.Insert)
//             .subscribe(response => {
//               if (response.error) {
//                 this.common.messageErr(response);
//               } else {
//                 // this.getAttachFile('mdm_thongBao', id_data);
//                 this.files = [];
//                 this.tabGroup.selectedIndex = 1;
//                 this.dialogRef.close();
//               }
//             });
//         }
//       });
//   }
//   deleteAttachFile(data: any) {
//     this.attachFiles.deleteAttachFilesDetail({ id: data.id }, this.common.QLVB.Insert).subscribe(res => {
//       if (res.error) {
//         this.common.messageErr(res);
//       } else {
//         this.getAttachFile('vb_quanLyVanBan', data.id_data);
//       }
//     });
//   }
//   downloadAttachFiles(data: any) {
//     this.attachFiles.downloadAttachFiles({ path: data.path }, this.common.QLVB.Insert).subscribe(res => {
//       if (res.error) {
//         this.common.messageErr(res);
//       } else {
//         saveAs(new File([res], data.filename));
//       }
//     });
//   }
// }
