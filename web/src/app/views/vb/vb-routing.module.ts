import { VanBanQuyCheComponent } from './vanBanQuyChe/vanBanQuyChe.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
      path: '',
      children: [
        {
          path: '',
          redirectTo: 'vanbanquyche'
        },
        {
          path: 'vanbanquyche',
          component: VanBanQuyCheComponent,
        },
      ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VbRoutingModule { }
