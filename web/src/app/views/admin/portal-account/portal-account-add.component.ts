import { TqDqtvService } from './../../../services/tq-dqtv.service';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { PortalAccountService } from './../../../services/portal-account.service';
import { PortalNhomQuyenService } from './../../../services/portal-nhom-quyen.service';
import { DonViService } from './../../../services/don-vi.service';
import { RolePermissionService } from './../../../services/role-permission.service';
import * as moment from 'moment';
import { CheckableSettings, CheckedState } from '@progress/kendo-angular-treeview';
import { Observable, of } from 'rxjs';
import { CommonService } from '../../../services/common.service';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-portal-account',
  templateUrl: './portal-account-add.component.html'
})
export class PortalAccountAddComponent implements OnInit {
  accountForm: FormGroup;
  submitted = false;
  today: string;
  canBos: select[] = [];
  nhomQuyens: select[] = [];
  public id_donViTvs: any[] = [];
  public id_donVis: any[] = [];
  public id_donViPers: any[] = [];
  public disabledKeys: any[] = [];
  expandedKeys: any[] = [];
  public id_roles: any[] = [];
  public id_donVi: any[] = [];
  donViTree: any;
  donViTvs: any;
  roleTree: any;
  loadTree1: boolean = false;
  loadTree2: boolean = false;
  // convenience getter for easy access to form fields
  get f() { return this.accountForm.controls; }

  constructor(
    private fb: FormBuilder,
    private account: PortalAccountService,
    private nhomQuyen: PortalNhomQuyenService,
    private donVi: DonViService,
    private common: CommonService,
    private roleService: RolePermissionService,
    private storageService: StorageService,
    private dmTqService: TqDqtvService,
    public dialogRef: MatDialogRef<PortalAccountAddComponent>,
    @Inject(MAT_DIALOG_DATA) public data: number
  ) {
    dialogRef.disableClose = true;
    this.storageService.get(AuthConstants.AUTH).then(res => {
      res.id_donVis.split(',').forEach(element => {
        this.id_donViPers.push(parseInt(element, 10));
      });
    });
  }

  ngOnInit() {
    this.loadCombo();
    this.createForm();
  }

  createForm() {
    this.accountForm = this.fb.group(
      {
        id_canbo: [null],
        username: [null, [Validators.required]],
        password: [null, [Validators.required, Validators.minLength(6)]],
        id_donVis: ['', []],
        id_donViTvs: ['', []],
        fullname: [null],
      });
  }

  loadCombo() {
    this.today = moment().format('YYYY/MM/DD');
    this.donVi.getTreeToValue({ id_trungTam: 1, tuNgay: this.today, denNgay: this.today }, this.common.HT_QuanLyNguoiDung.Insert)
      .subscribe(res => {
        this.loadTree1 = true;
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.donViTree = res.data;
          this.getAllParentTextProperties(this.donViTree, this.donViTree[0]);
        }
      });
    this.roleService.getRole({}, this.common.HT_QuanLyNguoiDung.Insert).subscribe(res => {
      this.loadTree2 = true;
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.roleTree = res.data;
      }
    });
    this.dmTqService.getAllDonViTv({}, this.common.DQTV_QLTV.View).subscribe(res => {
      if (!res.error) {
        this.donViTvs = res.data;
      }
    });
  }
  getCanBoByDonVi() {
    this.account.getCanBoChuaCoTaiKhoan( {id_donVi: this.id_donVi.toString()}, this.common.HT_QuanLyNguoiDung.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.canBos = [];
        res.data.forEach(element => {
          this.canBos.push({ value: element.canbo_id, display: element.hoTen });
        });
      }
    });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.accountForm.invalid) {
      return;
    }
    //     const id_donVis = [];
    // this.id_donViPers.forEach((element, i) => {
    //   const index: number = this.id_donVis.indexOf(element);
    //   if (index !== -1) {
    //     id_donVis.push(element);
    //   }
    // });
    // this.id_donVis = id_donVis;
    this.f.id_donViTvs.setValue(this.id_donViTvs.toString());
    this.f.id_donVis.setValue(this.id_donVis.toString());
    // Add portal_account
    this.account.add(this.accountForm.value, this.common.HT_QuanLyNguoiDung.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.roleService.updateListAccountRole({
          id_portal_account: res.data[0].id,
          id_roles: this.id_roles.toString()
        }, this.common.HT_QuanLyNhomQuyen.Insert)
          .subscribe(resRole => {
            if (resRole.error) {
              this.common.messageErr(res);
            } else {
            }
          });
        this.dialogRef.close(res.message);
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'multiple',
      checkOnClick: false
    };
  }
  public get checkableSettingsSingle(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'single',
      checkOnClick: false
    };
  }
  public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
  // public hasChildren = (dataItem: any): boolean => !!dataItem.children;
  public hasChildren(dataItem: any): boolean {
    // Check if the parent node has children.
    return dataItem.children && dataItem.children.length > 0;
  }
   getAllParentTextProperties(items: Array<any>, itemParent: any) {
    items.forEach(i => {
      if (i.hasPermission || i.id_capTren === 0) {
        itemParent.hasChildPermission = 1;
        const index: number = this.disabledKeys.indexOf(itemParent.data);
        if (index !== -1) {
          this.disabledKeys.splice(index, 1);
        }
        this.expandedKeys.push(itemParent.data);
      } else if (!i.hasPermission) {
        // this.disabledKeys.push(i.data);
      }
      if (i.children.length) {
        this.getAllParentTextProperties(i.children, i);
      }
    });
  }
}
