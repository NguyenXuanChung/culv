import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../services/common.service';
import { ApplicationService } from '../../services/application.service';
import { ConfigurationService } from '../../services/configuration.service';
import { DiaBanService } from '../../services/diaban.service';
import { GroupService } from '../../services/group.service';
import * as JSZip from 'jszip';

@Component({
  selector: 'app-dialog-update-app',
  templateUrl: './dialog-update-app.component.html',
})
export class DialogUpdateAppComponent implements OnInit {
  appForm: FormGroup;
  configs: any[];
  diaBans: any[];
  groups: any[];
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.appForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private appService: ApplicationService,
    private configService: ConfigurationService,
    private groupService: GroupService,
    private diaBanService: DiaBanService,
    public dialogRef: MatDialogRef<DialogUpdateAppComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.appForm = this.fb.group(
      {
        pkg: ['', [Validators.required]],
        name: ['', [Validators.required]],
        showIcon: ['', [Validators.required]],
        system: ['', [Validators.required]],
        latestVersion: ['', [Validators.required]],
        runAfterInstall: ['', [Validators.required]],
        type: ['', [Validators.required]],
        iconText: ['', [Validators.required]],
        iconId: ['', [Validators.required]],
        commonApp: [''],
      });
    if (this.data) {
      this.appForm.setValue({
        'pkg': this.data.pkg,
        'name': this.data.name,
        'showIcon': this.data.showIcon,
        'system': this.data.system,
        'latestVersion': this.data.latestVersion,
        'runAfterInstall': this.data.runAfterInstall,
        'type': this.data.type,
        'iconText': this.data.iconText,
        'iconId': this.data.iconId,
        'commonApp': this.data.commonApp,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.appForm.invalid) {
      return;
    }
    if (!this.data) {
      this.appService.insertApp(this.appForm.value, this.common.Application.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.appService.editApp({
        data: this.appForm.value,
        condition: { id: this.data.id }
      }, this.common.Application.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }
  }
  onFileChange(e: any) {
    if (e.target.files.length > 1) {
      this.common.messageErr({ error: { message: 'DontImportMutilFile' } });
    }
    const file = e.target.files[0];
    const fd = new FormData();
    fd.append('attach', file);
    this.appService.checkPackage(fd, this.common.Application.Insert)
    .subscribe(res => {
     
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.f.pkg.setValue(res.data.package);
        this.f.latestVersion.setValue(res.data.versionName);
      }
    });
  }
  // loadCombo() {
  //   this.configService.getConfig({}, this.common.Application.Insert).subscribe(res => {
  //     if (res.error) {
  //       this.common.messageErr(res);
  //     } else {
  //       this.configs = res.data;
  //     }
  //   });
  //   this.diaBanService.getDiaBan({}, this.common.Application.Insert).subscribe(res => {
  //     if (res.error) {
  //       this.common.messageErr(res);
  //     } else {
  //       this.diaBans = res.data;
  //     }
  //   });
  //   this.groupService.getGroup({}, this.common.Application.Insert).subscribe(res => {
  //     if (res.error) {
  //       this.common.messageErr(res);
  //     } else {
  //       this.groups = res.data;
  //     }
  //   });
  // }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
