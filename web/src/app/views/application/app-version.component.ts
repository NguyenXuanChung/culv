import { GroupService } from './../../services/group.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { CheckableSettings, CheckedState } from '@progress/kendo-angular-treeview';

import * as moment from 'moment';

// Translate
import { CommonService } from './../../services/common.service';
import { Observable, of } from 'rxjs';
import { StorageService } from '../../services/storage.service';
import { AuthConstants } from '../../config/auth-constants';
import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationService } from '../../services/application.service';
import { DialogUpdateVersionComponent } from './dialog-update-version.component';
import { DialogUpdateConfigAppComponent } from './dialog-update-config-app.component';

@Component({
  selector: 'app-app-version',
  templateUrl: './app-version.component.html'
})
export class AppVersionComponent implements OnInit {
  displayedColumns = ['index', 'version', 'url', 'actions'];
  dataSource: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  nameApplication: String;
  searchForm: FormGroup;
  permission: any;
  id_application: string;
  get fs() { return this.searchForm.controls; }

  constructor(
    private route: ActivatedRoute,
    private common: CommonService,
    private fb: FormBuilder,
    private appService: ApplicationService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
    this.id_application = this.route.snapshot.paramMap.get('id_project');

  }

  ngOnInit() {
    this.onSearch();
    if (!this.permission.Application._view) {
      this.router.navigate(['/']);
    }
  }

  onSearch() {
    this.getApp();
  }
  getApp() {
    this.appService.getVersion({id_application: this.id_application}, this.common.Application.View).subscribe(res => {
     
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource = new MatTableDataSource();
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  openUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateVersionComponent, {
      width: '700px',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openUpdateConfigDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateConfigAppComponent, {
      width: '700px',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.appService.deleteApp({ id: data.id }, this.common.Application.Delete).subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.onSearch();
            this.common.messageRes(res.message);
          }
        });
      }
    });
  }
  // openAttachFiles(data: any): void {
  //   const dialogRef = this.dialog.open(AttachFilesDialogComponent, {
  //     height: '80%',
  //     width: '700px',
  //     data: data
  //   });
  // }
  // openComments(data: any): void {
  //   const dialogRef = this.dialog.open(CommentsComponent, {
  //     height: '80%',
  //     width: '800px',
  //     data: data
  //   });
  // }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
