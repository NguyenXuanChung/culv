import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../services/common.service';
import { ApplicationService } from '../../services/application.service';
import { ConfigurationService } from '../../services/configuration.service';
import { DiaBanService } from '../../services/diaban.service';
import { GroupService } from '../../services/group.service';

@Component({
  selector: 'app-dialog-update-config-app',
  templateUrl: './dialog-update-config-app.component.html',
})
export class DialogUpdateConfigAppComponent implements OnInit {
  configAppForm: FormGroup;
  configs: any[];
  diaBans: any[];
  groups: any[];
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.configAppForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private appService: ApplicationService,
    private configService: ConfigurationService,
    private groupService: GroupService,
    private diaBanService: DiaBanService,
    public dialogRef: MatDialogRef<DialogUpdateConfigAppComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.configAppForm = this.fb.group(
      {
        pkg: ['', [Validators.required]],
        name: ['', [Validators.required]],
        showIcon: ['', [Validators.required]],
        system: ['', [Validators.required]],
        latestVersion: ['', [Validators.required]],
        runAfterInstall: ['', [Validators.required]],
        type: ['', [Validators.required]],
        iconText: ['', [Validators.required]],
        iconId: ['', [Validators.required]],
        commonApp: [''],
      });
    if (this.data) {
      this.configAppForm.setValue({
        'pkg': this.data.pkg,
        'name': this.data.name,
        'showIcon': this.data.showIcon,
        'system': this.data.system,
        'latestVersion': this.data.latestVersion,
        'runAfterInstall': this.data.runAfterInstall,
        'type': this.data.type,
        'iconText': this.data.iconText,
        'iconId': this.data.iconId,
        'commonApp': this.data.commonApp,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.configAppForm.invalid) {
      return;
    }
    if (!this.data) {
      this.appService.insertApp(this.configAppForm.value, this.common.Application.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.appService.editApp({
        data: this.configAppForm.value,
        condition: { id: this.data.id }
      }, this.common.Application.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  loadCombo() {
    this.configService.getConfig({}, this.common.Application.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.configs = res.data;
      }
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
