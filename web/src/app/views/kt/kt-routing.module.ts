import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DotKhenThuongComponent } from './dotKhenThuong/dotKhenThuong.component';
import { DsCaNhanComponent } from './dotKhenThuong/dsCaNhan.component';
import { ThemCaNhanComponent } from './dotKhenThuong/themCaNhan.component';
import { DsTapTheComponent } from './dotKhenThuong/dsTapThe.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: ''
      },
      {
        path: 'dotkhenthuong',
        component: DotKhenThuongComponent,
      },
      {
        path: 'dotkhenthuong/get/:id',
        component: DsCaNhanComponent,
      },
      {
        path: 'dotkhenthuong/insert/:id',
        component: ThemCaNhanComponent,
      },
      {
        path: 'dotkhenthuong/getdv/:id',
        component: DsTapTheComponent,
      },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KtRoutingModule { }
