import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { CheckableSettings, CheckedState } from '@progress/kendo-angular-treeview';

import * as moment from 'moment';
import * as dateFormat from 'dateformat';

// Translate
import { CommonService } from '../../../services/common.service';
import { Observable, of } from 'rxjs';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { TuyenQuanService } from '../../../services/tq.service';
import { DqtvService } from '../../../services/dqtv.service';
import { DialogUpdateDotKhenThuongComponent } from './dialog-update-dotKhenThuong.component';
import { KtService } from '../../../services/kt.service';

@Component({
  selector: 'app-dotkhenthuong',
  templateUrl: './dotKhenThuong.component.html'
})
export class DotKhenThuongComponent implements OnInit {
  displayedColumns = ['index', 'name', 'soQd', 'ngayQd', 'typeKhenThuong', 'id_phanLoai', 'id_danhHieuHinhThuc', 'ghiChu', 'actions'];
  dataSource: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  searchForm: FormGroup;
  permission: any;
  numbers: number[];
  phanLoais: any[];
  get fs() { return this.searchForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private ktService: KtService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
    const date = new Date();
    this.numbers = Array(100).fill(0).map((x, i) => (i + date.getFullYear() - 95));

  }

  ngOnInit() {
    this.loadCombo();
    this.createSearchForm();
    this.onSearch();
    if (!this.permission.KT._view) {
      this.router.navigate(['/']);
    }
  }

  createSearchForm() {
    const date = new Date();
    this.searchForm = this.fb.group(
      {
        nam: [date.getFullYear()],
        typeKhenThuong: [null],
        id_phanLoai: [null],
      });
  }

  onSearch() {
    this.getDotKhenThuong();
  }
  getDotKhenThuong() {
    this.ktService.getListDotKhenThuong(this.searchForm.value, this.common.KT.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource = new MatTableDataSource();
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  loadCombo() {
    this.ktService.getPhanLoai({}, this.common.KT.View).subscribe(res => {
      if (!res.error) {
        this.phanLoais = res.data;
      }
    });
  }
  openUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateDotKhenThuongComponent, {
      width: '700px',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.ktService.deleteDotKhenThuong({ id: data.id },
          this.common.KT.Delete).subscribe(res => {
            if (res.error) {
              this.common.messageErr(res);
            } else {
              this.onSearch();
              this.common.messageRes(res.message);
            }
          });
      }
    });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
