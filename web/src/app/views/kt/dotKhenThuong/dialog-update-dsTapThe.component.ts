import { TqDqtvService } from './../../../services/tq-dqtv.service';
import { DonViService } from './../../../services/don-vi.service';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
// Translate
import { CommonService } from './../../../services/common.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';
import { KtService } from '../../../services/kt.service';
import { CheckableSettings, CheckedState } from '@progress/kendo-angular-treeview';
import { Observable, of } from 'rxjs';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';


@Component({
  selector: 'app-dialog-update-dstapthe',
  templateUrl: './dialog-update-dsTapThe.component.html',
})
export class DialogUpdateDsTapTheComponent implements OnInit {
  submitted = false;
  isInputDate = false;
  // convenience getter for easy access to form fields
  tqForm: FormGroup;
  numbers: number[];
  donVis: any[] = [];
  donViTvs: any[] = [];
  public id_donViTvs: any[] = [];
  public id_donVis: any[] = [];
  public id_donViPers: any[] = [];
  public disabledKeys: any[] = [];
  expandedKeys: any[] = [];
  // convenience getter for easy access to form fields
  get f() { return this.tqForm.controls; }

  constructor(
    private common: CommonService,
    private ktService: KtService,
    private dmTqService: TqDqtvService,
    private donVi: DonViService,
    private storageService: StorageService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogUpdateDsTapTheComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
    this.storageService.get(AuthConstants.AUTH).then(res => {
      res.id_donVis.split(',').forEach(element => {
        this.id_donViPers.push(parseInt(element, 10));
      });
    });
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }

  createForm() {
    const date = new Date();
    this.tqForm = this.fb.group(
      {
        id_dotKhenThuong: [this.data.id_dotKhenThuong, [Validators.required]],
        id_donViTv: [null],
        id_donViQuanLy: [null]
      });
  }
  setValue() {
    this.f.id_donViQuanLy.setValue(this.data.body.id_donViQuanLy);
    this.f.id_donViTv.setValue(this.data.body.id_donViTv);
    if (this.data.body.id_donViTv) {
      this.data.body.id_donViTv.toString().split(',').forEach(element => {
        this.id_donViTvs.push(parseInt(element, 10));
      });
    }
    if (this.data.body.id_donViQuanLy) {
      this.data.body.id_donViQuanLy.toString().split(',').forEach(element => {
        this.id_donVis.push(parseInt(element, 10));
      });
    }
  }
  onSubmit() {
    const id_donVis = [];
    this.id_donViPers.forEach((element, i) => {
      const index: number = this.id_donVis.indexOf(element);
      if (index !== -1) {
        id_donVis.push(element);
      }
    });
    this.id_donVis = id_donVis;
    this.f.id_donViQuanLy.setValue(id_donVis.toString());
    this.f.id_donViTv.setValue(this.id_donViTvs.toString());
    this.submitted = true;
    // stop here if form is invalid
    if (this.tqForm.invalid) {
      return;
    }
    if (!this.data.body) {
      this.ktService.insertDanhSach(this.tqForm.value, this.common.KT.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.ktService.updateDanhSach({
        data: this.tqForm.value,
        condition: { id: this.data.body.id }
      }, this.common.KT.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  loadCombo() {
    this.ktService.getDanhSach({ id_dotKhenThuong: this.data.id_dotKhenThuong }, this.common.KT.View).subscribe(res => {
      if (!res.error) {

        this.data.body = res.data[0];
        this.setValue();
      }
    });
    this.dmTqService.getDonViTv({}, this.common.KT.View).subscribe(res => {
      if (!res.error) {
        this.donViTvs = res.data;
      }
    });
    const today = moment().format('YYYY/MM/DD');
    this.donVi.getTreeToValue({ id_trungTam: 1, tuNgay: today, denNgay: today }, this.common.KT.View)
      .subscribe((res: any) => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.donVis = res.data;
          this.getAllParentTextProperties(this.donVis, this.donVis[0]);
        }
      });
  }
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'multiple',
      checkOnClick: true
    };
  }
  getAllParentTextProperties(items: Array<any>, itemParent: any) {
    items.forEach(i => {
      if (i.hasPermission || i.id_capTren === 0) {
        itemParent.hasChildPermission = 1;
        const index: number = this.disabledKeys.indexOf(itemParent.data);
        if (index !== -1) {
          this.disabledKeys.splice(index, 1);
        }
        this.expandedKeys.push(itemParent.data);
      } else if (!i.hasPermission) {
        this.disabledKeys.push(i.data);
      }
      if (i.children.length) {
        this.getAllParentTextProperties(i.children, i);
      }
    });
  }
  public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
  public hasChildren(dataItem: any): boolean {
    return dataItem.children && dataItem.children.length > 0;
  }
}
