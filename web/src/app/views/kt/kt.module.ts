import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { AngularMaterial } from '../../angular-material';
import { MomentModule } from 'ngx-moment';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { EditorModule } from 'primeng/editor';
import { QuillModule } from 'ngx-quill';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MatTabsModule } from '@angular/material/tabs';
import { TreeModule } from 'primeng/tree';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { DialogUpdateDotKhenThuongComponent } from './dotKhenThuong/dialog-update-dotKhenThuong.component';
import { DotKhenThuongComponent } from './dotKhenThuong/dotKhenThuong.component';
import { KtRoutingModule } from './kt-routing.module';
import { DialogUpdateDsCaNhanComponent } from './dotKhenThuong/dialog-update-dsCaNhan.component';
import { DsCaNhanComponent } from './dotKhenThuong/dsCaNhan.component';
import { ThemCaNhanComponent } from './dotKhenThuong/themCaNhan.component';
import { DsTapTheComponent } from './dotKhenThuong/dsTapThe.component';
import { DialogUpdateDsTapTheComponent } from './dotKhenThuong/dialog-update-dsTapThe.component';
import { FlexLayoutModule } from '@angular/flex-layout';

export function translateHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    DotKhenThuongComponent,
    DialogUpdateDotKhenThuongComponent,
    DialogUpdateDsCaNhanComponent,
    DsCaNhanComponent,
    ThemCaNhanComponent,
    DsTapTheComponent,
    DialogUpdateDsTapTheComponent
  ],
  entryComponents: [
    DialogUpdateDotKhenThuongComponent,
    DialogUpdateDsCaNhanComponent,
    DialogUpdateDsTapTheComponent
  ],
  imports: [
    CommonModule,
    KtRoutingModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: translateHttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    AngularMaterial,
    MomentModule,
    InputTextareaModule,
    EditorModule,
    QuillModule.forRoot({
      modules: {
        syntax: false,
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'],
          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
          [{ 'list': 'ordered' }, { 'list': 'bullet' }],
          [{ 'indent': '-1' }, { 'indent': '+1' }],
          ['blockquote', 'code-block']
        ]
      }
    }),
    TreeViewModule,
    BsDropdownModule.forRoot(),
    MatTabsModule,
    TreeModule,
    MatAutocompleteModule,
    FlexLayoutModule
  ]
})
export class KtModule { }
