import { TinhHuyenXaComponent } from './tinhhuyenxa/tinhhuyenxa.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'DM'
    },
    children: [
      {
        path: '',
        redirectTo: 'tinh-huyen-xa'
      },
      {
        path: 'tinh-huyen-xa',
        component: TinhHuyenXaComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DanhMucRoutingModule { }
