import { TqDqtvService } from './../../../services/tq-dqtv.service';
import { DanhMucService } from './../../../services/danhmuc.service';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import * as dateFormat from 'dateformat';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-dialog',
  templateUrl: './dialog-update-dm.component.html'
})
export class DialogUpdateDmComponent implements OnInit {
  public dmForm: FormGroup;
  submitted = false;
  parent_ids: select[] = [];
  phanLoaiDiaLys: any[] = [];
  capHanhChinhs: any[] = [];
  loaiXas: any[] = [];
  phanLoaiTrongDiems: any[] = [];
  // convenience getter for easy access to form fields
  get f() { return this.dmForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private dmService: DanhMucService,
    private dmTqService: TqDqtvService,
    public dialogRef: MatDialogRef<DialogUpdateDmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }
  createForm() {
    this.dmForm = this.fb.group(
      {
        id_tinh: ['', [Validators.required]],
        id_huyen: ['', [Validators.required]],
        id_xa: ['', [Validators.required]],
        code: ['', [Validators.required]],
        name: ['', [Validators.required]],
        id_phanLoaiDiaLy: [null],
        id_capHanhChinh: [null],
        id_phanLoaiTrongDiem: [null],
        id_loaiXa: [null],
        ngayCongNhanTrongDiem: [null],
        ngayThoiTrongDiem: [null],
        sortOrder: [0, [Validators.required, Validators.min(0)]],
      });
    if (this.data.name === 'dmTinh') {
      this.f.id_tinh.disable();
      this.f.id_huyen.disable();
      this.f.id_xa.disable();
      this.f.id_capHanhChinh.disable();
      this.f.id_loaiXa.disable();
      this.f.id_phanLoaiTrongDiem.disable();
      this.f.ngayCongNhanTrongDiem.disable();
      this.f.ngayThoiTrongDiem.disable();
      if (this.data.body) {

        this.f.code.setValue(this.data.body.code);
        this.f.name.setValue(this.data.body.name);
        this.f.id_phanLoaiDiaLy.setValue(this.data.body.id_phanLoaiDiaLy);
        this.f.sortOrder.setValue(this.data.body.sortOrder);
      }
    } else if (this.data.name === 'dmHuyen') {
      this.f.id_tinh.setValue(this.data.id);
      this.f.id_huyen.disable();
      this.f.id_capHanhChinh.disable();
      this.f.id_loaiXa.disable();
      this.f.id_xa.disable();
      this.f.id_phanLoaiTrongDiem.disable();
      this.f.ngayCongNhanTrongDiem.disable();
      this.f.ngayThoiTrongDiem.disable();
      if (this.data.body) {
        this.f.id_tinh.setValue(this.data.body.id_tinh);
        this.f.code.setValue(this.data.body.code);
        this.f.name.setValue(this.data.body.name);
        this.f.id_phanLoaiDiaLy.setValue(this.data.body.id_phanLoaiDiaLy);
        this.f.sortOrder.setValue(this.data.body.sortOrder);
      }
    } else if (this.data.name === 'dmXa') {
      this.f.id_tinh.disable();
      this.f.id_huyen.setValue(this.data.id);
      this.f.id_xa.disable();
      this.loadComboXa();
      if (this.data.body) {

        this.f.code.setValue(this.data.body.code);
        this.f.name.setValue(this.data.body.name);
        this.f.id_phanLoaiDiaLy.setValue(this.data.body.id_phanLoaiDiaLy);
        this.f.sortOrder.setValue(this.data.body.sortOrder);
        this.f.id_capHanhChinh.setValue(this.data.body.id_capHanhChinh);
        this.f.id_phanLoaiTrongDiem.setValue(this.data.body.id_phanLoaiTrongDiem);
        this.f.id_loaiXa.setValue(this.data.body.id_loaiXa);
        if (this.data.body.ngayCongNhanTrongDiem) {
          this.f.ngayCongNhanTrongDiem.setValue(dateFormat(this.data.body.ngayCongNhanTrongDiem, 'yyyy-mm-dd'));
        }
        if (this.data.body.ngayThoiTrongDiem) {
          this.f.ngayThoiTrongDiem.setValue(dateFormat(this.data.body.ngayThoiTrongDiem, 'yyyy-mm-dd'));
        }
      }
    } else if (this.data.name === 'dmThon') {
      this.f.id_tinh.disable();
      this.f.id_huyen.disable();
      this.f.id_xa.setValue(this.data.id);
      this.f.id_capHanhChinh.disable();
      this.f.id_loaiXa.disable();
      this.f.id_phanLoaiTrongDiem.disable();
      this.f.ngayCongNhanTrongDiem.disable();
      this.f.ngayThoiTrongDiem.disable();
      if (this.data.body) {

        this.f.id_xa.setValue(this.data.body.id_xa);
        this.f.code.setValue(this.data.body.code);
        this.f.name.setValue(this.data.body.name);
        this.f.id_phanLoaiDiaLy.setValue(this.data.body.id_phanLoaiDiaLy);
        this.f.sortOrder.setValue(this.data.body.sortOrder);
      }
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.dmForm.invalid) {
      return;
    }
    if (this.data.name === 'dmXa') {
      if (this.f.ngayThoiTrongDiem.value === '') {
        this.f.ngayThoiTrongDiem.setValue(null);
      }
      if (this.f.ngayCongNhanTrongDiem.value === '') {
        this.f.ngayCongNhanTrongDiem.setValue(null);
      }
    }
    // kiểm tra xem thuộc loại danh mục gì để update theo danh mục tương ứng
    let updateRespone;
    if (!this.data.body) {
      if (this.data.name === 'dmTinh') {
        updateRespone = this.dmService.insertTinh(this.dmForm.value, this.common.DM_Tinh_Huyen_Xa.Insert);
      } else if (this.data.name === 'dmHuyen') {
        updateRespone = this.dmService.insertHuyen(this.dmForm.value, this.common.DM_Tinh_Huyen_Xa.Insert);
      } else if (this.data.name === 'dmXa') {
        updateRespone = this.dmService.insertXa(this.dmForm.value, this.common.DM_Tinh_Huyen_Xa.Insert);
      } else if (this.data.name === 'dmThon') {
        updateRespone = this.dmService.insertThon(this.dmForm.value, this.common.DM_Tinh_Huyen_Xa.Insert);
      }
    } else {
      if (this.data.name === 'dmTinh') {
        updateRespone = this.dmService.editTinh({
          data: this.dmForm.value,
          condition: { id: this.data.body.id }
        }, this.common.DM_Tinh_Huyen_Xa.Update);
      } else if (this.data.name === 'dmHuyen') {
        updateRespone = this.dmService.editHuyen({
          data: this.dmForm.value,
          condition: { id: this.data.body.id }
        }, this.common.DM_Tinh_Huyen_Xa.Update);
      } else if (this.data.name === 'dmXa') {
        updateRespone = this.dmService.editXa({
          data: this.dmForm.value,
          condition: { id: this.data.body.id }
        }, this.common.DM_Tinh_Huyen_Xa.Update);
      } else if (this.data.name === 'dmThon') {
        updateRespone = this.dmService.editThon({
          data: this.dmForm.value,
          condition: { id: this.data.body.id }
        }, this.common.DM_Tinh_Huyen_Xa.Update);
      }
    }
    if (updateRespone) {
      updateRespone.subscribe(res => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.dialogRef.close(res.message);
        }
      });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  loadCombo() {
    this.dmTqService.getPhanLoaiDiaLy({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.phanLoaiDiaLys = res.data;
      }
    });
  }
  loadComboXa() {
    this.dmTqService.getLoaiXa({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.loaiXas = res.data;
      }
    });
    this.dmTqService.getPhanLoaiTrongDiem({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.phanLoaiTrongDiems = res.data;
      }
    });
    this.dmTqService.getCapHanhChinh({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.capHanhChinhs = res.data;
      }
    });
  }
}
