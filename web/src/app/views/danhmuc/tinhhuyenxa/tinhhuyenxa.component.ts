import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
// Translate
import { CommonService } from '../../../services/common.service';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { DanhMucService } from '../../../services/danhmuc.service';
import { DialogUpdateDmComponent } from './dialog-update-dm.component';

@Component({
  selector: 'app-tinhhuyenxa',
  templateUrl: './tinhhuyenxa.component.html'
})
export class TinhHuyenXaComponent implements OnInit {
  displayedColumns1 = ['code', 'name', 'actions'];
  displayedColumns2 = ['code', 'name', 'actions'];
  displayedColumns3 = ['code', 'name', 'actions'];
  displayedColumns4 = ['code', 'name', 'actions'];
  dataSource1: MatTableDataSource<any>;
  dataSource2: MatTableDataSource<any>;
  dataSource3: MatTableDataSource<any>;
  dataSource4: MatTableDataSource<any>;
  rowTinh: any;
  rowHuyen: any;
  rowXa: any;
  selectedRow1: boolean;
  selectedRow2: boolean;
  selectedRow3: boolean;
  selectedRow4: boolean;
  loadTable1: boolean = true;
  loadTable2: boolean = true;
  loadTable3: boolean = true;
  loadTable4: boolean = true;
  @ViewChild(MatSort) sort: MatSort;
  permission: any;
  constructor(
    private common: CommonService,
    private dmService: DanhMucService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.loadTable1 = false;
    this.onSearch('dmTinh');
    if (!this.permission.DM_Tinh_Huyen_Xa._view) {
      this.router.navigate(['/']);
    }
  }

  onSearch(type: string) {
    if (type === 'dmTinh') {
      this.getTinh();
    } else if (type === 'dmHuyen') {
      this.getHuyen(this.rowTinh);
    } else if (type === 'dmXa') {
      this.getXa(this.rowHuyen);
    } else if (type === 'dmThon') {
      this.getThon(this.rowXa);
    }
  }
  //#region getTinh ((1))
  // khi get bảng đầu tiên thì làm mới 2 bảng phía sau.
  // nếu không get được thì làm mới bảng đầu tiên
  getTinh() {
    this.dmService.getTinh({ status: 1 }, this.common.DM_Tinh_Huyen_Xa.View).subscribe(res => {
      this.loadTable1 = true;
      this.rowHuyen = null;
      this.rowTinh = null;
      this.rowXa = null;
      this.dataSource2 = new MatTableDataSource();
      this.dataSource3 = new MatTableDataSource();
      this.dataSource4 = new MatTableDataSource();
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource1 = new MatTableDataSource();
      } else {
        this.dataSource1 = new MatTableDataSource(res.data);
        this.dataSource1.sort = this.sort;
        this.dataSource3 = new MatTableDataSource();
        this.dataSource4 = new MatTableDataSource();
      }
    });
  }
  //#endregion

  //#region getHuyen ((2))
  // selectedRow1 dùng để lưu lại row đã chọn tại bảng 1.
  // get bảng theo id row đã chọn ở bảng thứ 1.
  // khi get mới bảng thứ 2 sẽ làm mới bảng thứ 3
  // nếu không get được bảng thứ 2 (k có dữ liệu) thì sẽ làm mới bảng thứ 2.
  getHuyen(row: any) {
    this.loadTable2 = false;

    if (!this.selectedRow1) {
      this.selectedRow1 = row;
    } else {
      this.selectedRow1 = row;
    }
    if (row) {
      this.rowTinh = row;
    }
    this.rowHuyen = null;
    this.dataSource3 = new MatTableDataSource();
    this.dmService.getHuyen({ id_tinh: this.rowTinh.id, status: 1 },
      this.common.DM_Tinh_Huyen_Xa.View).subscribe(res => {
        this.loadTable2 = true;
        if (res.error) {
          // this.common.messageErr(res);
          this.dataSource2 = new MatTableDataSource();
          this.dataSource3 = new MatTableDataSource();
          this.dataSource4 = new MatTableDataSource();

        } else {
          this.dataSource2 = new MatTableDataSource(res.data);
          this.dataSource3 = new MatTableDataSource();
          this.dataSource4 = new MatTableDataSource();
          this.dataSource2.sort = this.sort;
        }
      });
  }
  //#endregion

  //#region getXa ((3))
  // selectedRow2 dùng để lưu lại row đã chọn tại bảng 2.
  // get bảng theo id row đã chọn ở bảng thứ 2.
  // nếu không get được bảng thứ 3 (k có dữ liệu) thì sẽ làm mới bảng thứ 3.
  getXa(row: any) {
    this.loadTable3 = false;

    if (!this.selectedRow2) {
      this.selectedRow2 = row;
    } else {
      this.selectedRow2 = row;
    }
    this.rowHuyen = row;
    this.dmService.getXa({ id_huyen: this.rowHuyen.id, status: 1 },
      this.common.DM_Tinh_Huyen_Xa.View).subscribe(res => {
        this.loadTable3 = true;
        if (res.error) {
          // this.common.messageErr(res);
          this.dataSource3 = new MatTableDataSource();
        } else {
          this.dataSource3 = new MatTableDataSource(res.data);
          this.dataSource3.sort = this.sort;
        }
      });
  }
  getThon(row: any) {
    this.loadTable4 = false;

    if (!this.selectedRow2) {
      this.selectedRow3 = row;
    } else {
      this.selectedRow3 = row;
    }
    this.rowXa = row;
    this.dmService.getThon({ id_xa: this.rowXa.id, status: 1 },
      this.common.DM_Tinh_Huyen_Xa.View).subscribe(res => {
        this.loadTable4 = true;
        if (res.error) {
          // this.common.messageErr(res);
          this.dataSource4 = new MatTableDataSource();
        } else {
          this.dataSource4 = new MatTableDataSource(res.data);
          this.dataSource4.sort = this.sort;
        }
      });
  }
  //#endregion

  // data sẽ có dạng [row.id, (tên danh mục)]
  openUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateDmComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch(data.name);
        this.common.messageRes(result);
      }
    });
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.body.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        let resDelete;
        if (data.name === 'dmTinh') {
          resDelete = this.dmService.deleteTinh({ id: data.body.id }, this.common.DM_Tinh_Huyen_Xa.Delete);
        } else if (data.name === 'dmHuyen') {
          resDelete = this.dmService.deleteHuyen({ id: data.body.id }, this.common.DM_Tinh_Huyen_Xa.Delete);

        } else if (data.name === 'dmXa') {
          resDelete = this.dmService.deleteXa({ id: data.body.id }, this.common.DM_Tinh_Huyen_Xa.Delete);
        } else if (data.name === 'dmThon') {
          resDelete = this.dmService.deleteThon({ id: data.body.id }, this.common.DM_Tinh_Huyen_Xa.Delete);
        }
        resDelete.subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.onSearch(data.name);
            this.common.messageRes(res.message);
          }
        });
      }
    });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow4) {
      this.selectedRow4 = row;
    } else {
      this.selectedRow4 = row;
    }
  }
}
