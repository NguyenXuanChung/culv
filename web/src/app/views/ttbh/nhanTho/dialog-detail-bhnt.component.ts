import { TtbhService } from './../../../services/ttbh.service';
import { VbService } from '../../../services/vb.service';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from '../../../services/common.service';
import { AttachFilesService } from '../../../services/attach-files.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { saveAs } from 'file-saver';
import * as dateFormat from 'dateformat';

@Component({
  selector: 'app-dialog-detail-bhnt',
  templateUrl: './dialog-detail-bhnt.component.html',
})
export class DialogDetailBhntComponent implements OnInit {
  vbForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.vbForm.controls; }
  displayedColumns = ['index', 'soHopDong', 'tuNgay', 'denNgay', 'mucDong', 'loaiBienDong', 'chucVu'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private ttbhService: TtbhService,
    private attachFiles: AttachFilesService,
    public dialogRef: MatDialogRef<DialogDetailBhntComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.getQuaTrinhBhnt();
  }
  getQuaTrinhBhnt() {
    this.ttbhService.getBhnt({ID_CanBo_BH: this.data.ID_CanBo_BH}, this.common.TTBH_NhanTho.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource = new MatTableDataSource();
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
