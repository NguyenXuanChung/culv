import { TtbhService } from './../../../services/ttbh.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { CheckableSettings, CheckedState } from '@progress/kendo-angular-treeview';

import * as moment from 'moment';

// Translate
import { CommonService } from './../../../services/common.service';
import { Observable, of } from 'rxjs';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { AttachFilesDialogComponent } from '../../shared/attach-files-dialog/attach-files-dialog.component';
import { DialogDetailBhntComponent } from './dialog-detail-bhnt.component';

@Component({
  selector: 'app-bhnt',
  templateUrl: './bhnt.component.html'
})
export class BhntComponent implements OnInit {
  displayedColumns = ['index', 'soHopDong', 'ngayThamGia', 'mucThamGia', 'mucHienTai', 'soThangThamGia', 'quaTrinhHopDong'];
  dataSource: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  searchForm: FormGroup;
  permission: any;
  get fs() { return this.searchForm.controls; }
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private ttbhService: TtbhService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.createSearchForm();
    this.onSearch();
    if (!this.permission.TTBH_NhanTho._view) {
      this.router.navigate(['/']);
    }
  }

  createSearchForm() {
    this.searchForm = this.fb.group(
      {
        content: [null],
      });
  }

  onSearch() {
    this.getBhnt();
  }
  getBhnt() {
    this.ttbhService.getBhnt(this.searchForm.value, this.common.TTBH_NhanTho.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource = new MatTableDataSource();
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  openDetailDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogDetailBhntComponent, {
      width: '700px',
      data: data
    });
  }
  openUpdateDialog(data: any): void {
    // const dialogRef = this.dialog.open(DialogUpdateVanBanComponent, {
    //   width: '700px',
    //   data: data
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.onSearch();
    //     this.common.messageRes(result);
    //   }
    // });
  }
  openDeleteDialog(data: any): void {
    // const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
    //   width: '350px',
    //   data: { delete: 1, name: data.name }
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     console.log('Yes clicked');
    //     this.ttbhService.deleteVb({ id: data.id }, this.common.TTBH_NhanTho.Delete).subscribe(res => {
    //       if (res.error) {
    //         this.common.messageErr(res);
    //       } else {
    //         this.onSearch();
    //         this.common.messageRes(res.message);
    //       }
    //     });
    //   }
    // });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
