import { TtbhService } from './../../../services/ttbh.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { CheckableSettings, CheckedState } from '@progress/kendo-angular-treeview';

import * as moment from 'moment';
import * as dateFormat from 'dateformat';
import { environment } from './../../../../environments/environment';

// Translate
import { CommonService } from '../../../services/common.service';
import { Observable, of } from 'rxjs';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { TuyenQuanService } from '../../../services/tq.service';
import { DqtvService } from '../../../services/dqtv.service';
import { KtService } from '../../../services/kt.service';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-bhxh',
  templateUrl: './bhxh.component.html'
})
export class BhxhComponent implements OnInit {
  displayedBhyts = ['index', 'soTheBhyt', 'ngayCap', 'tuNgay', 'denNgay',  'tenBenhVien'];
  dataBhyt: MatTableDataSource<any>;
  displayedBhxhs = ['index', 'loaiBienDong', 'tuThang', 'denThang', 'heSoLuong', 'mucLuong',
   'bhxh', 'bhyt', 'bhtn', 'bhtnld', 'tuDong'];
  dataBhxh: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  searchForm: FormGroup;
  permission: any;
  numbers: number[];
  phanLoais: any[];
  bhxh: any;
  get fs() { return this.searchForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private ttbhService: TtbhService,
    private authService: AuthService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
    // this.storageService.get(AuthConstants.AUTH).then(res => {
    //   this.info = res;
    // });
  }

  ngOnInit() {
    this.loadCombo();
    this.createSearchForm();
  }

  createSearchForm() {
    const date = new Date();
    this.searchForm = this.fb.group(
      {
        nam: [date.getFullYear()],
        typeKhenThuong: [null],
        id_phanLoai: [null],
      });
  }

  loadCombo() {
    this.ttbhService.getBhxh({}, this.common.TTBH_XaHoi.View).subscribe(res => {
      if (!res.error) {
        this.bhxh = res.data;
      }
    });
    this.ttbhService.getQuaTrinhBhyt({}, this.common.TTBH_XaHoi.View).subscribe(res => {
      if (!res.error) {
        console.log(res.data);
        this.dataBhyt = new MatTableDataSource(res.data);
        this.dataBhyt.paginator = this.paginator;
        this.dataBhyt.sort = this.sort;
      }
    });
    this.ttbhService.getQuaTrinhBhxh({}, this.common.TTBH_XaHoi.View).subscribe(res => {
      if (!res.error) {
        console.log(res.data);
        this.dataBhxh = new MatTableDataSource(res.data);
        this.dataBhxh.paginator = this.paginator;
        this.dataBhxh.sort = this.sort;
      }
    });
  }
  openUpdateDialog(data: any): void {
    // const dialogRef = this.dialog.open(DialogUpdateDotKhenThuongComponent, {
    //   width: '700px',
    //   data: data
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.onSearch();
    //     this.common.messageRes(result);
    //   }
    // });
  }
  openDeleteDialog(data: any): void {
    // const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
    //   width: '350px',
    //   data: { delete: 1, name: data.name }
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     console.log('Yes clicked');
    //     this.ktService.deleteDotKhenThuong({ id: data.id },
    //       this.common.KT.Delete).subscribe(res => {
    //         if (res.error) {
    //           this.common.messageErr(res);
    //         } else {
    //           this.onSearch();
    //           this.common.messageRes(res.message);
    //         }
    //       });
    //   }
    // });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
