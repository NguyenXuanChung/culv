import { BhntComponent } from './nhanTho/bhnt.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BhxhComponent } from './xaHoi/bhxh.component';
import { BhhcComponent } from './healthCare/bhhc.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'bhxh'
      },
      {
        path: 'bhxh',
        component: BhxhComponent,
      },
      {
        path: 'bhnt',
        component: BhntComponent,
      },
      {
        path: 'bhhc',
        component: BhhcComponent,
      },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TtbhRoutingModule { }
