import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class LuongService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  // Retrieve data with condition in body
  getLuongCoBan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('luong/luongcoban/get', data, permission);
  }
  getLuongThoaThuan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('luong/luongthoathuan/get', data, permission);
  }
  getHoTro(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('luong/hotro/get', data, permission);
  }


}
