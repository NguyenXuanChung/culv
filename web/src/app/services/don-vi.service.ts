import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class DonViService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  getTree(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('donvis/getTree', data, permission);
  }
  getTreeToValue(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('donvis/getTree-value', data, permission);
  }
  getTreeNode(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('donvis/gettree-node', data, permission);
  }
  getCanBo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('cb/canbo/getby-donvis', data, permission);
  }
  insertDonVi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('donvis/insert', data, permission);
  }
  insertDonViQT(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('donvis/insertQT', data, permission);
  }
  updateDonVi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('donvis/update', data, permission);
  }
  deleteDonVi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('donvis/delete', data, permission);
  }
  getAllRelations(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('donvis/get-AllRelations', data, permission);
  }
}
