import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class BaoCaoService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  getCongDan(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/congdan', data, permission);
  }
  getTqGnn(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/tqgnn', data, permission);
  }
  getTq01bGnn(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/tq01bgnn', data, permission);
  }
  getTq15Gnn(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/tq15gnn', data, permission);
  }
  getTq13GnnS(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/tq13gnns', data, permission);
  }
  getTq12bGnn(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/tq12bgnn', data, permission);
  }
  getTq14Gnn(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/tq14gnn', data, permission);
  }
  getTq06GnnT(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/tq06gnnt', data, permission);
  }
  getTq13GnnT(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/tq13gnnt', data, permission);
  }
  getDqtv1Tcll(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dqtv1tcll', data, permission);
  }
  getDqtv2Tcll(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dqtv2tcll', data, permission);
  }
  getDqtv3Tcll(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dqtv3tcll', data, permission);
  }
  getDqtv4Tcll(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dqtv4tcll', data, permission);
  }
  getDqtv5Tcll(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dqtv5tcll', data, permission);
  }
  getDqtv7Tcll(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dqtv7tcll', data, permission);
  }
  getDqtv8Tcll(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dqtv8tcll', data, permission);
  }
  getDqtv6Tcll(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dqtv6tcll', data, permission);
  }
  getDqtv11Tcll(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dqtv11tcll', data, permission);
  }
  getDqtv10Tcll(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dqtv10tcll', data, permission);
  }
  getCbDangCongTac(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/cbdangcongtac', data, permission);
  }
  getDqtv14Tac(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dqtv14tac', data, permission);
  }
  getSoTuyenHuyen(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/tqsotuyenhuyen', data, permission);
  }
  getSoTuyenTinh(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/tqsotuyentinh', data, permission);
  }
  getKhamTuyen(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/tqkhamtuyen', data, permission);
  }
  getKhamTuyenKetQua(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/tqkhamtuyenketqua', data, permission);
  }
  getDbdvM665(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dbdvm665', data, permission);
  }
  getDbdvM650(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dbdvm650', data, permission);
  }
  getDbdvM662a(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dbdvm662a', data, permission);
  }
  getPhuLuc3(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/phuluc3', data, permission);
  }
  get12Hl(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/bchqs12hl', data, permission);
  }
  get13Hl(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/bchqs13hl', data, permission);
  }
  getDbdvSapXep(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/dbdvsapxep', data, permission);
  }
  getSqdb704a(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/sqdb704a', data, permission);
  }
  getSqdb704aMau2P1(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/sqdb704amau2p1', data, permission);
  }
  getSqdb704aMau2P12(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/sqdb704amau2p12', data, permission);
  }
  getSqdb704aMau2P2(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/sqdb704amau2p2', data, permission);
  }
  getSqdb704aMau2DP(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/sqdb704amau2dp', data, permission);
  }
  getSqdb704aMau3(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/sqdb704amau3', data, permission);
  }
  getSqdb704aMau4(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/sqdb704amau4', data, permission);
  }
  getSqdb704aMau2Cnqs(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/sqdb704amau2cnqs', data, permission);
  }
  getQndbThucLuc(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/qndbthucluc', data, permission);
  }
  getQndbTangGiam(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/qndbtanggiam', data, permission);
  }
  getQndb711(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/qndb711', data, permission);
  }
  getPtkt801Qp(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/ptkt801qp', data, permission);
  }
  getPtktM04(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/ptktm04', data, permission);
  }
  getPtkt808TW(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/ptkt808tw', data, permission);
  }
  getPtkt808DP(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/ptkt808dp', data, permission);
  }
  getPtktM11(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/ptktm11', data, permission);
  }
  getQpan01(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/qpan01', data, permission);
  }
  getQpan02(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/qpan02', data, permission);
  }
  getQpan03(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/qpan03', data, permission);
  }
  getQpan04(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/qpan04', data, permission);
  }
  getQpan05(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/qpan05', data, permission);
  }
  getQpanPhuLuc1(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/qpan-phuluc1', data, permission);
  }
  getBaoCao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('export/getbaocao', data, permission);
  }
}
