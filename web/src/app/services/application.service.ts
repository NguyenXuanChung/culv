import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  //#region App
  // Retrieve data with condition in body
  getApp(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/applications/get', data, permission);
  }
  // Create a new item
  insertApp(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/applications/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editApp(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/applications/update', data, permission);
  }
  // Delete a item with id
  deleteApp(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/applications/delete', data, permission);
  }
  //#endregion
   //#region Version
  // Retrieve data with condition in body
  getVersion(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/application-versions/get', data, permission);
  }
  // Create a new item
  insertVersion(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/applications-versions/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editVersion(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/applications-versions/update', data, permission);
  }
  // Delete a item with id
  deleteVersion(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/applications-versions/delete', data, permission);
  }
  //#endregion
  checkPackage(data: any, permission: any): Observable<any> {
    return this.httpService.postFile('mdm/applications/checkpackage', data, permission);
  }
}
