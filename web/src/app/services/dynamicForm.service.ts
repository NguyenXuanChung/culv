import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class DynamicFormService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  getPermission(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dynamic-form/get-permission', data, permission);
  }

  getFormLayout(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dynamic-form/get-form-layout', data, permission);
  }

  getFormField(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dynamic-form/get-form-field', data, permission);
  }

  getFormData(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dynamic-form/get-form-data', data, permission);
  }

  getFormInput(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dynamic-form/get-form-input', data, permission);
  }

  getDataRow(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dynamic-form/get-data-row', data, permission);
  }

  insertFormData(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dynamic-form/insert-form-data', data, permission);
  }

  updateFormData(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dynamic-form/update-form-data', data, permission);
  }

  deleteFormData(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dynamic-form/delete-form-data', data, permission);
  }

  updateSelectedColumn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dynamic-form/update-selected-column', data, permission);
  }

  createUpdateDeleteForm(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dynamic-form/create-update-delete-form', data, permission);
  }

  // Export File
  exportTableToExcel(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('dynamic-form/table/export-excel', data, permission);
  }
}
