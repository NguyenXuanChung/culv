import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class TqDqtvService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }


  getNguonHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nguonhoc/get', data, permission);
  }
  insertNguonHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nguonhoc/insert', data, permission);
  }
  editNguonHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nguonhoc/update', data, permission);
  }
  deleteNguonHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nguonhoc/delete', data, permission);
  }
  getHeDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hedaotao/get', data, permission);
  }
  insertHeDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hedaotao/insert', data, permission);
  }
  editHeDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hedaotao/update', data, permission);
  }
  deleteHeDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hedaotao/delete', data, permission);
  }
  getCapQuanLy(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capquanly/get', data, permission);
  }
  insertCapQuanLy(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capquanly/insert', data, permission);
  }
  editCapQuanLy(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capquanly/update', data, permission);
  }
  deleteCapQuanLy(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capquanly/delete', data, permission);
  }
  getSapXepCanBo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-sapxepcanbo/get', data, permission);
  }
  insertSapXepCanBo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-sapxepcanbo/insert', data, permission);
  }
  editSapXepCanBo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-sapxepcanbo/update', data, permission);
  }
  deleteSapXepCanBo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-sapxepcanbo/delete', data, permission);
  }
  //#region
  getCapUyDang(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capuydang/get', data, permission);
  }
  insertCapUyDang(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capuydang/insert', data, permission);
  }
  editCapUyDang(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capuydang/update', data, permission);
  }
  deleteCapUyDang(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capuydang/delete', data, permission);
  }
  //#endregion
  //#region QndbNhomNganh
  getQndbNhomNganh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-nhomnganh/get', data, permission);
  }
  insertQndbNhomNganh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-nhomnganh/insert', data, permission);
  }
  editQndbNhomNganh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-nhomnganh/update', data, permission);
  }
  deleteQndbNhomNganh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-nhomnganh/delete', data, permission);
  }
  //#endregion
  //#region QndbNganh
  getQndbNganh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-nganh/get', data, permission);
  }
  insertQndbNganh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-nganh/insert', data, permission);
  }
  editQndbNganh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-nganh/update', data, permission);
  }
  deleteQndbNganh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-nganh/delete', data, permission);
  }
  //#endregion
  //#region QndbViTriChuyenMon
  getQndbViTriChuyenMon(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-vitrichuyenmon/get', data, permission);
  }
  insertQndbViTriChuyenMon(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-vitrichuyenmon/insert', data, permission);
  }
  editQndbViTriChuyenMon(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-vitrichuyenmon/update', data, permission);
  }
  deleteQndbViTriChuyenMon(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-vitrichuyenmon/delete', data, permission);
  }
  //type du bi
  getQndbTypeDuBi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-typedubi/get', data, permission);
  }
  insertQndbTypeDuBi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-typedubi/insert', data, permission);
  }
  editQndbTypeDuBi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-typedubi/update', data, permission);
  }
  deleteQndbTypeDuBi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-typedubi/delete', data, permission);
  }
  //phan loai dao tao
  getQndbPhanLoaiDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-phanloaidaotao/getList', data, permission);
  }
  insertQndbPhanLoaiDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-phanloaidaotao/insert', data, permission);
  }
  editQndbTypePhanLoaiDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-phanloaidaotao/update', data, permission);
  }
  deleteQndbTypePhanLoaiDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/qndb-phanloaidaotao/delete', data, permission);
  }
  //#endregion
  //#region PhanLoaiPhuongTien
  getPhanLoaiPhuongTien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaiphuongtien/get', data, permission);
  }
  insertPhanLoaiPhuongTien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaiphuongtien/insert', data, permission);
  }
  editPhanLoaiPhuongTien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaiphuongtien/update', data, permission);
  }
  deletePhanLoaiPhuongTien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaiphuongtien/delete', data, permission);
  }
  //#endregion
  //#region ThanhPhanBanThan
  getThanhPhanBanThan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-thanhphanbanthan/get', data, permission);
  }
  insertThanhPhanBanThan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-thanhphanbanthan/insert', data, permission);
  }
  editThanhPhanBanThan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-thanhphanbanthan/update', data, permission);
  }
  deleteThanhPhanBanThan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-thanhphanbanthan/delete', data, permission);
  }
  //#endregion
  //#region ThanhPhanGiaDinh
  getThanhPhanGiaDinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-thanhphangiadinh/get', data, permission);
  }
  insertThanhPhanGiaDinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-thanhphangiadinh/insert', data, permission);
  }
  editThanhPhanGiaDinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-thanhphangiadinh/update', data, permission);
  }
  deleteThanhPhanGiaDinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-thanhphangiadinh/delete', data, permission);
  }
  //#endregion
  //#region LoaiPhuongTien
  getLoaiPhuongTien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaiphuongtien/get', data, permission);
  }
  insertLoaiPhuongTien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaiphuongtien/insert', data, permission);
  }
  editLoaiPhuongTien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaiphuongtien/update', data, permission);
  }
  deleteLoaiPhuongTien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaiphuongtien/delete', data, permission);
  }
  //#endregion
  //#region PhuongTien
  getPhuongTien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phuongtien/get', data, permission);
  }
  insertPhuongTien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phuongtien/insert', data, permission);
  }
  editPhuongTien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phuongtien/update', data, permission);
  }
  deletePhuongTien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phuongtien/delete', data, permission);
  }
  //#endregion
  //#region PhanCapKyThuat
  getPhanCapKyThuat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancapkythuat/get', data, permission);
  }
  insertPhanCapKyThuat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancapkythuat/insert', data, permission);
  }
  editPhanCapKyThuat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancapkythuat/update', data, permission);
  }
  deletePhanCapKyThuat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancapkythuat/delete', data, permission);
  }
  getDoiTuongDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-doituongdaotao/get', data, permission);
  }
  // DM PhanLoaiVuKhi
  getPhanLoaiVuKhi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaivukhi/get', data, permission);
  }
  insertPhanLoaiVuKhi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaivukhi/insert', data, permission);
  }
  editPhanLoaiVuKhi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaivukhi/update', data, permission);
  }
  deletePhanLoaiVuKhi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaivukhi/delete', data, permission);
  }
  // DM HangMuc
  getHangMuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hangmuc/get', data, permission);
  }
  insertHangMuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hangmuc/insert', data, permission);
  }
  editHangMuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hangmuc/update', data, permission);
  }
  deleteHangMuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hangmuc/delete', data, permission);
  }
  // DM VuKhi
  getVuKhi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-vukhi/get', data, permission);
  }
  insertVuKhi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-vukhi/insert', data, permission);
  }
  editVuKhi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-vukhi/update', data, permission);
  }
  deleteVuKhi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-vukhi/delete', data, permission);
  }
  // DM Chức danh
  getLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaidn/get', data, permission);
  }
  insertLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaidn/insert', data, permission);
  }
  editLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaidn/update', data, permission);
  }
  deleteLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaidn/delete', data, permission);
  }
  // DM Chức danh
  getPhanCap(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancap/get', data, permission);
  }
  insertPhanCap(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancap/insert', data, permission);
  }
  editPhanCap(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancap/update', data, permission);
  }
  deletePhanCap(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancap/delete', data, permission);
  }
  // DM Chức danh
  getPhanLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidn/get', data, permission);
  }
  insertPhanLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidn/insert', data, permission);
  }
  editPhanLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidn/update', data, permission);
  }
  deletePhanLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidn/delete', data, permission);
  }
  // DM Chức danh
  getTrucThuoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tructhuoc/get', data, permission);
  }
  insertTrucThuoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tructhuoc/insert', data, permission);
  }
  editTrucThuoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tructhuoc/update', data, permission);
  }
  deleteTrucThuoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tructhuoc/delete', data, permission);
  }
  // DM Quân hàm
  getQuanHam(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanham/get', data, permission);
  }
  insertQuanHam(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanham/insert', data, permission);
  }
  editQuanHam(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanham/update', data, permission);
  }
  deleteQuanHam(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanham/delete', data, permission);
  }
  // DMCapHanhChinh
  getCapHanhChinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-caphanhchinh/get', data, permission);
  }
  insertCapHanhChinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-caphanhchinh/insert', data, permission);
  }
  editCapHanhChinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-caphanhchinh/update', data, permission);
  }
  deleteCapHanhChinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-caphanhchinh/delete', data, permission);
  }
  // DMLoaiXa
  getLoaiXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaixa/get', data, permission);
  }
  insertLoaiXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaixa/insert', data, permission);
  }
  editLoaiXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaixa/update', data, permission);
  }
  deleteLoaiXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaixa/delete', data, permission);
  }
  // DMLucLuong
  getLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lucluong/get', data, permission);
  }
  insertLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lucluong/insert', data, permission);
  }
  editLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lucluong/update', data, permission);
  }
  deleteLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lucluong/delete', data, permission);
  }
  // DMPhanLoaiDiaLy
  getPhanLoaiDiaLy(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidialy/get', data, permission);
  }
  insertPhanLoaiDiaLy(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidialy/insert', data, permission);
  }
  editPhanLoaiDiaLy(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidialy/update', data, permission);
  }
  deletePhanLoaiDiaLy(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidialy/delete', data, permission);
  }
  // DMPhanLoaiLucLuong
  getPhanLoaiLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloailucluong/get', data, permission);
  }
  insertPhanLoaiLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloailucluong/insert', data, permission);
  }
  editPhanLoaiLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloailucluong/update', data, permission);
  }
  deletePhanLoaiLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloailucluong/delete', data, permission);
  }
  // DMPhanLoaiTrongDiem
  getPhanLoaiTrongDiem(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaitrongdiem/get', data, permission);
  }
  insertPhanLoaiTrongDiem(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaitrongdiem/insert', data, permission);
  }
  editPhanLoaiTrongDiem(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaitrongdiem/update', data, permission);
  }
  deletePhanLoaiTrongDiem(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaitrongdiem/delete', data, permission);
  }
  // DMquyMoToChuc
  getQuyMoToChuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quymotochuc/get', data, permission);
  }
  insertQuyMoToChuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quymotochuc/insert', data, permission);
  }
  editQuyMoToChuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quymotochuc/update', data, permission);
  }
  deleteQuyMoToChuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quymotochuc/delete', data, permission);
  }

  // DM cấp bậc
  getCapBac(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capbac/get', data, permission);
  }
  insertCapBac(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capbac/insert', data, permission);
  }
  editCapBac(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capbac/update', data, permission);
  }
  deleteCapBac(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capbac/delete', data, permission);
  }
  // DM đơn vị
  getDonViDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvidbdv/get', data, permission);
  }
  insertDonViDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvidbdv/insert', data, permission);
  }
  editDonViDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvidbdv/update', data, permission);
  }
  deleteDonViDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvidbdv/delete', data, permission);
  }
  // DM đơn vị dự bị động viên bộ
  getDonViDbdvbo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvidbdvbo/get', data, permission);
  }
  insertDonViDbdvbo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvidbdvbo/insert', data, permission);
  }
  editDonViDbdvbo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvidbdvbo/update', data, permission);
  }
  deleteDonViDbdvbo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvidbdvbo/delete', data, permission);
  }
  // DM phan cap don vi du bi dong vien
  getPhanCapDonViDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancapdonvidbdv/get', data, permission);
  }
  insertPhanCapDonViDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancapdonvidbdv/insert', data, permission);
  }
  editPhanCapDonViDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancapdonvidbdv/update', data, permission);
  }
  deletePhanCapDonViDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancapdonvidbdv/delete', data, permission);
  }
  // DM đơn vị
  getDonViTv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvitv/getlist', data, permission);
  }
  getAllDonViTv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvitv/get', data, permission);
  }
  getListDonViTv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvitv/getlist', data, permission);
  }
  insertDonViTv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvitv/insert', data, permission);
  }
  editDonViTv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvitv/update', data, permission);
  }
  deleteDonViTv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvitv/delete', data, permission);
  }
  // DM Chức danh
  getChucDanh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-chucdanh/get', data, permission);
  }
  insertChucDanh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-chucdanh/insert', data, permission);
  }
  editChucDanh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-chucdanh/update', data, permission);
  }
  deleteChucDanh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-chucdanh/delete', data, permission);
  }
  // DM Đối tượng
  getDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-doituong/get', data, permission);
  }
  insertDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-doituong/insert', data, permission);
  }
  editDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-doituong/update', data, permission);
  }
  deleteDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-doituong/delete', data, permission);
  }
  // Nang khieu
  getNangKhieu(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nangkhieu/get', data, permission);
  }
  insertNangKhieu(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nangkhieu/insert', data, permission);
  }
  editNangKhieu(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nangkhieu/update', data, permission);
  }
  deleteNangKhieu(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nangkhieu/delete', data, permission);
  }
  // Nghề Nghiệp
  getNgheNghiep(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nghenghiep/get', data, permission);
  }
  insertNgheNghiep(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nghenghiep/insert', data, permission);
  }
  editNgheNghiep(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nghenghiep/update', data, permission);
  }
  deleteNgheNghiep(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nghenghiep/delete', data, permission);
  }
  // Sức Khỏe
  getSucKhoe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-suckhoe/get', data, permission);
  }
  insertSucKhoe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-suckhoe/insert', data, permission);
  }
  editSucKhoe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-suckhoe/update', data, permission);
  }
  deleteSucKhoe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-suckhoe/delete', data, permission);
  }
  // Lý do
  getLyDo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lydo/get', data, permission);
  }
  insertLyDo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lydo/insert', data, permission);
  }
  editLyDo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lydo/update', data, permission);
  }
  deleteLyDo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lydo/delete', data, permission);
  }
  // Lý do theo qua trinh
  getLyDoTheoQuaTrinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lydotheoquatrinh/get', data, permission);
  }
  insertLyDoTheoQuaTrinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lydotheoquatrinh/insert', data, permission);
  }
  editLyDoTheoQuaTrinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lydotheoquatrinh/update', data, permission);
  }
  deleteLyDoTheoQuaTrinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lydotheoquatrinh/delete', data, permission);
  }
  // tổ chức xã hội
  getToChucXaHoi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tochucxahoi/get', data, permission);
  }
  insertToChucXaHoi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tochucxahoi/insert', data, permission);
  }
  editToChucXaHoi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tochucxahoi/update', data, permission);
  }
  deleteToChucXaHoi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tochucxahoi/delete', data, permission);
  }
  // trình độ học vấn
  getTrinhDoHocVan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdohocvan/get', data, permission);
  }
  insertTrinhDoHocVan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdohocvan/insert', data, permission);
  }
  editTrinhDoHocVan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdohocvan/update', data, permission);
  }
  deleteTrinhDoHocVan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdohocvan/delete', data, permission);
  }
  // tạm hoãn
  getTamHoan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tamhoan/get', data, permission);
  }
  insertTamHoan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tamhoan/insert', data, permission);
  }
  editTamHoan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tamhoan/update', data, permission);
  }
  deleteTamHoan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tamhoan/delete', data, permission);
  }
  // trình độ văn hóa
  getTrinhDoVanHoa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdovanhoa/get', data, permission);
  }
  insertTrinhDoVanHoa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdovanhoa/insert', data, permission);
  }
  editTrinhDoVanHoa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdovanhoa/update', data, permission);
  }
  deleteTrinhDoVanHoa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdovanhoa/delete', data, permission);
  }
  // học vị
  getHocVi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hocvi/get', data, permission);
  }
  insertHocVi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hocvi/insert', data, permission);
  }
  editHocVi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hocvi/update', data, permission);
  }
  deleteHocVi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hocvi/delete', data, permission);
  }
  // công cụ hỗ trợ
  getCongCuHoTro(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-congcuhotro/get', data, permission);
  }
  insertCongCuHoTro(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-congcuhotro/insert', data, permission);
  }
  editCongCuHoTro(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-congcuhotro/update', data, permission);
  }
  deleteCongCuHoTro(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-congcuhotro/delete', data, permission);
  }
  // quan hệ
  getQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanhe/get', data, permission);
  }
  insertQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanhe/insert', data, permission);
  }
  editQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanhe/update', data, permission);
  }
  deleteQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanhe/delete', data, permission);
  }
  // ngành nghề cmkt
  getNganhNgheCmkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nganhnghecmkt/get', data, permission);
  }
  insertNganhNgheCmkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nganhnghecmkt/insert', data, permission);
  }
  editNganhNgheCmkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nganhnghecmkt/update', data, permission);
  }
  deleteNganhNgheCmkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nganhnghecmkt/delete', data, permission);
  }
   // nhóm ngành nghề cmkt
   getNhomNganhNgheCmkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nhomnganhnghecmkt/get', data, permission);
  }
  insertNhomNganhNgheCmkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nhomnganhnghecmkt/insert', data, permission);
  }
  editNhomNganhNgheCmkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nhomnganhnghecmkt/update', data, permission);
  }
  deleteNhomNganhNgheCmkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nhomnganhnghecmkt/delete', data, permission);
  }
  // miễn
  getMien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-mien/get', data, permission);
  }
  insertMien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-mien/insert', data, permission);
  }
  editMien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-mien/update', data, permission);
  }
  deleteMien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-mien/delete', data, permission);
  }
  // dân tộc
  getDanToc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-dantoc/get', data, permission);
  }
  insertDanToc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-dantoc/insert', data, permission);
  }
  editDanToc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-dantoc/update', data, permission);
  }
  deleteDanToc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-dantoc/delete', data, permission);
  }
  // tôn giáo
  getTonGiao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tongiao/get', data, permission);
  }
  insertTonGiao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tongiao/insert', data, permission);
  }
  editTonGiao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tongiao/update', data, permission);
  }
  deleteTonGiao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tongiao/delete', data, permission);
  }
}
