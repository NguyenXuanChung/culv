import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class GdQpanService {
  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }
  //trường học
  getTruongHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-truonghoc/get', data, permission);
  }
  insertTruongHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-truonghoc/insert', data, permission);
  }
  editTruongHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-truonghoc/update', data, permission);
  }
  deleteTruongHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-truonghoc/delete', data, permission);
  }
  // Khối
  getKhoi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-khoi/get', data, permission);
  }
  insertKhoi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-khoi/insert', data, permission);
  }
  editKhoi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-khoi/update', data, permission);
  }
  deleteKhoi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-khoi/delete', data, permission);
  }
  //đối tượng
  getDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-doituong/get', data, permission);
  }
  insertDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-doituong/insert', data, permission);
  }
  editDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-doituong/update', data, permission);
  }
  deleteDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-doituong/delete', data, permission);
  }
  // Cấp đào tạo
  getCapDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-capdaotao/get', data, permission);
  }
  insertCapDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-capdaotao/insert', data, permission);
  }
  editCapDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-capdaotao/update', data, permission);
  }
  deleteCapDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-capdaotao/delete', data, permission);
  }
  //phân loại đối tượng
  getPhanLoaiDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-phanloaidoituong/get', data, permission);
  }
  insertPhanLoaiDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-phanloaidoituong/insert', data, permission);
  }
  editPhanLoaiDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-phanloaidoituong/update', data, permission);
  }
  deletePhanLoaiDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-phanloaidoituong/delete', data, permission);
  }
  //phân loại lớp học
  getPhanLoaiLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-phanloailophoc/get', data, permission);
  }
  insertPhanLoaiLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-phanloailophoc/insert', data, permission);
  }
  editPhanLoaiLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-phanloailophoc/update', data, permission);
  }
  deletePhanLoaiLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-phanloailophoc/delete', data, permission);
  }
  //ket qua
  getKetQua(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-ketqua/get', data, permission);
  }
  insertKetQua(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-ketqua/insert', data, permission);
  }
  editKetQua(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-ketqua/update', data, permission);
  }
  deleteKetQua(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-ketqua/delete', data, permission);
  }
  //hình thức đào tạo
  getHinhThucDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-hinhthucdaotao/get', data, permission);
  }
  insertHinhThucDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-hinhthucdaotao/insert', data, permission);
  }
  editHinhThucDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-hinhthucdaotao/update', data, permission);
  }
  deleteHinhThucDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-hinhthucdaotao/delete', data, permission);
  }
  //lý luận chính trị
  getLyLuanChinhTri(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-lyluanchinhtri/get', data, permission);
  }
  insertLyLuanChinhTri(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-lyluanchinhtri/insert', data, permission);
  }
  editLyLuanChinhTri(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-lyluanchinhtri/update', data, permission);
  }
  deleteLyLuanChinhTri(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-lyluanchinhtri/delete', data, permission);
  }
  //xếp loại lớp học
  getXepLoaiLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-xeploailophoc/get', data, permission);
  }
  insertXepLoaiLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-xeploailophoc/insert', data, permission);
  }
  editXepLoaiLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-xeploailophoc/update', data, permission);
  }
  deleteXepLoaiLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-xeploailophoc/delete', data, permission);
  }
  //danh sach tham gia dao tao
  checkCMND(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/danhsachthamgiadaotao/checkCMND', data, permission);
  }
  getDanhSachXetDuyet(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/danhsachthamgiadaotao/danhSachXetDuyet', data, permission);
  }
  getDanhSachThamGiaDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/danhsachthamgiadaotao/getList', data, permission);
  }
  insertDanhSachThamGiaDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/danhsachthamgiadaotao/insert', data, permission);
  }
  editDanhSachThamGiaDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/danhsachthamgiadaotao/update', data, permission);
  }
  deleteDanhSachThamGiaDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/danhsachthamgiadaotao/delete', data, permission);
  }
  //quản lý giáo viên
  getQuanLyGiaoVien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/quanlygiaovien/getList', data, permission);
  }
  saveQuanLyGiaoVien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/quanlygiaovien/saveByThang', data, permission);
  }
  editQuanLyGiaoVien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/quanlygiaovien/update', data, permission);
  }
  deleteQuanLyGiaoVien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/quanlygiaovien/delete', data, permission);
  }
  //đào tạo theo lớp học
  getDaoTaoTheoLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/lophoc/getlist', data, permission);
  }
  insertDaoTaoTheoLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/lophoc/insert', data, permission);
  }
  editDaoTaoTheoLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/lophoc/update', data, permission);
  }
  deleteDaoTaoTheoLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/lophoc/delete', data, permission); 
  }
  //giáo dục toàn dân
  getGDToanDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/gdtoandan/getlist', data, permission);
  }
  saveGDToanDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/gdtoandan/saveThongTinTheoThang', data, permission);
  }
  editGDToanDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/gdtoandan/update', data, permission);
  }
  deleteGDToanDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/gdtoandan/delete', data, permission);
  }
  //phân loại trường học
  getPhanLoaiTruongHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-phanloaitruonghoc/get', data, permission);
  }
  insertPhanLoaiTruongHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-phanloaitruonghoc/insert', data, permission);
  }
  editPhanLoaiTruongHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-phanloaitruonghoc/update', data, permission);
  }
  deletePhanLoaiTruongHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/dm-phanloaitruonghoc/delete', data, permission);
  }
  //Quản lý kết quả đào tạo của công dân theo từng lớp học
  getKetQuaCongDanTheoLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/ketquacongdantheolophoc/getList', data, permission);
  }
  insertKetQuaCongDanTheoLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/ketquacongdantheolophoc/insert', data, permission);
  }
  editKetQuaCongDanTheoLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/ketquacongdantheolophoc/update', data, permission);
  }
  deleteKetQuaCongDanTheoLopHoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qpan/ketquacongdantheolophoc/delete', data, permission);
  }
}
