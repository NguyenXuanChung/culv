import { Injectable } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { ReplaySubject } from 'rxjs';
import { take } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
@Injectable({ providedIn: 'root' })
export class CommonService {
  language$ = new ReplaySubject<LangChangeEvent>(1);
  translate = this.translateService;
  public Login: any = {
    View: { 'code': 'Login', 'action': '_view' },
  };
  public Portal_Info: any = {
    View: { 'code': 'Portal_Info', 'action': '_view' },
    Update: { 'code': 'Portal_Info', 'action': '_update' },
  };
  public TTBH: any = {
    View: { 'code': 'TTBH', 'action': '_view' },
    Update: { 'code': 'TTBH', 'action': '_update' },
  };
  public TTBH_NhanTho: any = {
    View: { 'code': 'TTBH_NhanTho', 'action': '_view' },
    Update: { 'code': 'TTBH_NhanTho', 'action': '_update' },
  };
  public TTBH_XaHoi: any = {
    View: { 'code': 'TTBH_XaHoi', 'action': '_view' },
    Update: { 'code': 'TTBH_XaHoi', 'action': '_update' },
  };
  public TTBH_HealthCare: any = {
    View: { 'code': 'TTBH_HealthCare', 'action': '_view' },
    Update: { 'code': 'TTBH_HealthCare', 'action': '_update' },
  };
  public VB: any = {
    View: { 'code': 'VB', 'action': '_view' },
    Update: { 'code': 'VB', 'action': '_update' },
  };
  public VB_VanBanQuyChe: any = {
    View: { 'code': 'VB_VanBanQuyChe', 'action': '_view' },
    Update: { 'code': 'VB_VanBanQuyChe', 'action': '_update' },
  };
  public Luong: any = {
    View: { 'code': 'Luong', 'action': '_view' },
    Update: { 'code': 'Luong', 'action': '_update' },
  };
  public ChamCong: any = {
    View: { 'code': 'ChamCong', 'action': '_view' },
    Update: { 'code': 'ChamCong', 'action': '_update' },
  };
  public DangKyNghi: any = {
    View: { 'code': 'DangKyNghi', 'action': '_view' },
    Update: { 'code': 'DangKyNghi', 'action': '_update' },
  };
  public PheDuyetChamCong: any = {
    View: { 'code': 'PheDuyetChamCong', 'action': '_view' },
    Update: { 'code': 'PheDuyetChamCong', 'action': '_update' },
  };
  public Luong_LuongCoBan: any = {
    View: { 'code': 'Luong_LuongCoBan', 'action': '_view' },
    Update: { 'code': 'Luong_LuongCoBan', 'action': '_update' },
  };
  public Luong_LuongThoaThuan: any = {
    View: { 'code': 'Luong_LuongThoaThuan', 'action': '_view' },
    Update: { 'code': 'Luong_LuongThoaThuan', 'action': '_update' },
  };
  public Luong_HoTro: any = {
    View: { 'code': 'Luong_HoTro', 'action': '_view' },
    Update: { 'code': 'Luong_HoTro', 'action': '_update' },
  };
  public ALL: any = {
    View: { 'code': 'ALL', 'action': '_view' },
    Export: { 'code': 'ALL', 'action': '_export' },
    Insert: { 'code': 'ALL', 'action': '_insert' },
    Update: { 'code': 'ALL', 'action': '_update' },
    Delete: { 'code': 'ALL', 'action': '_delete' },
  };
  public QTHT: any = {
    View: { 'code': 'QTHT', 'action': '_view' },
    Export: { 'code': 'QTHT', 'action': '_export' },
    Insert: { 'code': 'QTHT', 'action': '_insert' },
    Update: { 'code': 'QTHT', 'action': '_update' },
    Delete: { 'code': 'QTHT', 'action': '_delete' },
  };
  public HT_QuanLyNguoiDung: any = {
    View: { 'code': 'HT_QuanLyNguoiDung', 'action': '_view' },
    Export: { 'code': 'HT_QuanLyNguoiDung', 'action': '_export' },
    Insert: { 'code': 'HT_QuanLyNguoiDung', 'action': '_insert' },
    Update: { 'code': 'HT_QuanLyNguoiDung', 'action': '_update' },
    Delete: { 'code': 'HT_QuanLyNguoiDung', 'action': '_delete' },
  };
  public HT_QuanLyQuyen: any = {
    View: { 'code': 'HT_QuanLyQuyen', 'action': '_view' },
    Export: { 'code': 'HT_QuanLyQuyen', 'action': '_export' },
    Insert: { 'code': 'HT_QuanLyQuyen', 'action': '_insert' },
    Update: { 'code': 'HT_QuanLyQuyen', 'action': '_update' },
    Delete: { 'code': 'HT_QuanLyQuyen', 'action': '_delete' },
  };
  public HT_QuanLyNhomQuyen: any = {
    View: { 'code': 'HT_QuanLyNhomQuyen', 'action': '_view' },
    Export: { 'code': 'HT_QuanLyNhomQuyen', 'action': '_export' },
    Insert: { 'code': 'HT_QuanLyNhomQuyen', 'action': '_insert' },
    Update: { 'code': 'HT_QuanLyNhomQuyen', 'action': '_update' },
    Delete: { 'code': 'HT_QuanLyNhomQuyen', 'action': '_delete' },
  };
  public HT_ThamSoHeThong: any = {
    View: { 'code': 'HT_ThamSoHeThong', 'action': '_view' },
    Export: { 'code': 'HT_ThamSoHeThong', 'action': '_export' },
    Insert: { 'code': 'HT_ThamSoHeThong', 'action': '_insert' },
    Update: { 'code': 'HT_ThamSoHeThong', 'action': '_update' },
    Delete: { 'code': 'HT_ThamSoHeThong', 'action': '_delete' },
  };
  public HT_NhatKyHeThong: any = {
    View: { 'code': 'HT_NhatKyHeThong', 'action': '_view' },
    Export: { 'code': 'HT_NhatKyHeThong', 'action': '_export' },
    Insert: { 'code': 'HT_NhatKyHeThong', 'action': '_insert' },
    Update: { 'code': 'HT_NhatKyHeThong', 'action': '_update' },
    Delete: { 'code': 'HT_NhatKyHeThong', 'action': '_delete' },
  };
  public TQ_CongDan: any = {
    View: { 'code': 'TQ_CongDan', 'action': '_view' },
    Export: { 'code': 'TQ_CongDan', 'action': '_export' },
    Insert: { 'code': 'TQ_CongDan', 'action': '_insert' },
    Update: { 'code': 'TQ_CongDan', 'action': '_update' },
    Delete: { 'code': 'TQ_CongDan', 'action': '_delete' },
  };
  public TQ_SSNN: any = {
    View: { 'code': 'TQ_SSNN', 'action': '_view' },
    Export: { 'code': 'TQ_SSNN', 'action': '_export' },
    Insert: { 'code': 'TQ_SSNN', 'action': '_insert' },
    Update: { 'code': 'TQ_SSNN', 'action': '_update' },
    Delete: { 'code': 'TQ_SSNN', 'action': '_delete' },
  };
  // đợt tuyển quân
  public TQ_DotTuyenQuan: any = {
    View: { 'code': 'TQ_DotTuyenQuan', 'action': '_view' },
    Export: { 'code': 'TQ_DotTuyenQuan', 'action': '_export' },
    Insert: { 'code': 'TQ_DotTuyenQuan', 'action': '_insert' },
    Update: { 'code': 'TQ_DotTuyenQuan', 'action': '_update' },
    Delete: { 'code': 'TQ_DotTuyenQuan', 'action': '_delete' },
  };
  public TQ_DonVidbdv: any = {
    View: { 'code': 'TQ_DonVidbdv', 'action': '_view' },
    Export: { 'code': 'TQ_DonVidbdv', 'action': '_export' },
    Insert: { 'code': 'TQ_DonVidbdv', 'action': '_insert' },
    Update: { 'code': 'TQ_DonVidbdv', 'action': '_update' },
    Delete: { 'code': 'TQ_DonVidbdv', 'action': '_delete' },
  };
  public TQ_XetDuyetXa: any = {
    View: { 'code': 'TQ_XetDuyetXa', 'action': '_view' },
    Export: { 'code': 'TQ_XetDuyetXa', 'action': '_export' },
    Insert: { 'code': 'TQ_XetDuyetXa', 'action': '_insert' },
    Update: { 'code': 'TQ_XetDuyetXa', 'action': '_update' },
    Delete: { 'code': 'TQ_XetDuyetXa', 'action': '_delete' },
  };
  public TQ_XetDuyetHuyen: any = {
    View: { 'code': 'TQ_XetDuyetHuyen', 'action': '_view' },
    Export: { 'code': 'TQ_XetDuyetHuyen', 'action': '_export' },
    Insert: { 'code': 'TQ_XetDuyetHuyen', 'action': '_insert' },
    Update: { 'code': 'TQ_XetDuyetHuyen', 'action': '_update' },
    Delete: { 'code': 'TQ_XetDuyetHuyen', 'action': '_delete' },
  };
  public TQ_NhapNgu: any = {
    View: { 'code': 'TQ_NhapNgu', 'action': '_view' },
    Export: { 'code': 'TQ_NhapNgu', 'action': '_export' },
    Insert: { 'code': 'TQ_NhapNgu', 'action': '_insert' },
    Update: { 'code': 'TQ_NhapNgu', 'action': '_update' },
    Delete: { 'code': 'TQ_NhapNgu', 'action': '_delete' },
  };
  public TQ_DQTV_ChucDanh: any = {
    View: { 'code': 'TQ_DQTV_ChucDanh', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_ChucDanh', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_ChucDanh', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_ChucDanh', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_ChucDanh', 'action': '_delete' },
  };
  public DiaBan: any = {
    View: { 'code': 'DiaBan', 'action': '_view' },
    Export: { 'code': 'DiaBan', 'action': '_export' },
    Insert: { 'code': 'DiaBan', 'action': '_insert' },
    Update: { 'code': 'DiaBan', 'action': '_update' },
    Delete: { 'code': 'DiaBan', 'action': '_delete' },
  };
  public Configuration: any = {
    View: { 'code': 'Configuration', 'action': '_view' },
    Export: { 'code': 'Configuration', 'action': '_export' },
    Insert: { 'code': 'Configuration', 'action': '_insert' },
    Update: { 'code': 'Configuration', 'action': '_update' },
    Delete: { 'code': 'Configuration', 'action': '_delete' },
  };
  public Group: any = {
    View: { 'code': 'Group', 'action': '_view' },
    Export: { 'code': 'Group', 'action': '_export' },
    Insert: { 'code': 'Group', 'action': '_insert' },
    Update: { 'code': 'Group', 'action': '_update' },
    Delete: { 'code': 'Group', 'action': '_delete' },
  };
  public Application: any = {
    View: { 'code': 'Application', 'action': '_view' },
    Export: { 'code': 'Application', 'action': '_export' },
    Insert: { 'code': 'Application', 'action': '_insert' },
    Update: { 'code': 'Application', 'action': '_update' },
    Delete: { 'code': 'Application', 'action': '_delete' },
  };
  public Location: any = {
    View: { 'code': 'Location', 'action': '_view' },
    Export: { 'code': 'Location', 'action': '_export' },
    Insert: { 'code': 'Location', 'action': '_insert' },
    Update: { 'code': 'Location', 'action': '_update' },
    Delete: { 'code': 'Location', 'action': '_delete' },
  };
  public DM_Tinh_Huyen_Xa: any = {
    View: { 'code': 'DM_Tinh_Huyen_Xa', 'action': '_view' },
    Export: { 'code': 'DM_Tinh_Huyen_Xa', 'action': '_export' },
    Insert: { 'code': 'DM_Tinh_Huyen_Xa', 'action': '_insert' },
    Update: { 'code': 'DM_Tinh_Huyen_Xa', 'action': '_update' },
    Delete: { 'code': 'DM_Tinh_Huyen_Xa', 'action': '_delete' },
  };
  public QLTB_ThongBao: any = {
    View: { 'code': 'QLTB_ThongBao', 'action': '_view' },
    Export: { 'code': 'QLTB_ThongBao', 'action': '_export' },
    Insert: { 'code': 'QLTB_ThongBao', 'action': '_insert' },
    Update: { 'code': 'QLTB_ThongBao', 'action': '_update' },
    Delete: { 'code': 'QLTB_ThongBao', 'action': '_delete' },
  };
  public DM_TrangThaiPhat: any = {
    View: { 'code': 'DM_TrangThaiPhat', 'action': '_view' },
    Export: { 'code': 'DM_TrangThaiPhat', 'action': '_export' },
    Insert: { 'code': 'DM_TrangThaiPhat', 'action': '_insert' },
    Update: { 'code': 'DM_TrangThaiPhat', 'action': '_update' },
    Delete: { 'code': 'DM_TrangThaiPhat', 'action': '_delete' },
  };
  public DM_CheDoPhat: any = {
    View: { 'code': 'DM_CheDoPhat', 'action': '_view' },
    Export: { 'code': 'DM_CheDoPhat', 'action': '_export' },
    Insert: { 'code': 'DM_CheDoPhat', 'action': '_insert' },
    Update: { 'code': 'DM_CheDoPhat', 'action': '_update' },
    Delete: { 'code': 'DM_CheDoPhat', 'action': '_delete' },
  };
  public DM_Kenh: any = {
    View: { 'code': 'DM_Kenh', 'action': '_view' },
    Export: { 'code': 'DM_Kenh', 'action': '_export' },
    Insert: { 'code': 'DM_Kenh', 'action': '_insert' },
    Update: { 'code': 'DM_Kenh', 'action': '_update' },
    Delete: { 'code': 'DM_Kenh', 'action': '_delete' },
  };
  public DM_KieuPhat: any = {
    View: { 'code': 'DM_KieuPhat', 'action': '_view' },
    Export: { 'code': 'DM_KieuPhat', 'action': '_export' },
    Insert: { 'code': 'DM_KieuPhat', 'action': '_insert' },
    Update: { 'code': 'DM_KieuPhat', 'action': '_update' },
    Delete: { 'code': 'DM_KieuPhat', 'action': '_delete' },
  };
  public DM_KieuLap: any = {
    View: { 'code': 'DM_KieuLap', 'action': '_view' },
    Export: { 'code': 'DM_KieuLap', 'action': '_export' },
    Insert: { 'code': 'DM_KieuLap', 'action': '_insert' },
    Update: { 'code': 'DM_KieuLap', 'action': '_update' },
    Delete: { 'code': 'DM_KieuLap', 'action': '_delete' },
  };
  public DM_LoaiThongBao: any = {
    View: { 'code': 'DM_LoaiThongBao', 'action': '_view' },
    Export: { 'code': 'DM_LoaiThongBao', 'action': '_export' },
    Insert: { 'code': 'DM_LoaiThongBao', 'action': '_insert' },
    Update: { 'code': 'DM_LoaiThongBao', 'action': '_update' },
    Delete: { 'code': 'DM_LoaiThongBao', 'action': '_delete' },
  };
  public DM_LoaiTin: any = {
    View: { 'code': 'DM_LoaiTin', 'action': '_view' },
    Export: { 'code': 'DM_LoaiTin', 'action': '_export' },
    Insert: { 'code': 'DM_LoaiTin', 'action': '_insert' },
    Update: { 'code': 'DM_LoaiTin', 'action': '_update' },
    Delete: { 'code': 'DM_LoaiTin', 'action': '_delete' },
  };
  public DM_TrangThaiDuyet: any = {
    View: { 'code': 'DM_TrangThaiDuyet', 'action': '_view' },
    Export: { 'code': 'DM_TrangThaiDuyet', 'action': '_export' },
    Insert: { 'code': 'DM_TrangThaiDuyet', 'action': '_insert' },
    Update: { 'code': 'DM_TrangThaiDuyet', 'action': '_update' },
    Delete: { 'code': 'DM_TrangThaiDuyet', 'action': '_delete' },
  };
  // đối tượng
  public TDV_DXBoNhiem: any = {
    View: { 'code': 'TDV_DXBoNhiem', 'action': '_view' },
    Export: { 'code': 'TDV_DXBoNhiem', 'action': '_export' },
    Insert: { 'code': 'TDV_DXBoNhiem', 'action': '_insert' },
    Update: { 'code': 'TDV_DXBoNhiem', 'action': '_update' },
    Delete: { 'code': 'TDV_DXBoNhiem', 'action': '_delete' },
  };
  // đối tượng
  public TQ_DQTV_DoiTuong: any = {
    View: { 'code': 'TQ_DQTV_DoiTuong', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_DoiTuong', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_DoiTuong', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_DoiTuong', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_DoiTuong', 'action': '_delete' },
  };
  // nang khieu
  public TQ_DQTV_NangKhieu: any = {
    View: { 'code': 'TQ_DQTV_NangKhieu', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_NangKhieu', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_NangKhieu', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_NangKhieu', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_NangKhieu', 'action': '_delete' },
  };
  // nghề nghiệp
  public TQ_DQTV_NgheNghiep: any = {
    View: { 'code': 'TQ_DQTV_NgheNghiep', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_NgheNghiep', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_NgheNghiep', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_NgheNghiep', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_NgheNghiep', 'action': '_delete' },
  };
  // ly do
  public TQ_DQTV_LyDo: any = {
    View: { 'code': 'TQ_DQTV_LyDo', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_LyDo', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_LyDo', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_LyDo', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_LyDo', 'action': '_delete' },
  };
  // sức khỏe
  public TQ_DQTV_SucKhoe: any = {
    View: { 'code': 'TQ_DQTV_SucKhoe', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_SucKhoe', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_SucKhoe', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_SucKhoe', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_SucKhoe', 'action': '_delete' },
  };
  // tổ chức xã hội
  public TQ_DQTV_ToChucXaHoi: any = {
    View: { 'code': 'TQ_DQTV_ToChucXaHoi', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_ToChucXaHoi', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_ToChucXaHoi', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_ToChucXaHoi', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_ToChucXaHoi', 'action': '_delete' },
  };
  // trình độ học vấn
  public TQ_DQTV_TrinhDoHocVan: any = {
    View: { 'code': 'TQ_DQTV_TrinhDoHocVan', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_TrinhDoHocVan', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_TrinhDoHocVan', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_TrinhDoHocVan', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_TrinhDoHocVan', 'action': '_delete' },
  };
  // trình độ văn hóa
  public TQ_DQTV_TrinhDoVanHoa: any = {
    View: { 'code': 'TQ_DQTV_TrinhDoVanHoa', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_TrinhDoVanHoa', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_TrinhDoVanHoa', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_TrinhDoVanHoa', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_TrinhDoVanHoa', 'action': '_delete' },
  };
  // tạm hoãn
  public TQ_DQTV_TamHoan: any = {
    View: { 'code': 'TQ_DQTV_TamHoan', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_TamHoan', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_TamHoan', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_TamHoan', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_TamHoan', 'action': '_delete' },
  };
  // học vị
  public TQ_DQTV_HocVi: any = {
    View: { 'code': 'TQ_DQTV_HocVi', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_HocVi', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_HocVi', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_HocVi', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_HocVi', 'action': '_delete' },
  };
  // cấp bậc
  public TQ_DQTV_CapBac: any = {
    View: { 'code': 'TQ_DQTV_CapBac', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_CapBac', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_CapBac', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_CapBac', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_CapBac', 'action': '_delete' },
  };
  // vũ khí
  public TQ_DQTV_VuKhi: any = {
    View: { 'code': 'TQ_DQTV_VuKhi', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_VuKhi', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_VuKhi', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_VuKhi', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_VuKhi', 'action': '_delete' },
  };
  // cấp hành chính
  public TQ_DQTV_CapHanhChinh: any = {
    View: { 'code': 'TQ_DQTV_CapHanhChinh', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_CapHanhChinh', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_CapHanhChinh', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_CapHanhChinh', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_CapHanhChinh', 'action': '_delete' },
  };
  // quan hệ
  public TQ_DQTV_QuanHe: any = {
    View: { 'code': 'TQ_DQTV_QuanHe', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_QuanHe', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_QuanHe', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_QuanHe', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_QuanHe', 'action': '_delete' },
  };
  // lực lượng
  public TQ_DQTV_LucLuong: any = {
    View: { 'code': 'TQ_DQTV_LucLuong', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_LucLuong', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_LucLuong', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_LucLuong', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_LucLuong', 'action': '_delete' },
  };
  // công cụ hỗ trợ
  public TQ_DQTV_CongCuHoTro: any = {
    View: { 'code': 'TQ_DQTV_CongCuHoTro', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_CongCuHoTro', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_CongCuHoTro', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_CongCuHoTro', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_CongCuHoTro', 'action': '_delete' },
  };
  // thành phần bản thân
  public TQ_DQTV_ThanhPhanBanThan: any = {
    View: { 'code': 'TQ_DQTV_ThanhPhanBanThan', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_ThanhPhanBanThan', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_ThanhPhanBanThan', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_ThanhPhanBanThan', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_ThanhPhanBanThan', 'action': '_delete' },
  };
  // thành phần gia đình
  public TQ_DQTV_ThanhPhanGiaDinh: any = {
    View: { 'code': 'TQ_DQTV_ThanhPhanGiaDinh', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_ThanhPhanGiaDinh', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_ThanhPhanGiaDinh', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_ThanhPhanGiaDinh', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_ThanhPhanGiaDinh', 'action': '_delete' },
  };
  // phân loại trọng điểm
  public TQ_DQTV_PhanLoaiTrongDiem: any = {
    View: { 'code': 'TQ_DQTV_PhanLoaiTrongDiem', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_PhanLoaiTrongDiem', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_PhanLoaiTrongDiem', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_PhanLoaiTrongDiem', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_PhanLoaiTrongDiem', 'action': '_delete' },
  };
  // quy mô tổ chức
  public TQ_DQTV_QuyMoToChuc: any = {
    View: { 'code': 'TQ_DQTV_QuyMoToChuc', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_QuyMoToChuc', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_QuyMoToChuc', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_QuyMoToChuc', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_QuyMoToChuc', 'action': '_delete' },
  };
  // quân hàm
  public TQ_DQTV_QuanHam: any = {
    View: { 'code': 'TQ_DQTV_QuanHam', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_QuanHam', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_QuanHam', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_QuanHam', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_QuanHam', 'action': '_delete' },
  };
  // quân nhân dự bị
  public TQ_DQTV_QuanNhanDuBi: any = {
    View: { 'code': 'TQ_DQTV_QuanNhanDuBi', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_QuanNhanDuBi', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_QuanNhanDuBi', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_QuanNhanDuBi', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_QuanNhanDuBi', 'action': '_delete' },
  };
  // loại dự bị
  public TQ_DQTV_LoaiDuBi: any = {
    View: { 'code': 'TQ_DQTV_LoaiDuBi', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_LoaiDuBi', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_LoaiDuBi', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_LoaiDuBi', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_LoaiDuBi', 'action': '_delete' },
  };
  // nganhnghecmkt
  public TQ_DQTV_NganhNgheCmkt: any = {
    View: { 'code': 'TQ_DQTV_NganhNgheCmkt', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_NganhNgheCmkt', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_NganhNgheCmkt', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_NganhNgheCmkt', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_NganhNgheCmkt', 'action': '_delete' },
  };
  // miễn
  public TQ_DQTV_Mien: any = {
    View: { 'code': 'TQ_DQTV_Mien', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_Mien', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_Mien', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_Mien', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_Mien', 'action': '_delete' },
  };
  // dân tộc
  public TQ_DQTV_DanToc: any = {
    View: { 'code': 'TQ_DQTV_DanToc', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_DanToc', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_DanToc', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_DanToc', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_DanToc', 'action': '_delete' },
  };
  // đơn vị dự bị động viên
  public TQ_DQTV_DonViDuBiDongVien: any = {
    View: { 'code': 'TQ_DQTV_DonViDuBiDongVien', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_DonViDuBiDongVien', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_DonViDuBiDongVien', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_DonViDuBiDongVien', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_DonViDuBiDongVien', 'action': '_delete' },
  };
  // tôn giáo
  public TQ_DQTV_TonGiao: any = {
    View: { 'code': 'TQ_DQTV_TonGiao', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_TonGiao', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_TonGiao', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_TonGiao', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_TonGiao', 'action': '_delete' },
  };
  // loại phương tiện
  public TQ_DQTV_LoaiPhuongTien: any = {
    View: { 'code': 'TQ_DQTV_LoaiPhuongTien', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_LoaiPhuongTien', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_LoaiPhuongTien', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_LoaiPhuongTien', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_LoaiPhuongTien', 'action': '_delete' },
  };
  // GD_QPAN
  // trường học
  public GD_QPAN_TruongHoc: any = {
    View: { 'code': 'GD_QPAN_TruongHoc', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_TruongHoc', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_TruongHoc', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_TruongHoc', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_TruongHoc', 'action': '_delete' },
  };
  // phân loại đối tượng
  public GD_QPAN_PhanLoaiDoiTuong: any = {
    View: { 'code': 'GD_QPAN_PhanLoaiDoiTuong', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_PhanLoaiDoiTuong', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_PhanLoaiDoiTuong', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_PhanLoaiDoiTuong', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_PhanLoaiDoiTuong', 'action': '_delete' },
  };
  // phân loại trườn học
  public GD_QPAN_PhanLoaiTruongHoc: any = {
    View: { 'code': 'GD_QPAN_PhanLoaiTruongHoc', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_PhanLoaiTruongHoc', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_PhanLoaiTruongHoc', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_PhanLoaiTruongHoc', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_PhanLoaiTruongHoc', 'action': '_delete' },
  };
  // đối tượng
  public GD_QPAN_DoiTuong: any = {
    View: { 'code': 'GD_QPAN_DoiTuong', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_DoiTuong', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_DoiTuong', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_DoiTuong', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_DoiTuong', 'action': '_delete' },
  };
  // phân loại lóp học
  public GD_QPAN_PhanLoaiLopHoc: any = {
    View: { 'code': 'GD_QPAN_PhanLoaiLopHoc', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_PhanLoaiLopHoc', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_PhanLoaiLopHoc', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_PhanLoaiLopHoc', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_PhanLoaiLopHoc', 'action': '_delete' },
  };
  // kết quả
  public GD_QPAN_KetQua: any = {
    View: { 'code': 'GD_QPAN_KetQua', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_KetQua', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_KetQua', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_KetQua', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_KetQua', 'action': '_delete' },
  };
  // hình thức dào tạo
  public GD_QPAN_HinhThucDaoTao: any = {
    View: { 'code': 'GD_QPAN_HinhThucDaoTao', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_HinhThucDaoTao', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_HinhThucDaoTao', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_HinhThucDaoTao', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_HinhThucDaoTao', 'action': '_delete' },
  };
  // lý luận chính trị
  public GD_QPAN_LyLuanChinhTri: any = {
    View: { 'code': 'GD_QPAN_LyLuanChinhTri', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_LyLuanChinhTri', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_LyLuanChinhTri', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_LyLuanChinhTri', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_LyLuanChinhTri', 'action': '_delete' },
  };
  // xếp loại lớp học
  public GD_QPAN_XepLoaiLopHoc: any = {
    View: { 'code': 'GD_QPAN_XepLoaiLopHoc', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_XepLoaiLopHoc', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_XepLoaiLopHoc', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_XepLoaiLopHoc', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_XepLoaiLopHoc', 'action': '_delete' },
  };
  // quản lý giáo viên
  public GD_QPAN_QuanLyGiaoVien: any = {
    View: { 'code': 'GD_QPAN_QuanLyGiaoVien', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_QuanLyGiaoVien', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_QuanLyGiaoVien', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_QuanLyGiaoVien', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_QuanLyGiaoVien', 'action': '_delete' },
  };
  // đào tạo theo lớp học
  public GD_QPAN_DaoTaoTheoLopHoc: any = {
    View: { 'code': 'GD_QPAN_DaoTaoTheoLopHoc', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_DaoTaoTheoLopHoc', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_DaoTaoTheoLopHoc', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_DaoTaoTheoLopHoc', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_DaoTaoTheoLopHoc', 'action': '_delete' },
  };
  // đào tạo theo lớp học
  public GD_QPAN_GDToanDan: any = {
    View: { 'code': 'GD_QPAN_GDToanDan', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_GDToanDan', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_GDToanDan', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_GDToanDan', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_GDToanDan', 'action': '_delete' },
  };
  // danh sach dao tao lop hoc
  public GD_QPAN_DanhSachThamGiaDaoTao: any = {
    View: { 'code': 'GD_QPAN_DanhSachThamGiaDaoTao', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_DanhSachThamGiaDaoTao', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_DanhSachThamGiaDaoTao', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_DanhSachThamGiaDaoTao', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_DanhSachThamGiaDaoTao', 'action': '_delete' },
  };
  public TQ_CongDanNu: any = {
    View: { 'code': 'TQ_CongDanNu', 'action': '_view' },
    Export: { 'code': 'TQ_CongDanNu', 'action': '_export' },
    Insert: { 'code': 'TQ_CongDanNu', 'action': '_insert' },
    Update: { 'code': 'TQ_CongDanNu', 'action': '_update' },
    Delete: { 'code': 'TQ_CongDanNu', 'action': '_delete' },
  };


  public TQ_SSDBDV1: any = {
    View: { 'code': 'TQ_SSDBDV1', 'action': '_view' },
    Export: { 'code': 'TQ_SSDBDV1', 'action': '_export' },
    Insert: { 'code': 'TQ_SSDBDV1', 'action': '_insert' },
    Update: { 'code': 'TQ_SSDBDV1', 'action': '_update' },
    Delete: { 'code': 'TQ_SSDBDV1', 'action': '_delete' },
  };

  public TQ_SSDBDV2: any = {
    View: { 'code': 'TQ_SSDBDV2', 'action': '_view' },
    Export: { 'code': 'TQ_SSDBDV2', 'action': '_export' },
    Insert: { 'code': 'TQ_SSDBDV2', 'action': '_insert' },
    Update: { 'code': 'TQ_SSDBDV2', 'action': '_update' },
    Delete: { 'code': 'TQ_SSDBDV2', 'action': '_delete' },
  };

  public TQ_DBDV1: any = {
    View: { 'code': 'TQ_DBDV1', 'action': '_view' },
    Export: { 'code': 'TQ_DBDV1', 'action': '_export' },
    Insert: { 'code': 'TQ_DBDV1', 'action': '_insert' },
    Update: { 'code': 'TQ_DBDV1', 'action': '_update' },
    Delete: { 'code': 'TQ_DBDV1', 'action': '_delete' },
  };

  public TQ_DBDV2: any = {
    View: { 'code': 'TQ_DBDV2', 'action': '_view' },
    Export: { 'code': 'TQ_DBDV2', 'action': '_export' },
    Insert: { 'code': 'TQ_DBDV2', 'action': '_insert' },
    Update: { 'code': 'TQ_DBDV2', 'action': '_update' },
    Delete: { 'code': 'TQ_DBDV2', 'action': '_delete' },
  };

  public TQ_SQDB: any = {
    View: { 'code': 'TQ_SQDB', 'action': '_view' },
    Export: { 'code': 'TQ_SQDB', 'action': '_export' },
    Insert: { 'code': 'TQ_SQDB', 'action': '_insert' },
    Update: { 'code': 'TQ_SQDB', 'action': '_update' },
    Delete: { 'code': 'TQ_SQDB', 'action': '_delete' },
  };

  public TQ_KeHoachDBDV: any = {
    View: { 'code': 'TQ_KeHoachDBDV', 'action': '_view' },
    Export: { 'code': 'TQ_KeHoachDBDV', 'action': '_export' },
    Insert: { 'code': 'TQ_KeHoachDBDV', 'action': '_insert' },
    Update: { 'code': 'TQ_KeHoachDBDV', 'action': '_update' },
    Delete: { 'code': 'TQ_KeHoachDBDV', 'action': '_delete' },
  };

  public TQ_QNDB: any = {
    View: { 'code': 'TQ_QNDB', 'action': '_view' },
    Export: { 'code': 'TQ_QNDB', 'action': '_export' },
    Insert: { 'code': 'TQ_QNDB', 'action': '_insert' },
    Update: { 'code': 'TQ_QNDB', 'action': '_update' },
    Delete: { 'code': 'TQ_QNDB', 'action': '_delete' },
  };

  public TQ_TheoDoiHuanLuyenDonVi: any = {
    View: { 'code': 'TQ_TheoDoiHuanLuyenDonVi', 'action': '_view' },
    Export: { 'code': 'TQ_TheoDoiHuanLuyenDonVi', 'action': '_export' },
    Insert: { 'code': 'TQ_TheoDoiHuanLuyenDonVi', 'action': '_insert' },
    Update: { 'code': 'TQ_TheoDoiHuanLuyenDonVi', 'action': '_update' },
    Delete: { 'code': 'TQ_TheoDoiHuanLuyenDonVi', 'action': '_delete' },
  };

  public DQTV: any = {
    View: { 'code': 'DQTV', 'action': '_view' },
    Export: { 'code': 'DQTV', 'action': '_export' },
    Insert: { 'code': 'DQTV', 'action': '_insert' },
    Update: { 'code': 'DQTV', 'action': '_update' },
    Delete: { 'code': 'DQTV', 'action': '_delete' },
  };

  public DQTV_Nguon: any = {
    View: { 'code': 'DQTV_Nguon', 'action': '_view' },
    Export: { 'code': 'DQTV_Nguon', 'action': '_export' },
    Insert: { 'code': 'DQTV_Nguon', 'action': '_insert' },
    Update: { 'code': 'DQTV_Nguon', 'action': '_update' },
    Delete: { 'code': 'DQTV_Nguon', 'action': '_delete' },
  };

  public DQTV_XDHN: any = {
    View: { 'code': 'DQTV_XDHN', 'action': '_view' },
    Export: { 'code': 'DQTV_XDHN', 'action': '_export' },
    Insert: { 'code': 'DQTV_XDHN', 'action': '_insert' },
    Update: { 'code': 'DQTV_XDHN', 'action': '_update' },
    Delete: { 'code': 'DQTV_XDHN', 'action': '_delete' },
  };

  public DQTV_SSMR: any = {
    View: { 'code': 'DQTV_SSMR', 'action': '_view' },
    Export: { 'code': 'DQTV_SSMR', 'action': '_export' },
    Insert: { 'code': 'DQTV_SSMR', 'action': '_insert' },
    Update: { 'code': 'DQTV_SSMR', 'action': '_update' },
    Delete: { 'code': 'DQTV_SSMR', 'action': '_delete' },
  };

  public DQTV_BCDQTV: any = {
    View: { 'code': 'DQTV_BCDQTV', 'action': '_view' },
    Export: { 'code': 'DQTV_BCDQTV', 'action': '_export' },
    Insert: { 'code': 'DQTV_BCDQTV', 'action': '_insert' },
    Update: { 'code': 'DQTV_BCDQTV', 'action': '_update' },
    Delete: { 'code': 'DQTV_BCDQTV', 'action': '_delete' },
  };

  public DQTV_QLTV: any = {
    View: { 'code': 'DQTV_QLTV', 'action': '_view' },
    Export: { 'code': 'DQTV_QLTV', 'action': '_export' },
    Insert: { 'code': 'DQTV_QLTV', 'action': '_insert' },
    Update: { 'code': 'DQTV_QLTV', 'action': '_update' },
    Delete: { 'code': 'DQTV_QLTV', 'action': '_delete' },
  };

  public DQTV_TCTG: any = {
    View: { 'code': 'DQTV_TCTG', 'action': '_view' },
    Export: { 'code': 'DQTV_TCTG', 'action': '_export' },
    Insert: { 'code': 'DQTV_TCTG', 'action': '_insert' },
    Update: { 'code': 'DQTV_TCTG', 'action': '_update' },
    Delete: { 'code': 'DQTV_TCTG', 'action': '_delete' },
  };

  public DQTV_HTDQTV: any = {
    View: { 'code': 'DQTV_HTDQTV', 'action': '_view' },
    Export: { 'code': 'DQTV_HTDQTV', 'action': '_export' },
    Insert: { 'code': 'DQTV_HTDQTV', 'action': '_insert' },
    Update: { 'code': 'DQTV_HTDQTV', 'action': '_update' },
    Delete: { 'code': 'DQTV_HTDQTV', 'action': '_delete' },
  };

  public DQTV_CTHL: any = {
    View: { 'code': 'DQTV_CTHL', 'action': '_view' },
    Export: { 'code': 'DQTV_CTHL', 'action': '_export' },
    Insert: { 'code': 'DQTV_CTHL', 'action': '_insert' },
    Update: { 'code': 'DQTV_CTHL', 'action': '_update' },
    Delete: { 'code': 'DQTV_CTHL', 'action': '_delete' },
  };

  public DQTV_HLLL: any = {
    View: { 'code': 'DQTV_HLLL', 'action': '_view' },
    Export: { 'code': 'DQTV_HLLL', 'action': '_export' },
    Insert: { 'code': 'DQTV_HLLL', 'action': '_insert' },
    Update: { 'code': 'DQTV_HLLL', 'action': '_update' },
    Delete: { 'code': 'DQTV_HLLL', 'action': '_delete' },
  };

  public DQTV_TBVK: any = {
    View: { 'code': 'DQTV_TBVK', 'action': '_view' },
    Export: { 'code': 'DQTV_TBVK', 'action': '_export' },
    Insert: { 'code': 'DQTV_TBVK', 'action': '_insert' },
    Update: { 'code': 'DQTV_TBVK', 'action': '_update' },
    Delete: { 'code': 'DQTV_TBVK', 'action': '_delete' },
  };

  public QLVB: any = {
    View: { 'code': 'QLVB', 'action': '_view' },
    Export: { 'code': 'QLVB', 'action': '_export' },
    Insert: { 'code': 'QLVB', 'action': '_insert' },
    Update: { 'code': 'QLVB', 'action': '_update' },
    Delete: { 'code': 'QLVB', 'action': '_delete' },
  };

  public TQ_QuanLyCongDan: any = {
    View: { 'code': 'TQ_QuanLyCongDan', 'action': '_view' },
    Export: { 'code': 'TQ_QuanLyCongDan', 'action': '_export' },
    Insert: { 'code': 'TQ_QuanLyCongDan', 'action': '_insert' },
    Update: { 'code': 'TQ_QuanLyCongDan', 'action': '_update' },
    Delete: { 'code': 'TQ_QuanLyCongDan', 'action': '_delete' },
  };

  public DQTV_HDQD: any = {
    View: { 'code': 'DQTV_HDQD', 'action': '_view' },
    Export: { 'code': 'DQTV_HDQD', 'action': '_export' },
    Insert: { 'code': 'DQTV_HDQD', 'action': '_insert' },
    Update: { 'code': 'DQTV_HDQD', 'action': '_update' },
    Delete: { 'code': 'DQTV_HDQD', 'action': '_delete' },
  };

  public CB_CanBo: any = {
    View: { 'code': 'CB_CanBo', 'action': '_view' },
    Export: { 'code': 'CB_CanBo', 'action': '_export' },
    Insert: { 'code': 'CB_CanBo', 'action': '_insert' },
    Update: { 'code': 'CB_CanBo', 'action': '_update' },
    Delete: { 'code': 'CB_CanBo', 'action': '_delete' },
  };

  public CB_XDTX: any = {
    View: { 'code': 'CB_XDTX', 'action': '_view' },
    Export: { 'code': 'CB_XDTX', 'action': '_export' },
    Insert: { 'code': 'CB_XDTX', 'action': '_insert' },
    Update: { 'code': 'CB_XDTX', 'action': '_update' },
    Delete: { 'code': 'CB_XDTX', 'action': '_delete' },
  };

  public CB_DaoTao: any = {
    View: { 'code': 'CB_DaoTao', 'action': '_view' },
    Export: { 'code': 'CB_DaoTao', 'action': '_export' },
    Insert: { 'code': 'CB_DaoTao', 'action': '_insert' },
    Update: { 'code': 'CB_DaoTao', 'action': '_update' },
    Delete: { 'code': 'CB_DaoTao', 'action': '_delete' },
  };

  public KT: any = {
    View: { 'code': 'KT', 'action': '_view' },
    Export: { 'code': 'KT', 'action': '_export' },
    Insert: { 'code': 'KT', 'action': '_insert' },
    Update: { 'code': 'KT', 'action': '_update' },
    Delete: { 'code': 'KT', 'action': '_delete' },
  };


  public DQTV_KeHoachLuanPhien: any = {
    View: { 'code': 'DQTV_KeHoachLuanPhien', 'action': '_view' },
    Export: { 'code': 'DQTV_KeHoachLuanPhien', 'action': '_export' },
    Insert: { 'code': 'DQTV_KeHoachLuanPhien', 'action': '_insert' },
    Update: { 'code': 'DQTV_KeHoachLuanPhien', 'action': '_update' },
    Delete: { 'code': 'DQTV_KeHoachLuanPhien', 'action': '_delete' },
  };

  public TQ_DKPTKT: any = {
    View: { 'code': 'TQ_DKPTKT', 'action': '_view' },
    Export: { 'code': 'TQ_DKPTKT', 'action': '_export' },
    Insert: { 'code': 'TQ_DKPTKT', 'action': '_insert' },
    Update: { 'code': 'TQ_DKPTKT', 'action': '_update' },
    Delete: { 'code': 'TQ_DKPTKT', 'action': '_delete' },
  };

  public TQ_BCPTKT: any = {
    View: { 'code': 'TQ_BCPTKT', 'action': '_view' },
    Export: { 'code': 'TQ_BCPTKT', 'action': '_export' },
    Insert: { 'code': 'TQ_BCPTKT', 'action': '_insert' },
    Update: { 'code': 'TQ_BCPTKT', 'action': '_update' },
    Delete: { 'code': 'TQ_BCPTKT', 'action': '_delete' },
  };

  public TQ_DSPTKT: any = {
    View: { 'code': 'TQ_DSPTKT', 'action': '_view' },
    Export: { 'code': 'TQ_DSPTKT', 'action': '_export' },
    Insert: { 'code': 'TQ_DSPTKT', 'action': '_insert' },
    Update: { 'code': 'TQ_DSPTKT', 'action': '_update' },
    Delete: { 'code': 'TQ_DSPTKT', 'action': '_delete' },
  };

  public TQ_KTPTKT: any = {
    View: { 'code': 'TQ_KTPTKT', 'action': '_view' },
    Export: { 'code': 'TQ_KTPTKT', 'action': '_export' },
    Insert: { 'code': 'TQ_KTPTKT', 'action': '_insert' },
    Update: { 'code': 'TQ_KTPTKT', 'action': '_update' },
    Delete: { 'code': 'TQ_KTPTKT', 'action': '_delete' },
  };

  public TQ_KQHDPTKT: any = {
    View: { 'code': 'TQ_KQHDPTKT', 'action': '_view' },
    Export: { 'code': 'TQ_KQHDPTKT', 'action': '_export' },
    Insert: { 'code': 'TQ_KQHDPTKT', 'action': '_insert' },
    Update: { 'code': 'TQ_KQHDPTKT', 'action': '_update' },
    Delete: { 'code': 'TQ_KQHDPTKT', 'action': '_delete' },
  };

  public TQ_DotKeHoach: any = {
    View: { 'code': 'TQ_DotKeHoach', 'action': '_view' },
    Export: { 'code': 'TQ_DotKeHoach', 'action': '_export' },
    Insert: { 'code': 'TQ_DotKeHoach', 'action': '_insert' },
    Update: { 'code': 'TQ_DotKeHoach', 'action': '_update' },
    Delete: { 'code': 'TQ_DotKeHoach', 'action': '_delete' },
  };

  public TQ_KeHoachHuyDong: any = {
    View: { 'code': 'TQ_KeHoachHuyDong', 'action': '_view' },
    Export: { 'code': 'TQ_KeHoachHuyDong', 'action': '_export' },
    Insert: { 'code': 'TQ_KeHoachHuyDong', 'action': '_insert' },
    Update: { 'code': 'TQ_KeHoachHuyDong', 'action': '_update' },
    Delete: { 'code': 'TQ_KeHoachHuyDong', 'action': '_delete' },
  };

  public TQ_BaoCao: any = {
    View: { 'code': 'TQ_BaoCao', 'action': '_view' },
    Export: { 'code': 'TQ_BaoCao', 'action': '_export' },
    Insert: { 'code': 'TQ_BaoCao', 'action': '_insert' },
    Update: { 'code': 'TQ_BaoCao', 'action': '_update' },
    Delete: { 'code': 'TQ_BaoCao', 'action': '_delete' },
  };

  public DQTV_BaoCao: any = {
    View: { 'code': 'DQTV_BaoCao', 'action': '_view' },
    Export: { 'code': 'DQTV_BaoCao', 'action': '_export' },
    Insert: { 'code': 'DQTV_BaoCao', 'action': '_insert' },
    Update: { 'code': 'DQTV_BaoCao', 'action': '_update' },
    Delete: { 'code': 'DQTV_BaoCao', 'action': '_delete' },
  };

  public TQ_TheoDoiHuanLuyenTapTrung: any = {
    View: { 'code': 'TQ_TheoDoiHuanLuyenTapTrung', 'action': '_view' },
    Export: { 'code': 'TQ_TheoDoiHuanLuyenTapTrung', 'action': '_export' },
    Insert: { 'code': 'TQ_TheoDoiHuanLuyenTapTrung', 'action': '_insert' },
    Update: { 'code': 'TQ_TheoDoiHuanLuyenTapTrung', 'action': '_update' },
    Delete: { 'code': 'TQ_TheoDoiHuanLuyenTapTrung', 'action': '_delete' },
  };

  public CB_ChiBo: any = {
    View: { 'code': 'CB_ChiBo', 'action': '_view' },
    Export: { 'code': 'CB_ChiBo', 'action': '_export' },
    Insert: { 'code': 'CB_ChiBo', 'action': '_insert' },
    Update: { 'code': 'CB_ChiBo', 'action': '_update' },
    Delete: { 'code': 'CB_ChiBo', 'action': '_delete' },
  };

  public CB_ChiDoan: any = {
    View: { 'code': 'CB_ChiDoan', 'action': '_view' },
    Export: { 'code': 'CB_ChiDoan', 'action': '_export' },
    Insert: { 'code': 'CB_ChiDoan', 'action': '_insert' },
    Update: { 'code': 'CB_ChiDoan', 'action': '_update' },
    Delete: { 'code': 'CB_ChiDoan', 'action': '_delete' },
  };
  public CB_ToChuc: any = {
    View: { 'code': 'CB_ToChuc', 'action': '_view' },
    Export: { 'code': 'CB_ToChuc', 'action': '_export' },
    Insert: { 'code': 'CB_ToChuc', 'action': '_insert' },
    Update: { 'code': 'CB_ToChuc', 'action': '_update' },
    Delete: { 'code': 'CB_ToChuc', 'action': '_delete' },
  };
  public CB_BCHQS: any = {
    View: { 'code': 'CB_BCHQS', 'action': '_view' },
    Export: { 'code': 'CB_BCHQS', 'action': '_export' },
    Insert: { 'code': 'CB_BCHQS', 'action': '_insert' },
    Update: { 'code': 'CB_BCHQS', 'action': '_update' },
    Delete: { 'code': 'CB_BCHQS', 'action': '_delete' },
  };
  public CB_DonViTv: any = {
    View: { 'code': 'CB_DonViTv', 'action': '_view' },
    Export: { 'code': 'CB_DonViTv', 'action': '_export' },
    Insert: { 'code': 'CB_DonViTv', 'action': '_insert' },
    Update: { 'code': 'CB_DonViTv', 'action': '_update' },
    Delete: { 'code': 'CB_DonViTv', 'action': '_delete' },
  };
  public TQ_GiaoChiTieu: any = {
    View: { 'code': 'TQ_GiaoChiTieu', 'action': '_view' },
    Export: { 'code': 'TQ_GiaoChiTieu', 'action': '_export' },
    Insert: { 'code': 'TQ_GiaoChiTieu', 'action': '_insert' },
    Update: { 'code': 'TQ_GiaoChiTieu', 'action': '_update' },
    Delete: { 'code': 'TQ_GiaoChiTieu', 'action': '_delete' },
  };

  public CongDan_QuaTrinh: any = {
    View: { 'code': 'CongDan_QuaTrinh', 'action': '_view' },
    Export: { 'code': 'CongDan_QuaTrinh', 'action': '_export' },
    Insert: { 'code': 'CongDan_QuaTrinh', 'action': '_insert' },
    Update: { 'code': 'CongDan_QuaTrinh', 'action': '_update' },
    Delete: { 'code': 'CongDan_QuaTrinh', 'action': '_delete' },
  };

  public BaoCaoChung: any = {
    View: { 'code': 'BaoCaoChung', 'action': '_view' },
    Export: { 'code': 'BaoCaoChung', 'action': '_export' },
    Insert: { 'code': 'BaoCaoChung', 'action': '_insert' },
    Update: { 'code': 'BaoCaoChung', 'action': '_update' },
    Delete: { 'code': 'BaoCaoChung', 'action': '_delete' },
  };
  public navData: any;
  constructor(
    private translateService: TranslateService,
    private toastrService: ToastrService,
  ) { }

  setInitState() {
    this.translateService.addLangs(['en', 'vi']);
    const browserLang = (this.translate.getBrowserLang().includes('vi')) ? 'en' : 'vi';
    this.setLang(browserLang);
  }
  trans(data: string) {
    return this.translate.instant(data);
  }
  setLang(lang: string) {
    this.translateService.onLangChange.pipe(take(1)).subscribe(result => {
      this.language$.next(result);
    });
    this.translateService.use(lang);
  }
  bitToValue = (value) => {
    if (value == null) {
      return 0;
    } else if (value.data) {
      return value.data[0];
    } else {
      return value ? 1 : 0;
    }
  }
  messageRes(message: any) {
    this.toastrService.success(this.translate.instant(message));
  }
  messageErr(res: any) {
    if (res.error && res.error.message) {
      if (res.error.message.includes('code') && res.error.message.includes('duplicate')) {
        this.toastrService.error(this.translate.instant('common.validate.code.duplicate'));
      } else if (res.error.message.includes('name') && res.error.message.includes('duplicate')) {
        this.toastrService.error(this.translate.instant('common.validate.name.duplicate'));
      } else {
        this.toastrService.error(this.translate.instant(res.error.message));

      }
    } else if (res.error) {
      this.toastrService.error(this.translate.instant('common.error.service'));
    }
  }
}

