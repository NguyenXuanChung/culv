import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class QuyTrinhService {
  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }
  // bổ nhiệm
  getListQuyTrinhDeXuat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qt/dexuat/getlist', data, permission);
  }
  getDetailQuyTrinhDeXuat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qt/dexuat/getdetail', data, permission);
  }
  getQuyTrinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qt/get', data, permission);
  }
  getDeXuat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qt/dexuat/get', data, permission);
  }
  insertDeXuat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qt/dexuat/insert', data, permission);
  }
  getActivity(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qt/activity/get', data, permission);
  }
  insertActivity(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qt/activity/insert', data, permission);
  }
  editActivity(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qt/activity/update', data, permission);
  }
  deleteActivity(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qt/activity/delete', data, permission);
  }
  editDeXuat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qt/dexuat/update', data, permission);
  }
  deleteDeXuat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('qt/dexuat/delete', data, permission);
  }
}
