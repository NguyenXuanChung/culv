import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class WorkFlowService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  //#region WorkFlow
  // Retrieve data with condition in body
  getWorkFlow(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('sys/workflow/get', data, permission);
  }
  getWorkFlowState(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('sys/workflow/get-state', data, permission);
  }
  getWorkFlowStateAction(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('sys/workflow/get-state-action', data, permission);
  }
  //#endregion

}
