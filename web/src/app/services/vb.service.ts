import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class VbService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  //#region Vb
  // Retrieve data with condition in body
  getVanBanQuyChe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('vb/vanbanquyche/get', data, permission);
  }
  getVb(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('vb/get', data, permission);
  }
  // Create a new item
  insertVb(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('vb/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editVb(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('vb/update', data, permission);
  }
  // Delete a item with id
  deleteVb(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('vb/delete', data, permission);
  }
  //#endregion

}
