import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class TtbhService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  getBhxh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('ttbh/xahoi/getbhxh', data, permission);
  }
  getQuaTrinhBhxh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('ttbh/xahoi/getquatrinhbhxh', data, permission);
  }
  getQuaTrinhBhyt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('ttbh/xahoi/getquatrinhbhyt', data, permission);
  }
  getBhnt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('ttbh/nhantho/get', data, permission);
  }
  getQuaTrinhBhnt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('ttbh/nhantho/getquatrinh', data, permission);
  }
  getBhhc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('ttbh/healthcare/get', data, permission);
  }
  getQuaTrinhBhhc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('ttbh/healthcare/getquatrinh', data, permission);
  }
}
