import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class InfoService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  //#region Info
  // QuaTrinhCongTac with canBo_ID
  getQuaTrinhCongTac(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('info/quatrinhcongtac', data, permission);
  }
  // KhenThuongKyLuat with canBo_ID
  getKhenThuongKyLuat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('info/khenthuongkyluat', data, permission);
  }
  // GiamTruGiaCanh with canBo_ID
  getGiamTruGiaCanh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('info/giamtrugiacanh', data, permission);
  }
  // QuaTrinhHopDong with canBo_ID
  getQuaTrinhHopDong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('info/quatrinhhopdong', data, permission);
  }
  //#endregion

}
