import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class DmService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  // Retrieve data with condition in body
  getLoaiHopDong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/loaihopdong/get', data, permission);
  }
  getHinhThucSuDungLaoDong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/hinhthucsudunglaodong/get', data, permission);
  }
}
