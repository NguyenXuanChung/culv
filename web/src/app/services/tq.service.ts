import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class TuyenQuanService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }
  // Công dân
  getDataImportCongDan(data: any, permission: any): Observable<any> {
    return this.httpService.downloadAttachFilePermission('export/getdataimportcongdan', data, permission);
  }
  // DaoTaoCnqs
  getDaoTaoCnqs(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/daotaocnqs/get', data, permission);
  }
  deleteDaoTaoCnqs(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/daotaocnqs/delete', data, permission);
  }
  insertDaoTaoCnqs(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/daotaocnqs/insert', data, permission);
  }
  editDaoTaoCnqs(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/daotaocnqs/update', data, permission);
  }
   // DotTuyenQuan GiaoChiTieu
   getGiaoChiTieu(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/giaochitieu/get', data, permission);
  }
  deleteGiaoChiTieu(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/giaochitieu/delete', data, permission);
  }
  insertGiaoChiTieu(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/giaochitieu/insert', data, permission);
  }
  editGiaoChiTieu(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/giaochitieu/update', data, permission);
  }
  // DM Ptkt
  getPtkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/ptkt/get', data, permission);
  }
  getPtktKiemTra(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/ptkt/getkiemtra', data, permission);
  }
  insertPtktKiemTra(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/ptkt/updatekiemtra', data, permission);
  }
  deletePtktKiemTra(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/ptkt/deletekiemtra', data, permission);
  }
  getPtktDetail(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/ptkt/getdetail', data, permission);
  }
  deletePtktDetail(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/ptkt/deletedetail', data, permission);
  }
  deletePtkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/ptkt/delete', data, permission);
  }
  insertPtkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/ptkt/insert', data, permission);
  }
  insertPtktDetail(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/ptkt/insertdetail', data, permission);
  }
  editPtkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/ptkt/update', data, permission);
  }
  editQuaTrinhPtkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/ptkt/editquatrinh', data, permission);
  }
  updateQuaTrinhPtkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/ptkt/updatequatrinh', data, permission);
  }


  // updateCongDanSSDBDV(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('tq/congdan/updatessdbdv', data, permission);
  // }
  updateQuaTrinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/updatequatrinh', data, permission);
  }
  editQuaTrinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/editquatrinh', data, permission);
  }
  deleteQuaTrinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/deletequatrinh', data, permission);
  }
  updateCongDanDetail(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/updatedetail', data, permission);
  }
  updateBienChe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/updatebienche', data, permission);
  }
  updateDongVien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/updatedongvien', data, permission);
  }
  updateGiaiNgach(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/updategiaingach', data, permission);
  }
  // updateCongDanTq(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('tq/congdan/updatetq', data, permission);
  // }
  updateCongDanXd(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/updatexd', data, permission);
  }
  // updateCongDanNn(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('tq/congdan/updatenn', data, permission);
  // }
  getCongDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/get', data, permission);
  }
  importCongDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/import', data, permission);
  }
  getDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getdbdv', data, permission);
  }
  getQNDB(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getqndb', data, permission);
  }
  getCongDanByCmnd(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getbycmnd', data, permission);
  }
  getCongDanNu(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getlistnu', data, permission);
  }
  getCongDanSsnn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getssnn', data, permission);
  }
  getCongDanSsDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getssdbdv', data, permission);
  }
  getCongDanDetail(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getdetail', data, permission);
  }
  getCongDanXd(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getxd', data, permission);
  }
  getCongDanNn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getnn', data, permission);
  }
  getLoaiCongDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdantypestateaction/get', data, permission);
  }
  getCongDanTypeState(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdantypestate/get', data, permission);
  }
  deleteCongDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/delete', data, permission);
  }
  insertCongDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/insert', data, permission);
  }
  editCongDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/update', data, permission);
  }

  // DM CongDanDetail
  getCongDanDetailState(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdandetail/get', data, permission);
  }
  deleteCongDanDetail(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdandetail/delete', data, permission);
  }
  insertCongDanDetail(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdandetail/insert', data, permission);
  }
  editCongDanDetail(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdandetail/update', data, permission);
  }
  editCongDanDetailLatest(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdandetail/updatelatest', data, permission);
  }

  // DM DotTuyenQuan
  getDotTuyenQuan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dottuyenquan/get', data, permission);
  }
  getDotTuyenQuanByDate(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dottuyenquan/getbydate', data, permission);
  }
  deleteDotTuyenQuan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dottuyenquan/delete', data, permission);
  }
  insertDotTuyenQuan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dottuyenquan/insert', data, permission);
  }
  editDotTuyenQuan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dottuyenquan/update', data, permission);
  }
  // QuanHe
  getQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/quanhe/get', data, permission);
  }
  deleteQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/quanhe/delete', data, permission);
  }
  insertQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/quanhe/insert', data, permission);
  }
  editQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/quanhe/update', data, permission);
  }
  // KeHoachDbdv
  getKeHoachDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/kehoachdbdv/get', data, permission);
  }
  deleteKeHoachDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/kehoachdbdv/delete', data, permission);
  }
  insertKeHoachDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/kehoachdbdv/insert', data, permission);
  }
  editKeHoachDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/kehoachdbdv/update', data, permission);
  }
  // TheoDoiHuanLuyen
  getTheoDoiHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/theodoihuanluyen/get', data, permission);
  }
  deleteTheoDoiHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/theodoihuanluyen/delete', data, permission);
  }
  insertTheoDoiHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/theodoihuanluyen/insert', data, permission);
  }
  editTheoDoiHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/theodoihuanluyen/update', data, permission);
  }
  // DotKeHoach
  getDotKeHoach(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dotkehoach/get', data, permission);
  }
  deleteDotKeHoach(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dotkehoach/delete', data, permission);
  }
  insertDotKeHoach(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dotkehoach/insert', data, permission);
  }
  editDotKeHoach(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dotkehoach/update', data, permission);
  }
  // DotKeHoachHuyDong
  getDotKeHoachHuyDong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dotkehoachhuydong/get', data, permission);
  }
  deleteDotKeHoachHuyDong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dotkehoachhuydong/delete', data, permission);
  }
  // insertDotKeHoachHuyDong(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('tq/dotkehoachhuydong/insert', data, permission);
  // }
  updateDotKeHoachHuyDong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dotkehoachhuydong/update', data, permission);
  }
}
