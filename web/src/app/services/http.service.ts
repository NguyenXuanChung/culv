import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { StorageService } from '../services/storage.service';
import { AuthConstants } from '../config/auth-constants';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  token: string;

  constructor(
    private router: Router,
    private http: HttpClient,
    private storageService: StorageService,
  ) {
    this.storageService.get(AuthConstants.TOKEN).then(res => {
      this.token = res;
    });
  }

  refreshToken() {
    this.storageService.get(AuthConstants.TOKEN).then(res => {
      this.token = res;
    });
  }

  post_authenticate(serviceName: string, data: any) {
    const url = environment.apiUrl + serviceName;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers: headers };
    return this.http.post(url, JSON.stringify(data), options);
  }

  post(serviceName: string, data: any) {
    const url = environment.apiUrl + serviceName;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-access-token': `${this.token}`
    });
    const options = { headers: headers };
    return this.http.post<any>(url, JSON.stringify(data), options);
  }

  postAttachFile(serviceName: string, data: any) {
    const url = environment.apiUrl + serviceName;
    const headers = new HttpHeaders({
      'x-access-token': `${this.token}`,
    });
    const options = { headers: headers };
    return this.http.post<any>(url, data, options);
  }

  get(serviceName: string, permission: any) {
    const url = environment.apiUrl + serviceName;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-access-token': `${this.token}`,
      'x-access-permission': `${JSON.stringify(permission)}`
    });
    const options = { headers: headers };
    return this.http.get(url, options);
  }

  downloadAttachFile(serviceName: string, data: any) {
    const url = environment.apiUrl + serviceName;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-access-token': `${this.token}`
    });
    const options = { headers: headers };
    return this.http.post<any>(url, data, { ...options, responseType: 'blob' as 'json' });
  }

  postPermission(serviceName: string, data: any, permission: any) {
    const url = environment.apiUrl + serviceName;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-access-token': `${this.token}`,
      'x-access-permission': `${JSON.stringify(permission)}`
    });
    const options = { headers: headers };
    return this.http.post<any>(url, JSON.stringify(data), options);
  }
  postPermissionAttachFile(serviceName: string, data: any, permission: any) {
    const url = environment.apiUrl + serviceName;
    const headers = new HttpHeaders({
      // 'Content-Type': 'multipart/form-data',
      'x-access-token': `${this.token}`,
      'x-access-permission': `${JSON.stringify(permission)}`
    });
    const options = { headers: headers };
    return this.http.post<any>(url, data, options);
  }
  postFile(serviceName: string, data: any, permission: any) {
    const url = environment.apiUrl + serviceName;
    const headers = new HttpHeaders({
      // 'Content-Type': 'multipart/form-data',
      'x-access-token': `${this.token}`,
      'x-access-permission': `${JSON.stringify(permission)}`
    });
    const options = { headers: headers };
    return this.http.post<any>(url, data, options);
  }
  downloadAttachFilePermission(serviceName: string, data: any, permission: any) {
    const url = environment.apiUrl + serviceName;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-access-permission': `${JSON.stringify(permission)}`,
      'x-access-token': `${this.token}`
    });
    const options = { headers: headers };
    return this.http.post<any>(url, data, { ...options, responseType: 'blob' as 'json' });
  }
}
