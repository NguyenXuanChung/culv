import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class QltbService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  // textToSpeech(data: any, option: any): Observable<any> {
  //   return this.httpService.postAptFpt(option, data);
  // }
  getT2S(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/thongbao/get-t2s', data, permission);
  }
   //#region Duyet
  // Retrieve data with condition in body
  getDuyet(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/thongbao/duyet/get', data, permission);
  }
  // Create a new item
  insertDuyet(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/thongbao/duyet/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editDuyet(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/thongbao/duyet/update', data, permission);
  }
  // Delete a item with id
  deleteDuyet(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/thongbao/duyet/delete', data, permission);
  }
  //#endregion
  //#region DmKenh
  // Retrieve data with condition in body
  getDmKenh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-kenh/get', data, permission);
  }
  // Create a new item
  insertDmKenh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-kenh/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editDmKenh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-kenh/update', data, permission);
  }
  // Delete a item with id
  deleteDmKenh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-kenh/delete', data, permission);
  }
  //#endregion
  //#region DmCheDoPhat
  // Retrieve data with condition in body
  getDmCheDoPhat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-chedophat/get', data, permission);
  }
  // Create a new item
  insertDmCheDoPhat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-chedophat/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editDmCheDoPhat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-chedophat/update', data, permission);
  }
  // Delete a item with id
  deleteDmCheDoPhat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-chedophat/delete', data, permission);
  }
  //#endregion
  //#region DmKieuLap
  // Retrieve data with condition in body
  getDmKieuLap(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-kieulap/get', data, permission);
  }
  // Create a new item
  insertDmKieuLap(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-kieulap/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editDmKieuLap(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-kieulap/update', data, permission);
  }
  // Delete a item with id
  deleteDmKieuLap(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-kieulap/delete', data, permission);
  }
  //#endregion
  //#region DmKieuPhat
  // Retrieve data with condition in body
  getDmKieuPhat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-kieuphat/get', data, permission);
  }
  // Create a new item
  insertDmKieuPhat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-kieuphat/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editDmKieuPhat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-kieuphat/update', data, permission);
  }
  // Delete a item with id
  deleteDmKieuPhat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-kieuphat/delete', data, permission);
  }
  //#endregion
  //#region DmLoaiThongBao
  // Retrieve data with condition in body
  getDmLoaiThongBao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-loaithongbao/get', data, permission);
  }
  // Create a new item
  insertDmLoaiThongBao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-loaithongbao/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editDmLoaiThongBao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-loaithongbao/update', data, permission);
  }
  // Delete a item with id
  deleteDmLoaiThongBao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-loaithongbao/delete', data, permission);
  }
  //#endregion
  //#region DmLoaiTin
  // Retrieve data with condition in body
  getDmLoaiTin(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-loaitin/get', data, permission);
  }
  // Create a new item
  insertDmLoaiTin(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-loaitin/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editDmLoaiTin(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-loaitin/update', data, permission);
  }
  // Delete a item with id
  deleteDmLoaiTin(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-loaitin/delete', data, permission);
  }
  //#endregion
  //#region DmTrangThaiDuyet
  // Retrieve data with condition in body
  getDmTrangThaiDuyet(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-trangthaiduyet/get', data, permission);
  }
  // Create a new item
  insertDmTrangThaiDuyet(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-trangthaiduyet/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editDmTrangThaiDuyet(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-trangthaiduyet/update', data, permission);
  }
  // Delete a item with id
  deleteDmTrangThaiDuyet(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-trangthaiduyet/delete', data, permission);
  }
  //#endregion
  //#region DmTrangThaiPhat
  // Retrieve data with condition in body
  getDmTrangThaiPhat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-trangthaiphat/get', data, permission);
  }
  // Create a new item
  insertDmTrangThaiPhat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-trangthaiphat/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editDmTrangThaiPhat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-trangthaiphat/update', data, permission);
  }
  // Delete a item with id
  deleteDmTrangThaiPhat(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/dm-trangthaiphat/delete', data, permission);
  }
  //#endregion
   //#region ThongBao
  // Retrieve data with condition in body
  getThongBao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/thongbao/get', data, permission);
  }
   // Retrieve data with condition in body
   getListThongBao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/thongbao/get-by-diaban', data, permission);
  }
  // Create a new item
  insertThongBao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/thongbao/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editThongBao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/thongbao/update', data, permission);
  }
  // Delete a item with id
  deleteThongBao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/thongbao/delete', data, permission);
  }
  //#endregion
  //#region Group
  // // Retrieve data with condition in body
  // getGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/get', data, permission);
  // }
  // // Create a new item
  // insertGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/insert', data, permission);
  // }
  // // Update a item with condition {id: id}
  // editGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/update', data, permission);
  // }
  // // Delete a item with id
  // deleteGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/delete', data, permission);
  // }
  // //#endregion
  //#region Group
  // // Retrieve data with condition in body
  // getGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/get', data, permission);
  // }
  // // Create a new item
  // insertGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/insert', data, permission);
  // }
  // // Update a item with condition {id: id}
  // editGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/update', data, permission);
  // }
  // // Delete a item with id
  // deleteGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/delete', data, permission);
  // }
  // //#endregion
  //#region Group
  // // Retrieve data with condition in body
  // getGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/get', data, permission);
  // }
  // // Create a new item
  // insertGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/insert', data, permission);
  // }
  // // Update a item with condition {id: id}
  // editGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/update', data, permission);
  // }
  // // Delete a item with id
  // deleteGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/delete', data, permission);
  // }
  // //#endregion
  //#region Group
  // // Retrieve data with condition in body
  // getGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/get', data, permission);
  // }
  // // Create a new item
  // insertGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/insert', data, permission);
  // }
  // // Update a item with condition {id: id}
  // editGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/update', data, permission);
  // }
  // // Delete a item with id
  // deleteGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/delete', data, permission);
  // }
  // //#endregion
  //#region Group
  // // Retrieve data with condition in body
  // getGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/get', data, permission);
  // }
  // // Create a new item
  // insertGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/insert', data, permission);
  // }
  // // Update a item with condition {id: id}
  // editGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/update', data, permission);
  // }
  // // Delete a item with id
  // deleteGroup(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('mdm/groups/delete', data, permission);
  // }
  // //#endregion
}
