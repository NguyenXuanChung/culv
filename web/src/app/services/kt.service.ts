import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class KtService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  //#region DotKhenThuong
  // Retrieve data with condition in body
  getDotKhenThuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dotkhenthuong/get', data, permission);
  }
  getListDotKhenThuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dotkhenthuong/getlist', data, permission);
  }
  getDsCaNhan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dotkhenthuong/getcanhan', data, permission);
  }
  getDsTapThe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dotkhenthuong/gettapthe', data, permission);
  }
  getDanhSach(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dotkhenthuong/getdanhsach', data, permission);
  }
  insertDanhSach(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dotkhenthuong/insertdanhsach', data, permission);
  }
  updateDanhSach(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dotkhenthuong/updatedanhsach', data, permission);
  }
  // Create a new item
  insertDotKhenThuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dotkhenthuong/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editDotKhenThuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dotkhenthuong/update', data, permission);
  }
  updateDs(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dotkhenthuong/updateds', data, permission);
  }
  // Delete a item with id
  deleteDotKhenThuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dotkhenthuong/delete', data, permission);
  }
  deleteDs(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dotkhenthuong/deleteds', data, permission);
  }
  //#endregion

    //#region CapQuyetDinh
  // Retrieve data with condition in body
  getCapQuyetDinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dm-capquyetdinh/get', data, permission);
  }
  // Create a new item
  insertCapQuyetDinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dm-capquyetdinh/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editCapQuyetDinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dm-capquyetdinh/update', data, permission);
  }
  // Delete a item with id
  deleteCapQuyetDinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dm-capquyetdinh/delete', data, permission);
  }
  //#endregion
    //#region DanhHieuHinhThuc
  // Retrieve data with condition in body
  getDanhHieuHinhThuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dm-danhhieuhinhthuc/get', data, permission);
  }
  // Create a new item
  insertDanhHieuHinhThuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dm-danhhieuhinhthuc/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editDanhHieuHinhThuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dm-danhhieuhinhthuc/update', data, permission);
  }
  // Delete a item with id
  deleteDanhHieuHinhThuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dm-danhhieuhinhthuc/delete', data, permission);
  }
  //#endregion
    //#region PhanLoai
  // Retrieve data with condition in body
  getPhanLoai(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dm-phanloai/get', data, permission);
  }
  // Create a new item
  insertPhanLoai(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dm-phanloai/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editPhanLoai(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dm-phanloai/update', data, permission);
  }
  // Delete a item with id
  deletePhanLoai(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('kt/dm-phanloai/delete', data, permission);
  }
  //#endregion
}
