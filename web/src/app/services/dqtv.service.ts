import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class DqtvService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  getChiBo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/chibo/get', data, permission);
  }
  getListChiBo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/chibo/getlist', data, permission);
  }
  insertChiBo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/chibo/insert', data, permission);
  }
  editChiBo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/chibo/update', data, permission);
  }
  deleteChiBo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/chibo/delete', data, permission);
  }

  getChiDoan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/chidoan/get', data, permission);
  }
  getListChiDoan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/chidoan/getlist', data, permission);
  }
  insertChiDoan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/chidoan/insert', data, permission);
  }
  editChiDoan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/chidoan/update', data, permission);
  }
  deleteChiDoan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/chidoan/delete', data, permission);
  }
  getToChuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/tochuc/get', data, permission);
  }
  getListToChuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/tochuc/getlist', data, permission);
  }
  insertToChuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/tochuc/insert', data, permission);
  }
  editToChuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/tochuc/update', data, permission);
  }
  deleteToChuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/tochuc/delete', data, permission);
  }
  getKeHoachLuanPhien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/kehoachluanphien/get', data, permission);
  }
  insertKeHoachLuanPhien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/kehoachluanphien/insert', data, permission);
  }
  editKeHoachLuanPhien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/kehoachluanphien/update', data, permission);
  }
  deleteKeHoachLuanPhien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/kehoachluanphien/delete', data, permission);
  }

  getDotDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dotdaotao/get', data, permission);
  }
  getListDotDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dotdaotao/getlist', data, permission);
  }
  getDsDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dotdaotao/getdsdaotao', data, permission);
  }
  updateDsDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dotdaotao/updateds', data, permission);
  }
  deleteDsDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dotdaotao/deleteds', data, permission);
  }
  insertDotDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dotdaotao/insert', data, permission);
  }
  editDotDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dotdaotao/update', data, permission);
  }
  updateKetQuaDotDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dotdaotao/updateketqua', data, permission);
  }
  deleteDotDaoTao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dotdaotao/delete', data, permission);
  }

  getXayDungTruSo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/xaydungtruso/get', data, permission);
  }
  getListXayDungTruSo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/xaydungtruso/getlist', data, permission);
  }
  insertXayDungTruSo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/xaydungtruso/insert', data, permission);
  }
  editXayDungTruSo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/xaydungtruso/update', data, permission);
  }
  deleteXayDungTruSo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/xaydungtruso/delete', data, permission);
  }

  getDqtv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/get', data, permission);
  }
  getCanBo(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/getcanbo', data, permission);
  }
  getListDqtvNguon(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/getlist', data, permission);
  }
  getListDqtv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/getdqtv', data, permission);
  }
  getBienChe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/getbc', data, permission);
  }
  insertDqtv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/insert', data, permission);
  }
  editDqtv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/update', data, permission);
  }
  deleteDqtv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/delete', data, permission);
  }
  getDotHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dothuanluyen/get', data, permission);
  }
  getListDotHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dothuanluyen/getlist', data, permission);
  }
  getDsHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dothuanluyen/getdshuanluyen', data, permission);
  }
  updateDsHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dothuanluyen/updateds', data, permission);
  }
  updateKetQuaHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dothuanluyen/updateketqua', data, permission);
  }
  deleteDsHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dothuanluyen/deleteds', data, permission);
  }
  insertDotHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dothuanluyen/insert', data, permission);
  }
  editDotHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dothuanluyen/update', data, permission);
  }
  deleteDotHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/dothuanluyen/delete', data, permission);
  }
  getHoatDong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/hoatdong/get', data, permission);
  }
  insertHoatDong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/hoatdong/insert', data, permission);
  }
  editHoatDong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/hoatdong/update', data, permission);
  }
  deleteHoatDong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/hoatdong/delete', data, permission);
  }
  getDsHoatDong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/hoatdong/getds', data, permission);
  }
  updateDsHoatDong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/hoatdong/updateds', data, permission);
  }
  deleteDsHoatDong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/hoatdong/deleteds', data, permission);
  }
  getVuKhi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/vukhi/get', data, permission);
  }
  getVukhiDs(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/vukhi/getds', data, permission);
  }
  insertVuKhi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/vukhi/insert', data, permission);
  }
  editVuKhi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/vukhi/update', data, permission);
  }
  deleteVuKhi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/vukhi/delete', data, permission);
  }
}
