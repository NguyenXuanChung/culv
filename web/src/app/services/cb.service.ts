import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class CbService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  //#region CanBo
  // Retrieve data with condition in body
  getList(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('cb/canbo/get-list', data, permission);
  }
  //#endregion

}
