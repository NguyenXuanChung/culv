import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class DangKyNghiService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  // Retrieve data with condition in body
  getDangKyNghi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dangkynghi/get', data, permission);
  }
  getDaDuyet(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dangkynghi/get-daduyet', data, permission);
  }
  getChoDuyet(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dangkynghi/get-choduyet', data, permission);
  }
  insertDangKyNghi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dangkynghi/insert', data, permission);
  }
  getPhep(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dangkynghi/get-phep', data, permission);
  }

}
