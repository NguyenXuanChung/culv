const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(fileUpload({ createParentPath: true, limits: { fileSize: 50 * 1024 * 1024 }, abortOnLimit: true }));
app.use(cors());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get('/', (req, res) => {
	res.json({ message: 'Welcome to Customer API application.' });
});

// Quản trị hệ thống
require('./app/routes/auth.routes')(app);

// Chấm công
require('./app/routes/cc_thoiGian.routes')(app);
require('./app/routes/cc_theoDoiPhep.routes')(app);
require('./app/routes/info.routes')(app);
require('./app/routes/ttbh.routes')(app);
require('./app/routes/dm.routes')(app);
require('./app/routes/vb.routes')(app);
require('./app/routes/cb.routes')(app);
require('./app/routes/sys_workflow.routes')(app);
require('./app/routes/luong.routes')(app);
require('./app/routes/donVi.routes')(app);
require('./app/routes/sys_attachFiles.routes')(app);
require('./app/routes/qt.routes')(app);

var server = require('http').createServer(app);
// set port, listen for requests
const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}.`);
});
