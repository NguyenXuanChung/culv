const config = require("../config/sys.config");
const PizZip = require("pizzip");
const Docxtemplater = require("docxtemplater");
const ImageModule = require("docxtemplater-image-module-free");
const { CanvasRenderService } = require("chartjs-node-canvas");
const fs = require("fs");
const path = require("path");

// constructor
const Word = function () {};

const chartJsFactory = () => {
  const Chart = require("chart.js");
  require("chartjs-plugin-datalabels");
  delete require.cache[require.resolve("chart.js")];
  delete require.cache[require.resolve("chartjs-plugin-datalabels")];
  return Chart;
};

Word.exportWord = async (json, res) => {
  // Load the docx file as a binary
  var content = fs.readFileSync(
    path.resolve(`./${config.WORD_TEMPLATE}${json.template}`),
    "binary"
  );

  var tasks = [];

  if (json.data.chart) {
    json.data.chart.forEach((element) => {
      if (element.type === "line") {
        tasks.push(async () => {
          await Word.mkLineChart(
            json.data[element.datasource],
            element.label,
            element.value,
            element.title,
            element.chartName
          );
        });
      } else if (element.type === "bar") {
        tasks.push(async () => {
          await Word.mkBarChart(
            json.data[element.datasource],
            element.label,
            element.value,
            element.title,
            element.chartName
          );
        });
      }
    });
  }

  await Promise.all(tasks.map((p) => p())).then(() => {
    var zip = new PizZip(content);
    var doc;
    try {
      var opts = {};
      opts.centered = false;
      opts.getImage = function (tagValue, tagName) {
        return fs.readFileSync(tagValue);
      };

      opts.getSize = function (img, tagValue, tagName) {
        return [600, 400];
      };

      var imageModule = new ImageModule(opts);

      doc = new Docxtemplater().loadZip(zip).attachModule(imageModule); // (zip);
    } catch (error) {
      // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
      Word.errorHandler(error);
    }

    // Set the templateVariables
    doc.setData(json.data);

    try {
      doc.render();
    } catch (error) {
      // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
      Word.errorHandler(error);
    }

    res.setHeader("Content-Type", `${json.fileSave}.docx`);
    res.setHeader(
      "Content-Disposition",
      `attachment; filename=${json.fileSave}`
    );
    buf = doc.getZip().generate({ type: "nodebuffer" });
    res.send(buf);
    res.end();
  });
};

Word.replaceErrors = (key, value) => {
  if (value instanceof Error) {
    return Object.getOwnPropertyNames(value).reduce(function (error, key) {
      error[key] = value[key];
      return error;
    }, {});
  }
  return value;
};

Word.errorHandler = (error) => {
  console.log(JSON.stringify({ error: error }, Word.replaceErrors));
  if (error.properties && error.properties.errors instanceof Array) {
    const errorMessages = error.properties.errors
      .map(function (error) {
        return error.properties.explanation;
      })
      .join("\n");
    console.log("errorMessages", errorMessages);
  }
  throw error;
};

Word.mkBarChart = async (data, label, value, title, chartName) => {
  var configuration = {
    type: "bar",
    data: {
      labels: Object.values(data.map((item) => item[label])),
      datasets: [
        {
          label: title,
          data: Object.values(data.map((item) => item[value])),
          backgroundColor: "rgba(54, 162, 235, 1)",
          borderColor: "rgba(255,99,132,1)",
          borderWidth: 1,
          datalabels: {
            align: "end",
            anchor: "start",
          },
        },
      ],
    },
    options: {
      plugins: {
        datalabels: {
          backgroundColor: function (context) {
            return context.dataset.backgroundColor;
          },
          borderRadius: 4,
          color: "white",
          font: {
            weight: "bold",
          },
          formatter: Math.round,
        },
      },
      scales: {
        yAxes: [
          {
            ticks: {
              precision: 0,
              beginAtZero: true,
            },
          },
        ],
      },
    },
  };

  const canvasRenderService = new CanvasRenderService(
    600,
    400,
    undefined,
    undefined,
    chartJsFactory
  );
  var file = `./uploads/temp/${chartName}`;
  if (fs.existsSync(file)) {
    fs.unlinkSync(file);
  }
  fs.writeFileSync(
    file,
    await canvasRenderService.renderToBuffer(configuration)
  );
};

Word.mkLineChart = async (data, label, value, title, chartName) => {
  var configuration = {
    type: "line",
    data: {
      labels: Object.values(data.map((item) => item[label])),
      datasets: [
        {
          label: title,
          data: Object.values(data.map((item) => item[value])),
          borderColor: "rgba(54, 162, 235, 1)",
          borderWidth: 1,
          datalabels: {
            align: "end",
            anchor: "start",
          },
        },
      ],
    },
    options: {
      plugins: {
        datalabels: {
          color: "black",
          display: function (context) {
            return context.dataset.data[context.dataIndex] > 15;
          },
          font: {
            weight: "bold",
          },
          formatter: Math.round,
        },
      },
      scales: {
        yAxes: [
          {
            ticks: {
              precision: 0,
              beginAtZero: true,
            },
          },
        ],
      },
    },
  };

  const canvasRenderService = new CanvasRenderService(
    600,
    400,
    undefined,
    undefined,
    chartJsFactory
  );
  var file = `./uploads/temp/${chartName}`;
  if (fs.existsSync(file)) {
    fs.unlinkSync(file);
  }
  fs.writeFileSync(
    file,
    await canvasRenderService.renderToBuffer(configuration)
  );
};

module.exports = Word;
