const sql = require("mssql");
const dbConfig = require("../config/db.config");

var connection = new sql.ConnectionPool({
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  server: dbConfig.HOST,
  database: dbConfig.DATABASE,
  port: dbConfig.PORT,
  connectionTimeout: dbConfig.TIMEOUT,
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMillis: 60000
  },
  options: {
    instanceName: dbConfig.INSTANCE,
    encrypt: false,
    enableArithAbort: true,
  }
});

connection.on('error', err => {
  console.log('sql errors', err);
  if (err) {
    console.log('sql errors', err);
  }
  if (!err) {
    connection.connect();
  }
});

module.exports = connection;