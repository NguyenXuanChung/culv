const ExcelJS = require('exceljs');
const config = require('../config/sys.config');
const htmlToText = require('html-to-text');

// constructor
const Excel = function () {};

Excel.exprortTemplate = async (json, res) => {
	var wb = new ExcelJS.Workbook();
	// Đọc file template
	await wb.xlsx.readFile(`${config.EXCEL_TEMPLATE}${json.templateFile}`);

	// Truy cập vào worksheet
	var ws = await wb.getWorksheet(json.templateSheet);

	// Replace thông tin báo cáo
	json.replaces.forEach((e) => {
		if (ws.getCell(e.cell).value) {
			ws.getCell(e.cell).value = ws.getCell(e.cell).value.replace(e.text, e.value);
		}
	});

	// Thiết lập nội dung báo cáo
	if (json.data.length > 0 && json.table.colCode) {
		// Insert thêm dòng
		if (json.data.length > 3) {
			ws.duplicateRow(json.table.colStart + 1, json.data.length - 3, true);
		}

		// Fill nội dung
		let row,
			c,
			code,
			rowCode = ws.getRow(json.table.colCode);
		var i = 0;
		json.data.forEach((e) => {
			row = ws.getRow(json.table.colStart + i);
			c = 1;
			code = rowCode.getCell(c).value;
			do {
				if (code.toLowerCase() === 'stt') {
					row.getCell(c).value = i + 1;
				} else if (code.indexOf('html.') === 0) {
					row.getCell(c).value = htmlToText.fromString(e[code.substring(5, code.length)]);
				} else row.getCell(c).value = e[code];
				c++;
				code = rowCode.getCell(c).value;
			} while (code);
			i++;
		});
	}

	// Ẩn các dòng config
	// Ẩn dòng mã
	if (json.table.colCode) {
		ws.getRow(json.table.colCode).hidden = true;
	}
	// Ẩn dòng format
	ws.getRow(1).hidden = true;

	// Trả về file excel
	// res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	res.setHeader('Content-Type', json.fileSave);
	res.setHeader('Content-Disposition', `attachment; filename=${json.fileSave}`);
	return wb.xlsx.write(res).then(function () {
		res.status(200).end();
	});
};

Excel.exportDynamicTable = async (json, res) => {
	var wb = new ExcelJS.Workbook();
	// Đọc file template
	await wb.xlsx.readFile(`${config.EXCEL_TEMPLATE}${json.templateFile}`);

	// Truy cập vào worksheet
	var ws = await wb.getWorksheet(json.templateSheet);

	// Replace thông tin báo cáo
	json.replaces.forEach((e) => {
		ws.getCell(e.cell).value = ws.getCell(e.cell).value.replace(e.text, e.value);
	});
	ws.mergeCells(4, 1, 4, json.header.length);

	// Fill nội dung
	ws.getRow(6).values = json.header;
	// Set style
	for (i = 1; i <= json.header.length; i++) {
		ws.getCell(6, i).border = config.CELL_BORDER;
		ws.getCell(6, i).font.bold = true;
	}
	ws.columns = json.columns;
	var i = 7;
	json.data.forEach((element) => {
		ws.getRow(i).values = element;
		for (j = 1; j <= json.header.length; j++) {
			ws.getCell(i, j).border = config.CELL_BORDER;
			ws.getCell(i, j).alignment = { wrapText: true };
			ws.getCell(i, j).font = config.CELL_FONT;
		}
		i++;
	});

	// Trả về file excel
	// res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	res.setHeader('Content-Type', `${json.fileSave}.${json.outputType}`);
	res.setHeader('Content-Disposition', `attachment; filename=${json.fileSave}`);
	return wb.xlsx.write(res).then(function () {
		res.status(200).end();
	});
};

module.exports = Excel;
