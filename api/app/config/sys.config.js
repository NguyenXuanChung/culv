module.exports = {
  CONG_TY: "Hưng Thịnh Group",
  TRUNG_TAM: "Công ty phần mềm Hưng Thịnh",
  EXCEL_TEMPLATE: "templates/excel/",
  WORD_TEMPLATE: "templates/word/",

  // REDIS
  REDIS_INFO: "userInfo",
  REDIS_MENU: "menu",
  REDIS_PERMISSION: "permission",
  REDIS_ANHCANBO: "anhCanBo",
  REDIS_PORTAL_INFO: "portalUserInfo",
  REDIS_PORTAL_PERMISSION: "portalPermission",

  // INTEGRATION
  BiHRP: "https://fit.hrmonline.vn/",
};
