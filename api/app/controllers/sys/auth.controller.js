const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const axios = require('axios');
const config = require('../../config/db.config');
const _const = require('../../config/sys.config');
const Common = require('../../models/common_mssql.model');
const redis = require('../../models/redis');
const tableName = "Account";

const Auth = function () { };

Auth.Login = async (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: 'Content can not be empty!',
		});
	}

	let msg = {};
	try {
		let account = {}, encryptPassword = "";

		var json = {
			table: tableName,
			data: { username: req.body.username, enable: 1 },
		};
		await Common.get(json, async (err, data) => {
			if (err) {
				if (err.kind === 'not_found') {
					return res.send({ auth: false, token: null, message: 'Account not found.' });
				} else {
					return res.send({ auth: false, token: null, message: err.message });
				}
			} else {
				account = data[0];

				await axios.post(`${_const.BiHRP}default.aspx/pEncrypt`, { password: req.body.password })
					.then((res) => {
						encryptPassword = res.data.d;
					}).catch((err) => {
						console.error(err);
					});

				// check if the password is valid
				if (account.Password == encryptPassword) {
					// Get permission
					let permission = {};

					json = {
						procedure: 'Account_GetPermission',
						data: { Account_ID: account.Account_ID },
					};

					await Common.callProcedure(json, (err, data) => {
						if (err) {
							if (err.kind === 'not_found') {
								msg = { error: { message: 'User permission not found.' } };
							} else {
								msg = { error: { ...err, message: err.message } };
							}
						} else {
							permission = convertPermission(data);
						}
					});

					var token = jwt.sign(
						{
							id: account.Account_ID,
							id_trungTam: account.ID_TrungTam,
							id_donvis: account.ID_DonVis,
							type_account: 'admin'

						},
						config.SECRET,
						{
							expiresIn: '1d', // expires in 1 days
						}
					);

					// REDIS set permission
					redis
						.set(`${_const.REDIS_PERMISSION}_${account.Account_ID}`, JSON.stringify(permission), 0)
						.then((result) => { });

					// REDIS remove User info
					redis.del(`${_const.REDIS_INFO}_${account.Account_ID}`).then();

					// return the information including token as JSON
					return res.send({ auth: true, token: token, permission: permission, message: 'Login successfully!' });
				} else {
					return res.send({ auth: false, token: null, message: 'Incorrect password!' });
				}
			}
		});
	} catch (error) {
		if (!msg) return res.send(error);
		else return res.send(msg);
	}
};
exports.login = Auth.Login;

Auth.getUserInfo = async (req, res) => {
	let info = JSON.parse(await redis.get(`${_const.REDIS_INFO}_${req.userId}`));
	var json = {};

	// Lấy thông tin người dùng
	if (!info) {
		json = {
			procedure: 'Account_GetByAccount_ID',
			data: { Account_ID: req.userId },
		};

		await Common.callProcedure(json, async (err, data) => {
			if (err) {
				if (err.kind === 'not_found') {
					res.send({
						error: { message: 'User info not found.' },
					});
				} else {
					res.send({
						error:
							{ ...err, message: err.message } ||
							`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
					});
				}
			} else {
				info = data;
				// REDIS set userinfo
				await redis.set(`${_const.REDIS_INFO}_${req.userId}`, JSON.stringify(data));
			}
		});
	}

	// // Lấy menu
	// json = {
	// 	procedure: 'portal_account_getMenu',
	// 	data: { _id_account: req.userId },
	// };
	// await Common.callProcedure(json, (err, data) => {
	// 	if (err) {
	// 		if (err.kind === 'not_found') {
	// 			res.send({
	// 				error: { message: 'Menu not found.' },
	// 			});
	// 		} else {
	// 			res.send({
	// 				error:
	// 					{ ...err, message: err.message } ||
	// 					`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
	// 			});
	// 		}
	// 	} else {
	// 		menus = Common.trimChildren(nest(data, 0, 0));
	// 		// REDIS set menu
	// 		// redis.set(`${_const.REDIS_MENU}_${req.userId}`, JSON.stringify(menus), 0).then((result) => {});
	// 	}
	// });

	// let avatar = '';
	// if (info[0].anhDaiDien) {
	// 	avatar = Common.imageToBase64(`./uploads/canbo/${info[0].id_canbo}/${info[0].anhDaiDien}`);
	// }
	res.send({
		userInfo: info,
		message: `Get user info successfully!`,
	});
};
exports.getUserInfo = Auth.getUserInfo;

const convertPermission = (data) => {
	var res = {};
	data.forEach(
		(v) =>
		(res[v.ObjectName] = {
			_view: v._View,
			_update: v._Update,
		})
	);
	return res;
};

const nest = (items, id = null, level = 0, link = 'parent_id') =>
	items
		.filter((item) => item[link] === id)
		.map((item) => ({
			name: item.name,
			url: item.url,
			icon: item.icon,
			level: level,
			class: `menu-level${level}`,
			// attributes: { collapse: true },
			children: nest(items, item.id, level + 1),
		}));
