const Common = require('../../models/common_mssql.model');
const bcrypt = require('bcryptjs');
const _ = require('lodash');
const tableName = 'SYS_AttachFiles';

const Attach = function () { };

// Find items with condition
exports.get = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
	};

	Common.get(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};

// Create and Save a new item
exports.create = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body, createBy: req.userId, createDate: Common.now() },
	};

	// Save data in the database
	Common.create(json, (err, data) => {
		if (err)
			res.send({
				error: { ...err, message: err.message } || `Some error occurred while creating the ${json.table}.`,
			});
		else res.send({ data: data, message: 'Item was created successfully!' });
	});
};

Attach.insertDetail = async (req, attachFiles, res) => {
	if (!attachFiles.status) {
		// Nếu trạng thái dữ liệu là 0 thì cập nhật lại thành 1
		json = {
			procedure: 'sys_attachfiles_update_hoatdong',
			data: { tableName: req.body.tableName, id_data: req.body.id_data },
		};

		await Common.callProcedure(json, (err, data) => {
			if (err) {
				if (err.kind === 'not_found') {
					res.send({
						error: { message: 'Not found data.' },
					});
				} else {
					res.send({
						error:
							{ ...err, message: err.message } ||
							`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
					});
				}
			}
		});
	}

	// Thực hiện thêm file đính kèm vào CSDL
	json = {
		procedure: 'sys_attachfiles_detail_add',
		data: {
			id_attachFiles: attachFiles.id,
			tableName: req.body.tableName,
			id_data: req.body.id_data,
			filenames: req.body.files,
			createBy: req.userId,
		},
	};

	await Common.callProcedure(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Call sys_attachfiles_detail_add successfully!' });
	});
}

exports.insert = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { tableName: req.body.tableName, id_data: req.body.id_data },
	};

	let attachFiles = [];

	Common.get(json, async (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				// Nếu chưa tồn tại thì thực hiện insert vào bảng sys_attachfiles
				if (req.body.id_canbo) {
					json.data = { ...json.data, id_canbo: req.body.id_canbo };
				}

				await Common.create(json, async (err, data) => {
					if (err)
						res.send({
							error:
								{ ...err, message: err.message } ||
								`Some error occurred while creating the ${json.table}.`,
						});
					else Attach.insertDetail(req, data, res);
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else Attach.insertDetail(req, data[0], res);
	});
};

// Update a item identified by the condition in the request
exports.update = (req, res) => {
	// Validate Request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body.data, updateBy: req.userId, updateDate: Common.now() },
		condition: req.body.condition,
	};

	Common.update(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
				});
			}
		} else res.send({ data: data, message: 'Item was updated successfully!' });
	});
};

// Delete items with the specified data in body in the request
exports.delete = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
		userId: req.userId,
	};

	Common.remove(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ message: `Item was deleted successfully!` });
	});
};

exports.uploads = (req, res) => {
	try {
		if (!req.files) {
			res.send({ error: { message: 'No file uploaded' } });
		} else {
			let data = [];
			//loop all files
			// File đầu tiên truyền thông tin tableName và id data nên chỉ lấy dữ liệu ở name
			// sẽ lưu vào bắt đầu tư file thứ 2.
			_.forEach(_.keysIn(req.files.attach), (key) => {
				let file;
				file = req.files.attach[key];
				if (key == 0) {
					const content = file.name.split(',');
					req.body.id_data = content[0];
					req.body.tableName = content[1];
				} else {
					//move photo to uploads directory
					file.mv(`./uploads/${req.body.tableName}/${req.body.id_data}/${file.name}`);
					//push file details
					data.push({
						name: file.name,
						mimetype: file.mimetype,
						size: file.size,
					});
				}
			});
			//return response
			res.send({ message: 'Files are uploaded', data: data });
		}
	} catch (err) {
		res.send({ error: { ...err, message: err.message } });
	}
};

exports.changeImage = (req, res) => {
	try {
		if (!req.files.attach[1]) {
			res.send({ error: { message: 'No file uploaded' } });
		} else {
			let file = req.files.attach[0];
			let content = file.name.split(',');
			req.body.id_data = content[0];
			req.body.tableName = content[1];
			req.body.fieldName = content[2];
			// Update dữ liệu vào bảng gốc
			file = req.files.attach[1];
			var json = {
				from: req.body.tableName,
				set: {
					[req.body.fieldName]: file.name,
				},
				where: {
					id: {
						operator: '=',
						value: req.body.id_data,
					},
				},
			};
			Common.updateAdvance(json, (err, data) => {
				if (err) {
					if (err.kind === 'not_found') {
						res.send({
							error: { message: 'Not found data.' },
						});
					} else {
						res.send({
							error:
								{ ...err, message: err.message } ||
								`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
						});
					}
				} else {
					// move photo to uploads directory
					file.mv(`./uploads/${req.body.tableName}/${req.body.id_data}/${file.name}`);
					let result = [];
					//push file details
					result.push({
						name: file.name,
						size: file.size,
					});
					//return response
					res.send({ message: 'Files uploaded', data: result });
				}
			});
		}
	} catch (err) {
		res.send({ error: { ...err, message: err.message } });
	}
};

exports.download = (req, res) => {
	const file = `.${req.body.path}`;
	res.download(file, (err) => {
		if (err) {
			res.send({ error: { ...err, message: err.message } });
		}
	});
};
