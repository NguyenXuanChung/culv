const Common = require('../../models/common_mssql.model');


exports.getWorkflow = async (req, res) => {
  var json = {
    table: 'SYS_Workflow',
    data: req.body,
  };
  await Common.get(json, async (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}


exports.getWorkflowState = async (req, res) => {
  var json = {
    table: 'SYS_Workflow_State',
    data: req.body,
  };
  await Common.get(json, async (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}

exports.getWorkflowStateAction = async (req, res) => {
  var json = {
    table: 'SYS_Workflow_State_Action',
    data: req.body,
  };
  await Common.get(json, async (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}
