const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const axios = require('axios');
const config = require('../../config/db.config');
const _const = require('../../config/sys.config');
const Common = require('../../models/common_mssql.model');
const redis = require('../../models/redis');
const tableName = "Portal_Account";

const Auth = function () { };

exports.login = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: 'Content can not be empty!',
		});
	}

	let msg = {};
	try {
		let account = {}, encryptPassword = "";

		var json = {
			table: tableName,
			data: { username: req.body.username, status: 1 },
		};
		Common.get(json, async (err, data) => {
			if (err) {
				if (err.kind === 'not_found') {
					return res.send({ auth: false, token: null, message: 'Account not found.' });
				} else {
					return res.send({ auth: false, token: null, message: err.message });
				}
			} else {
				account = data[0];

				await axios.post(`${_const.BiHRP}default.aspx/pEncrypt`, { password: req.body.password })
					.then((res) => {
						encryptPassword = res.data.d;
					}).catch((err) => {
						console.error(err);
					});

				json = {
					table: 'portal_account_password',
					data: { ID_Portal_Account: account.Portal_Account_ID, status: 1 },
				};

				let accountPassword = '';

				await Common.get(json, (err, data) => {
					if (err) {
						if (err.kind === 'not_found') {
							msg = { error: { message: 'Password not found.' } };
						} else {
							msg = { error: err.message };
						}
					} else {
						accountPassword = data[0].Password;
					}
				});
				// check if the password is valid
				if (encryptPassword == accountPassword) {
					// Get permission
					let permission = {};

					json = {
						procedure: 'Portal_Account_GetPermission',
						data: { Portal_Account_ID: account.Portal_Account_ID },
					};

					await Common.callProcedure(json, (err, data) => {
						if (err) {
							if (err.kind === 'not_found') {
								msg = { error: { message: 'User permission not found.' } };
							} else {
								msg = { error: { ...err, message: err.message } };
							}
						} else {
							permission = convertPermission(data);
						}
					});

					jsonInfo = {
						procedure: 'Portal_Account_GetUserInfo',
						data: { id_canBo: account.ID_CanBo },
					};

					await Common.callProcedure(jsonInfo, async (err, data) => {
						if (err) {
							if (err.kind === 'not_found') {
								res.send({
									error: { message: 'User info not found.' },
								});
							} else {
								res.send({
									error:
										{ ...err, message: err.message } ||
										`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
								});
							}
						} else {
							var token = jwt.sign(
								{
									id_portal: data[0].Portal_Account_ID,
									id_canBo: data[0].CanBo_ID,
									id_trungTam: data[0].ID_TrungTam,
									id_donvi: data[0].ID_DonVi,
									id_portal_nhomQuyen: data[0].ID_Portal_NhomQuyen,
									type_account: 'portal'
								},
								config.SECRET,
								{
									expiresIn: '30d', // expires in 30 days
								}
							);
							console.log(data);
							// REDIS set permission
							redis
								.set(`${_const.REDIS_PORTAL_PERMISSION}_${account.Portal_Account_ID}`, JSON.stringify(permission), 0)
								.then((result) => { });

							// REDIS remove User info
							redis.del(`${_const.REDIS_PORTAL_INFO}_${account.Portal_Account_ID}`).then();

							// return the information including token as JSON
							return res.send({ auth: true, token: token, message: 'Login successfully!' });
						}
					});
				} else {
					return res.send({ auth: false, token: null, message: 'Incorrect password!' });
				}
			}
		});
	} catch (error) {
		if (!msg) return res.send(error);
		else return res.send(msg);
	}
};

Auth.getUserInfo = async (req, res) => {
	let info = JSON.parse(await redis.get(`${_const.REDIS_PORTAL_INFO}_${req.userPortalId}`));
	var json = {};
	let permissions;
	let menu;
	// Lấy thông tin người dùng
	if (!info) {
		json = {
			procedure: 'Portal_Account_GetUserInfo',
			data: { id_canBo: req.canBoId },
		};

		await Common.callProcedure(json, async (err, data) => {
			if (err) {
				if (err.kind === 'not_found') {
					res.send({
						error: { message: 'User info not found.' },
					});
				} else {
					res.send({
						error:
							{ ...err, message: err.message } ||
							`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
					});
				}
			} else {
				info = data;
				// REDIS set userinfo
				await redis.set(`${_const.REDIS_PORTAL_INFO}_${req.portalId}`, JSON.stringify(data));
			}
		});
	}

	// Lấy quyền truy cập
	if (!permissions) {
		json = {
			procedure: 'portal_account_getPermission',
			data: { portal_account_id: req.portalId },
		};
		await Common.callProcedure(json, async (err, permission) => {
			if (err) {
				if (err.kind === 'not_found') {
					res.send({
						error: { message: 'User permission not found.' },
					});
				} else {
					res.send({
						error:
							{ ...err, message: err.message } ||
							`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
					});
				}
			} else {
				permissions = convertPermission(permission);
				// REDIS set permission
				await redis.set(`${_const.REDIS_PORTAL_PERMISSION}_${req.portalId}`, JSON.stringify(permissions), 0);
			}
		});
	}

	// Lấy menu
	json = {
		procedure: 'portal_account_getMenu',
		data: { portal_Account_ID: req.portalId },
	};
	await Common.callProcedure(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Menu not found.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
				});
			}
		} else {
			menus = Common.trimChildren(nest(data, 0, 0));
			// REDIS set menu
			// redis.set(`${_const.REDIS_MENU}_${req.portalId}`, JSON.stringify(menus), 0).then((result) => {});
		}
	});

	// let avatar = '';
	// if (info[0].anhDaiDien) {
	// 	avatar = Common.imageToBase64(`./uploads/canbo/${info[0].id_canbo}/${info[0].anhDaiDien}`);
	// }
	res.send({
		userInfo: info[0],
		permission: permissions,
		menu: menus,
		message: `Get user info successfully!`,
	});
};
exports.getUserInfo = Auth.getUserInfo;

const convertPermission = (data) => {
	var res = {};
	data.forEach(
		(v) =>
		(res[v.ObjectName] = {
			_view: v._View,
			_update: v._Update,
		})
	);
	return res;
};

const nest = (items, id = null, level = 0, link = 'parent_id') =>
	items
		.filter((item) => item[link] === id)
		.map((item) => ({
			name: item.name.trim(),
			url: item.url.trim(),
			icon: item.icon.trim(),
			level: level,
			class: `menu-level${level}`,
			// attributes: { collapse: true },
			children: nest(items, item.id, level + 1),
		}));
