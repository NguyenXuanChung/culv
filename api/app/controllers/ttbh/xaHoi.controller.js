const Common = require('../../models/common_mssql.model');


exports.getTrangThaiBhxh = async (req, res) => {
  var json = {
    procedure: 'Portal_DuLieuTrangThaiBHXH_GetByIDCanBo',
    data: {
      ID_CanBo: req.canBoId
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}



exports.getQuaTrinhBhyt = async (req, res) => {
  var json = {
    procedure: 'BH_CanBo_QuaTrinhBHYT_Get',
    data: {
      ID_CanBo: req.canBoId
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}


exports.getQuaTrinhBhxh = async (req, res) => {
  var json = {
    procedure: 'BH_CanBo_QuaTrinhBaoHiemXaHoi_GetByIdCanBo',
    data: {
      CanBo_Id: req.canBoId
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}