const Common = require('../../models/common_mssql.model');


exports.getBhhc = async (req, res) => {
  var json = {
    procedure: 'BHHC_GetDanhSachHopDongByID_CanBo',
    data: {
      ID_CanBo: req.canBoId, ngay: Common.now(),
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}
exports.getQuaTrinhBhhc = async (req, res) => {
  var json = {
    procedure: 'BHHC_QuaTrinhBaoHiemByID_CanBo_BaoHiem',
    data: {
      ID_CanBo_BaoHiem: req.body.ID_CanBo_BaoHiem
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}
