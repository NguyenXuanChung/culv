const Common = require('../../models/common_mssql.model');


exports.getBhnt = async (req, res) => {
  var json = {
    procedure: 'BHNT_GetDanhSachHopDongBH_ByID_CanBo',
    data: {
      ID_CanBo: req.canBoId, ngay: Common.now(),
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}



exports.getQuaTrinhBhnt = async (req, res) => {
  var json = {
    procedure: 'BHNT_QuaTrinhDongBaoHiemByID_CanBo_BH',
    data: {
      ID_CanBo_BH: req.body.ID_CanBo_BH
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}
