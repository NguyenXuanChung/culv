const Common = require('../../models/common_mssql.model');


exports.getQuaTrinhCongTac = async (req, res) => {
  var json = {
    procedure: 'CanBo_QuaTrinhCongTac_GetID_CanBo',
    data: {
      ID_CanBo: req.canBoId
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      console.log(data);
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}



exports.getQuaTrinhHopDong = async (req, res) => {
  var json = {
    procedure: 'CanBo_HopDongLaoDong_Get',
    data: {
      hopDongId: 0, CanBoId: req.canBoId
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}


exports.getGiamTruGiaCanh = async (req, res) => {
  var json = {
    procedure: 'TL_GiamTruGiaCanh_GetBy_ID_CanBo',
    data: {
      ID_CanBo: req.canBoId
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}


exports.getKhenThuongKyLuat = async (req, res) => {
  var json = {
    procedure: 'Portal_KhenThuongKyLuat_GetbyID_CanBo',
    data: {
      ID_CanBo: req.canBoId
    },
  };
  await Common.callProcedure(json, async (err1, data1) => {
    var json = {
      procedure: 'Portal_KyLuat_GetbyID_CanBo',
      data: {
        ID_CanBo: req.canBoId
      },
    };
    await Common.callProcedure(json, (err2, data2) => {
      if (err1 && err2 ) {
        if (err1.kind === "not_found"|| err2.kind === "not_found") {
          res.send({ error: { message: "Not found data." } });
        } else {
          res.send({ error: { ...err1, message: err1.message } });
        }
      } else {
        res.send({ data: [...data1, ...data2], message: "Retrieve data successfully!" });
      }
    });
  });
}
