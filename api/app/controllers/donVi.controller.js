const Common = require("../models/common_mssql.model");
const bcrypt = require("bcryptjs");
const tableName = "donvi";

// Find items with condition
exports.get = (req, res) => {
  var json = {
    table: "donvi",
    data: req.body,
  };

  Common.get(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({
          error: { message: "Not found data." },
        });
      } else {
        res.send({
          error:
            { ...err, message: err.message } ||
            `Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
        });
      }
    } else
      res.send({ data: data, message: `Get ${json.table} was successfully!` });
  });
};

// Create
exports.create = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.send({
      error: { message: "Content can not be empty!" },
    });
  }

  var json = {
    table: "donvi_master",
    data: {
      id_customer: req.customerId,
      tenDonViGoc: req.body.tenDonVi,
      ngayCapPhep: req.body.ngayBatDau,
      ngayBatDau: req.body.ngayBatDau,
      //ngayKetThuc: req.body.ngayKetThuc,
      //soGiayPhepThanhLap: req.body.soGiayPhepThanhLap,
      createBy: req.userId,
      createDate: Common.now(),
    },
  };

  Common.create(json, async (err, data) => {
    if (err)
      res.send({
        error:
          { ...err, message: err.message } ||
          `Some error occurred while creating the "donvi_master".`,
      });
    else {
      var donViMaster = data;
      json = {
        table: tableName,
        data: {
          id_customer: req.customerId,
          donVi_id: donViMaster.id,
          id_capTren: req.body.id_donvi,
          tuNgayDonVi: req.body.ngayBatDau,
          maDonVi: req.body.maDonVi,
          tenDonVi: req.body.tenDonVi,
          diaChi: req.body.diaChi,
          dienThoai: req.body.dienThoai,
          email: req.body.email,
          soDKKD: req.body.soDKKD,
          maSoThue: req.body.maSoThue,
          soTaiKhoan: req.body.soTaiKhoan,
          id_nganHang: req.body.id_nganHang,
          id_loaiHinhDonVi: req.body.id_loaiHinhDonVi,
          createBy: req.userId,
          createDate: Common.now(),
        },
      };

      await Common.create(json, async (err, data) => {
        if (err)
          res.send({
            error:
              { ...err, message: err.message } ||
              `Some error occurred while creating the ${json.table}.`,
          });
      });
      res.send({
        data: donViMaster,
        message: "Item was created successfully!",
      });
    }
  });
};

// Create
exports.createQT = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.send({
      error: { message: "Content can not be empty!" },
    });
  }

  var json = {
    table: tableName,
    data: {
      id_customer: req.customerId,
      donVi_id: req.body.donvi_id,
      id_capTren: req.body.id_donvi,
      tuNgayDonVi: req.body.ngayBatDau,
      maDonVi: req.body.maDonVi,
      tenDonVi: req.body.tenDonVi,
      diaChi: req.body.diaChi,
      dienThoai: req.body.dienThoai,
      email: req.body.email,
      soDKKD: req.body.soDKKD,
      maSoThue: req.body.maSoThue,
      soTaiKhoan: req.body.soTaiKhoan,
      id_nganHang: req.body.id_nganHang,
      id_loaiHinhDonVi: req.body.id_loaiHinhDonVi,
      khamBenh: req.body.data.khamBenh,
      capCuu: req.body.data.capCuu,
      khamSan: req.body.data.khamSan,
      noiTru: req.body.data.noiTru,
      createBy: req.userId,
      createDate: Common.now(),
    },
  };

  Common.create(json, async (err, data) => {
    if (err)
      res.send({
        error:
          { ...err, message: err.message } ||
          `Some error occurred while creating the "donvi_master".`,
      });
    else res.send({ data: data, message: "Item was created successfully!" });
  });
};

// Update
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.send({
      error: { message: "Content can not be empty!" },
    });
  }

  var json = {
    table: tableName,
    data: {
      tuNgayDonVi: req.body.data.ngayBatDau,
      maDonVi: req.body.data.maDonVi,
      tenDonVi: req.body.data.tenDonVi,
      diaChi: req.body.data.diaChi,
      dienThoai: req.body.data.dienThoai,
      email: req.body.data.email,
      soDKKD: req.body.data.soDKKD,
      maSoThue: req.body.data.maSoThue,
      soTaiKhoan: req.body.data.soTaiKhoan,
      id_nganHang: req.body.data.id_nganHang,
      id_loaiHinhDonVi: req.body.data.id_loaiHinhDonVi,
      khamBenh: req.body.data.khamBenh,
      capCuu: req.body.data.capCuu,
      khamSan: req.body.data.khamSan,
      noiTru: req.body.data.noiTru,
      updateBy: req.userId,
      updateDate: Common.now(),
    },
    condition: req.body.condition,
  };

  Common.update(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({
          error: { message: "Not found data." },
        });
      } else {
        res.send({
          error:
            { ...err, message: err.message } ||
            `Error updating ${json.table} with ${JSON.stringify(
              json.condition
            )}.`,
        });
      }
    } else res.send({ data: data, message: "Item was updated successfully!" });
  });
};

exports.delete = (req, res) => {
  var json = {
    table: "donvi_master",
    data: req.body,
    userId: req.userId,
  };

  Common.remove(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({
          error: { message: "Not found data." },
        });
      } else {
        res.send({
          error:
            { ...err, message: err.message } ||
            `Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
        });
      }
    } else {
      //Xóa các dữ liệu của bảng con
      json = {
        table: tableName,
        data: { status: 0, updateBy: req.userId, updateDate: Common.now() },
        condition: { donVi_id: req.body.donvi_id, status: 1 },
      };
      Common.update(json, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.send({
              error: { message: "Not found data." },
            });
          } else {
            res.send({
              error:
                { ...err, message: err.message } ||
                `Error updating ${json.table} with ${JSON.stringify(
                  json.condition
                )}.`,
            });
          }
        } else
          res.send({ data: data, message: "Item was updated successfully!" });
      });
    }
  });
};

//get điểm trung bình theo tiêu chí and Đợt
exports.getAllRelations = (req, res) => {
  var json = {
    procedure: "donvi_getAllRelations",
    data: { _id_donvi: req.body.id_donvi },
  };

  Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({
          error: { message: "Not found data." },
        });
      } else {
        res.send({
          error:
            { ...err, message: err.message } ||
            `Error retrieving ${json.procedure} with ${JSON.stringify(
              json.data
            )}`,
        });
      }
    } else res.send({ data: data, message: "Retrieve data successfully!" });
  });
};

exports.getTree = (req, res) => {
  var json = {
    procedure: "donvi_getTree",
    data: { id_customer: req.customerId, ...req.body },
  };

  Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({
          error: { message: "Not found data." },
        });
      } else {
        res.send({
          error:
            { ...err, message: err.message } ||
            `Error retrieving ${json.procedure} with ${JSON.stringify(
              json.data
            )}`,
        });
      }
    } else {
      res.send({
        data: nest(data, 0),
        message: "Get tree donvi successfully!",
      });
    }
  });
};

exports.getTree_Value = (req, res) => {
  var json = {
    procedure: "DonVi_GetByApplyDate",
    data: { id_trungTam: req.id_trungTam, ngayApDung: req.body.ngay },
  };
  Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({
          error: { message: "Not found data." },
        });
      } else {
        res.send({
          error:
            { ...err, message: err.message } ||
            `Error retrieving ${json.procedure} with ${JSON.stringify(
              json.data
            )}`,
        });
      }
    } else {
      res.send({
        data: nestValue(data),
        message: "Get tree donvi successfully!",
      });
    }
  });
};

exports.getTreeNode = (req, res) => {
  var json = {};
  if (!req.body) {
    json = {
      procedure: "donvi_getTree",
      data: { id_customer: req.customerId, ...req.body },
    };
  } else {
    json = {
      procedure: "donvi_getTree",
      data: {
        id_customer: req.customerId,
        id_trungTam: 1,
        tuNgay: Common.now(),
        denNgay: Common.now(),
      },
    };
  }

  Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({
          error: { message: "Not found data." },
        });
      } else {
        res.send({
          error:
            { ...err, message: err.message } ||
            `Error retrieving ${json.procedure} with ${JSON.stringify(
              json.data
            )}`,
        });
      }
    } else {
      res.send({
        data: Common.trimChildren(nestTreeNode(data, 0)),
        message: "Get tree donvi successfully!",
      });
    }
  });
};

const nest = (items, DonVi_ID = null, link = "ID_CapTren") =>
  items
    .filter((item) => item[link] === DonVi_ID)
    .map((item) => ({ ...item, children: nest(items, item.DonVi_ID) }));

const nestValue = (items, DonVi_ID = null, link = "ID_CapTren") =>
  items
    .filter((item) => item[link] === DonVi_ID)
    .map((item) => ({
      data: item.DonVi_ID,
      label: item.TenDonVi,
      ID_CapTren: item.ID_CapTren,
      hasPermission: item.hasPermission,
      children: nestValue(items, item.DonVi_ID),
    }));

// const nestNotPermission = (items) => items.filter((item) => item[`hasPermission`] == 0).map((item) => item.donvi_id);

const nestTreeNode = (items, DonVi_ID = null, link = "ID_CapTren") =>
  items
    .filter((item) => item[link] === DonVi_ID)
    .map((item) => ({
      data: { ...item },
      expanded: item.ID_CapTren == 0 ? true : false,
      children: nestTreeNode(items, item.DonVi_ID),
    }));
