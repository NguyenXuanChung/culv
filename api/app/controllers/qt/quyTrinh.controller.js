const Common = require('../../models/common_mssql.model');
const tableName = 'Portal_QuyTrinh_DeXuat'

exports.get = async (req, res) => {
  var json = {
    table: 'Portal_QuyTrinh',
    data: req.body,
  };
  await Common.get(json, async (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}
exports.getListDeXuat = async (req, res) => {
  var json = {
    procedure: 'Portal_QuyTrinh_DeXuat_Get',
    data: {
			ID_Portal_QuyTrinh: req.body.ID_Portal_QuyTrinh? req.body.ID_Portal_QuyTrinh: 0,
			Code_Portal_QuyTrinh: req.body.Code_Portal_QuyTrinh? req.body.Code_Portal_QuyTrinh: '',
			CodeState: req.body.CodeState,
			ID_Portal_NhomQuyenCreate: req.body.ID_Portal_NhomQuyenCreate ? req.body.ID_Portal_NhomQuyenCreate: 0,
			ID_SYS_Workflow_State: req.body.ID_SYS_Workflow_State? req.body.ID_SYS_Workflow_State: 0,
			TuNgay: req.body.TuNgay,
			DenNgay: req.body.DenNgay,
			CreateBy: req.body.CreateBy? req.body.CreateBy: 0 
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}

exports.getDetailDeXuat = async (req, res) => {
  var json = {
    procedure: 'Portal_QuyTrinh_DeXuat_Get_Detail',
    data: {
			CodeState: req.body.CodeState,
			ID_Portal_NhomQuyen: req.id_portal_nhomQuyen,
			Type: req.body.Type,
			ID_PortalQuyTrinh_DeXuat: req.body.ID_PortalQuyTrinh_DeXuat,
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data[0], message: "Retrieve data successfully!" });
    }
  });
}

exports.getDeXuat = async (req, res) => {
  var json = {
    table: tableName,
    data: req.body,
  };
  await Common.get(json, async (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}

exports.createDeXuat = async (req, res) => {
  var json = {
    procedure: 'Portal_QuyTrinh_DeXuat_Insert',
    data: {
			ID_NhomQuyenCreate: req.id_portal_nhomQuyen,
			CreateBy: req.portalId, 
			CreateDate: Common.now(),
			... req.body
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({message: "Item was created successfully!" });
    }
  });
}

// Update a item identified by the condition in the request
exports.updateDeXuat = (req, res) => {
	// Validate Request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body.data, updateBy: req.userId, updateDate: Common.now() },
		condition: req.body.condition,
	};

	Common.update(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
				});
			}
		} else res.send({ data: data, message: 'Item was updated successfully!' });
	});
};

// Delete items with the specified data in body in the request
exports.deleteDeXuat = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
		userId: req.userId,
	};

	Common.remove(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ message: `Item was deleted successfully!` });
	});
};




exports.getActivity = async (req, res) => {
  var json = {
    table: 'Portal_QuyTrinh_Activity',
    data: req.body,
  };
  await Common.get(json, async (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}

exports.getActivity = async (req, res) => {
  var json = {
    procedure: 'Portal_QuyTrinh_DeXuat_Get_Activity',
    data: {
			ID_Portal_QuyTrinh_DeXuat: req.body.ID_Portal_QuyTrinh_DeXuat,
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}

exports.createActivity = async (req, res) => {
  var json = {
    procedure: 'Portal_QuyTrinh_Activity_Insert',
    data: {
			CreateBy: req.portalId, 
			CreateDate: Common.now(),
			... req.body
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({message: "Item was created successfully!" });
    }
  });
}

exports.updateActivity = async (req, res) => {
  var json = {
    procedure: 'Portal_QuyTrinh_Activity_Edit',
    data: {
			CreateBy: req.portalId, 
			CreateDate: Common.now(),
			... req.body
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({message: "Item was updated successfully!" });
    }
  });
}

// Delete items with the specified data in body in the request
exports.deleteActivity = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
		userId: req.userId,
	};

	Common.remove(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ message: `Item was deleted successfully!` });
	});
};

