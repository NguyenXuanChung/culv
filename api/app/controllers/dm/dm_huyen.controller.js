const Common = require('../../models/common.model');
const tableName = 'dm_huyen';

//Find items with condition
exports.get = (req, res) => {
    var json = {
        table: tableName,
        data: req.body,
        orderBy: 'sortOrder ASC',
    };

     await Common.get(json, async (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
};

//create and save a new item
exports.create = (req, res) => {
    //validate request
    if (!req.body || req.body.lenght == 0) {
        res.send({
            error: { message: 'Content can not be empty' },
        })
    }
    var json = {
        table: tableName,
        data: { ...req.body, createBy: req.userId, createDate: Common.now() },
    }
    //save data in datatable
    Common.create(json, (err, data) => {
        if (err)
            res.send({
                errorr: { ...err, message: err.message || `Some error occurred while creating  the ${json.table}.` },
            });
        res.send({ data: data, message: 'Item was created succsessfully!' })
    })
};

//update a item identified  by the  condition in the  request
exports.update = (req, res) => {
    //validate request
    if (!req.body) {
        res.send({
            error: { message: 'Content can not  be empty' },
        })
    }
    var json = {
        table: tableName,
        data: { ...req.body.data, updateBy: req.userId, updateDate: Common.now() },
        condition: req.body.condition,
    };
    Common.update(json, (err, data) => {
        if (err) {
            if (err.kind === 'not_found') {
                res.send({
                    error: { message: 'Not found data' }
                });
            } else {
                res.send({
                    error:
                        { ...err, message: err.message } ||
                        `Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
                })
            }
        } else res.send({ data: data, message: 'Item was  updated succsessfully!' })
    });
};
// Delete items with the specified data in body in the request
exports.delete = (req, res) => {
    var json = {
        table: tableName,
        data: req.body,
        userId: req.userId,
    };

    Common.remove(json, (err, data) => {
        if (err) {
            if (err.kind === 'not_found') {
                res.send({
                    error: { message: 'Not found data.' },
                });
            } else {
                res.send({
                    error:
                        { ...err, message: err.message } ||
                        `Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
                });
            }
        } else {
            //Xóa các dữ liệu của bảng con
            json = {
                table: 'dm_xa',
                data: { status: 0, updateBy: req.userId, updateDate: Common.now() },
                condition: { id_huyen: req.body.id, status: 1 },
            };
            Common.update(json, (err, data) => {
                if (err) {
                    if (err.kind === 'not_found') {
                        res.send({
                            error: { message: 'Not found data.' },
                        })
                    } else {
                        res.send({
                            error:
                                { ...err, message: err.message } || `Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
                        })
                    }
                } else res.send({ data: data, message: 'Item was updated successfully!' });
            })
        }
    });
};




