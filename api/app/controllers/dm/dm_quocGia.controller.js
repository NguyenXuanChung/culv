const Common = require('../../models/common_mssql.model');
const tableName = "DM_QuocGia";


exports.get = async (req, res) => {
  var json = {
    table: tableName,
    data: req.body,
  };
  await Common.get(json, async (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}