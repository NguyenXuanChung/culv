const Common = require('../../models/common_mssql.model');


exports.getDangKy = async (req, res) => {
  var json = {
    procedure: 'Portal_TicketInfo_GetByUserCreate',
    data: {
      ID_Portal_Account: req.portalId, ID_TrungTam: req.id_trungTam,
      tuNgay: req.body.tuNgay, denNgay: req.body.denNgay, Lang: ''
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}
exports.getDaDuyet = async (req, res) => {
  var json = {
    procedure: 'Portal_TicketInfo_GetByAssignedApprover',
    data: {
      ID_Portal_Account: req.portalId, ID_Portal_NhomQuyen: req.body.ID_Portal_NhomQuyen, ID_TrungTam: req.id_trungTam,
      tuNgay: req.body.tuNgay, denNgay: req.body.denNgay, Lang: ''
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}
exports.getChoDuyet = async (req, res) => {
  var json = {
    procedure: 'Portal_TicketInfo_GetByWaitingForApproval',
    data: {
      ID_Portal_Account: req.portalId, ID_Portal_NhomQuyen: req.id_portal_nhomQuyen, ID_TrungTam: req.id_trungTam,
      tuNgay: req.body.tuNgay, denNgay: req.body.denNgay, Lang: ''
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}
exports.getSoNgayNghi = async (req, res) => {
  var json = {
    procedure: 'Portal_PhepNam_GetSoNgayNghiResult',
    data: {
      id_canBo: req.canBoId, ID_TrungTam: req.id_trungTam,
      thang: req.body.thang, nam: req.body.nam, 
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}
exports.getSoNgayNghi = async (req, res) => {
  var json = {
    procedure: 'Portal_PhepNam_GetSoNgayNghiResult',
    data: {
      id_canBo: req.canBoId, ID_TrungTam: req.id_trungTam,
      thang: req.body.thang, nam: req.body.nam, 
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}

exports.getSoNgayDuocPhepNghi = async (req, res) => {
  var json = {
    procedure: 'TL_NgayNghiTrongNam_GetByThangNam',
    data: {
      ID_TrungTam: req.id_trungTam,
      tuNgay: req.body.tuNgay, denNgay: req.body.denNgay, 
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}

