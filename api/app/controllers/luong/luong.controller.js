const Common = require('../../models/common_mssql.model');


exports.getLuongCoBan = async (req, res) => {
  var json = {
    procedure: 'CanBo_BienDongLuongCung_GetByIdCanBo',
    data: {
      ID_CanBo: req.canBoId, id_trungTam: req.id_trungTam
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}
exports.getLuongThoaThuan = async (req, res) => {
  var json = {
    procedure: 'CanBo_BienDongLuongChucDanh_GetByIdCanBo',
    data: {
      IDcanBo: req.canBoId, id_trungTam: req.id_trungTam
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}
exports.getHoTro = async (req, res) => {
  var json = {
    procedure: 'CanBo_BienDongPhuCap_GetBy_ID_CanBo_ID_LoaiPhuCap',
    data: {
      ID_CanBo: req.canBoId, Id_loaiPhuCap: 0, id_trungTam: req.id_trungTam
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}
