const Common = require('../../models/common_mssql.model');


exports.getVanBanQuyChe = async (req, res) => {
  var json = {
    procedure: 'Portal_NoiQuyCoQuan_GetByID_ChucDanh',
    data: {
      id_ChucDanh: req.body.id_chucDanh, id_donVi: req.body.id_donVi,
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}
