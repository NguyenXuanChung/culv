const Common = require('../../models/common_mssql.model');
const tableName = "CC_ThoiGian";

const ThoiGian = function () { };

exports.TachCaTuDong = (req, res) => {
  res.send(ThoiGian.TachCaTuDong(req.id_trungTam, req.body.TuNgay, req.body.DenNgay,
    req.body.ID_CanBo, req.body.ID_DonVis).then());
}

ThoiGian.TachCaTuDong = async (id_trungTam, TuNgay, DenNgay, ID_CanBo, ID_DonVis) => {
  var json = {
    procedure: 'CC_ThoiGian_GetDuLieuTachCa',
    data: {
      ID_TrungTam: id_trungTam, TuNgay: TuNgay, DenNgay: DenNgay,
      ID_CanBo: ID_CanBo, ID_DonVis: ID_DonVis
    },
  };

  await Common.callProcedure(json, async (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        return { error: { message: "Not found data." } };
      } else {
        return { error: { ...err, message: err.message } };
      }
    } else {
      data.forEach(async element => {
        await ThoiGian.TachCa(element);
      });

      return { data: { item: data.length }, message: "Execute successfully!" };
    }
  });
}

ThoiGian.TachCa = async (item) => {
  var soPhutLamViec = 0;
  if (item.SoLanChamCong === 2) {
    // Nếu chấm công 2 lần
    if (!item.GioVao1 || !item.GioRa1) {
      soPhutLamViec = 0;
    } else if (item.GioVao1 <= item.GioVaoCa && item.GioRa1 >= item.GioRaCa) {
      soPhutLamViec = item.SoGioCa * 60;
    }
    else if (item.NghiGiuaCaTu) {
      // Tính khoảng trước giờ nghỉ giữa ca
      if (item.GioVao1 <= item.GioVaoCa) {
        if (item.GioRa1 > item.NghiGiuaCaTu) {
          soPhutLamViec += Math.ceil((item.NghiGiuaCaTu.getTime() - item.GioVaoCa.getTime()) / 60000.0);
        }
        else {
          soPhutLamViec += Math.ceil((item.GioRa1.getTime() - item.GioVaoCa.getTime()) / 60000.0);
        }
      } else if (item.GioVao1 > item.GioVaoCa && item.GioVao1 < item.NghiGiuaCaTu) {
        if (item.GioRa1 > item.NghiGiuaCaTu) {
          soPhutLamViec += Math.ceil((item.NghiGiuaCaTu.getTime() - item.GioVao1.getTime()) / 60000.0);
        }
        else {
          soPhutLamViec += Math.ceil((item.GioRa1.getTime() - item.GioVao1.getTime()) / 60000.0);
        }
      }
      // Tính khoảng sau giờ nghỉ giữa ca
      if (item.GioRa1 >= item.GioRaCa) {
        if (item.GioVao1 <= item.NghiGiuaCaDen) {
          soPhutLamViec += Math.ceil((item.GioRaCa.getTime() - item.NghiGiuaCaDen.getTime()) / 60000.0);
        } else {
          soPhutLamViec += Math.ceil((item.GioRaCa.getTime() - item.GioVao1.getTime()) / 60000.0);
        }
      } else if (item.GioRa1 < item.GioRaCa && item.GioRa1 > item.NghiGiuaCaDen) {
        if (item.GioVao1 <= item.NghiGiuaCaDen) {
          soPhutLamViec += Math.ceil((item.GioRa1.getTime() - item.NghiGiuaCaDen.getTime()) / 60000.0);
        } else {
          soPhutLamViec += Math.ceil((item.GioRa1.getTime() - item.GioVao1.getTime()) / 60000.0);
        }
      }
    } else {
      soPhutLamViec += Math.ceil((item.GioRa1.getTime() - item.GioVao1.getTime()) / 60000.0);
    }
  } else {
    // Nếu chấm công 4 lần
    // Tính 2 lần chấm đầu
    if (item.GioVao1 && item.GioRa1) {
      if (item.GioVao1 <= item.GioVaoCa) {
        if (item.GioRa1 > item.NghiGiuaCaTu) {
          soPhutLamViec += Math.ceil((item.NghiGiuaCaTu.getTime() - item.GioVaoCa.getTime()) / 60000.0);
        }
        else {
          soPhutLamViec += Math.ceil((item.GioRa1.getTime() - item.GioVaoCa.getTime()) / 60000.0);
        }
      } else if (item.GioVao1 > item.GioVaoCa && item.GioVao1 < item.NghiGiuaCaTu) {
        if (item.GioRa1 > item.NghiGiuaCaTu) {
          soPhutLamViec += Math.ceil((item.NghiGiuaCaTu.getTime() - item.GioVao1.getTime()) / 60000.0);
        }
        else {
          soPhutLamViec += Math.ceil((item.GioRa1.getTime() - item.GioVao1.getTime()) / 60000.0);
        }
      }
    }
    // Tính 2 lần chấm sau
    if (item.GioVao2 && item.GioRa2) {
      if (item.GioRa2 >= item.GioRaCa) {
        if (item.GioVao2 <= item.NghiGiuaCaDen) {
          soPhutLamViec += Math.ceil((item.GioRaCa.getTime() - item.NghiGiuaCaDen.getTime()) / 60000.0);
        } else {
          soPhutLamViec += Math.ceil((item.GioRaCa.getTime() - item.GioVao2.getTime()) / 60000.0);
        }
      } else if (item.GioRa2 < item.GioRaCa && item.GioRa2 > item.NghiGiuaCaDen) {
        if (item.GioVao2 <= item.NghiGiuaCaDen) {
          soPhutLamViec += Math.ceil((item.GioRa2.getTime() - item.NghiGiuaCaDen.getTime()) / 60000.0);
        } else {
          soPhutLamViec += Math.ceil((item.GioRa2.getTime() - item.GioVao2.getTime()) / 60000.0);
        }
      }
    }
  }

  var soGioLamViec = 0;
  var soPhutNghiCheDo = item.DiMuonCheDo + item.VeSomCheDo;
  if (item.ToanTuMotCong == '>=' && soPhutLamViec >= item.SoPhutTinhMotCong - soPhutNghiCheDo) {
    soGioLamViec = 8;
  } else if (item.ToanTuMotCong == '>' && soPhutLamViec > item.SoPhutTinhMotCong - soPhutNghiCheDo) {
    soGioLamViec = 8;
  } else if (item.ToanTuNuaCong == '>=' && soPhutLamViec >= item.SoPhutTinhNuaCong - soPhutNghiCheDo) {
    soGioLamViec = 4;
  } else if (item.ToanTuNuaCong == '>' && soPhutLamViec > item.SoPhutTinhNuaCong - soPhutNghiCheDo) {
    soGioLamViec = 4;
  }

  json = {
    procedure: 'CC_ThoiGian_UpdateTachCa',
    data: {
      ID_CC_ThoiGian: item.CC_ThoiGian_ID, ID_CanBo: item.ID_CanBo, Ngay: item.Ngay,
      LamViecQuaDem: item.LamViecQuaDem, ID_MuiGio_Ca: item.ID_CaDangKy, SoGio: soGioLamViec
    },
  };

  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        return "Not found data.";
      } else {
        return err.message;
      }
    } else {
      return data[0].ErrorMessage;
    }
  });
}

exports.GetDuLieuChamMay = async (req, res) => {
  var json = {
    procedure: 'CC_ThoiGian_GetDuLieuChamMay',
    data: {
      ID_CanBo: req.body.ID_CanBo, TuNgay: req.body.TuNgay, DenNgay: req.body.DenNgay
    },
  };

  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}

ThoiGian.XoaDuLieuTachCa = async (id_trungTam, ID_CanBo, TuNgay, DenNgay) => {
  var json = {
    procedure: 'CC_ThoiGian_XoaDuLieuTachCa',
    data: {
      ID_TrungTam: id_trungTam, ID_CanBo: ID_CanBo, TuNgay: TuNgay, DenNgay: DenNgay
    },
  };

  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        return { error: { message: "Not found data." } };
      } else {
        return { error: { ...err, message: err.message } };
      }
    } else {
      return { data: data, message: "Delete successfully!" };
    }
  });
}

exports.XoaDuLieuTachCa = (req, res) => {
  res.send(ThoiGian.XoaDuLieuTachCa(req.id_trungTam, req.body.ID_CanBo, req.body.TuNgay, req.body.DenNgay).then());
}

ThoiGian.XoaChamCongKyHieu = async (ID_CC_DangKyNghiLamViec, ID_Portal_TicketDetail) => {
  var json = {
    procedure: 'Portal_Delete_CC_ChamCongKyHieu',
    data: {
      ID_Portal_TicketDetail: ID_Portal_TicketDetail
    },
  };

  if (ID_CC_DangKyNghiLamViec) {
    json = {
      procedure: 'CC_DangKyNghiLamViec_DeleteChamCongKyHieu',
      data: {
        ID_CC_DangKyNghiLamViec: ID_CC_DangKyNghiLamViec
      },
    };
  }

  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        return { error: { message: "Not found data." } };
      } else {
        return { error: { ...err, message: err.message } };
      }
    } else {
      return { data: data, message: "Delete successfully!" };
    }
  });
}

exports.DuyetDangKyNghiVaoNghiepVu = async (req, res) => {
  var json = {
    procedure: 'CC_DangKyNghiLamViec_GetDuyetTaoKyHieu',
    data: {
      ID_TrungTam: req.id_trungTam,
      ID_CC_DangKyNghiLamViec: req.body.ID_CC_DangKyNghiLamViec,
      ID_Portal_TicketDetail: req.body.ID_Portal_TicketDetail
    },
  };

  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      data.forEach(async (element) => {
        element.CategoryItem = JSON.parse(element.CategoryItem);
        element.SanXuat = JSON.parse(element.SanXuat);
        element.DangKyCa = JSON.parse(element.DangKyCa);
        element.CaMacDinh = JSON.parse(element.CaMacDinh);
        element.NgayNghi = JSON.parse(element.NgayNghi);
        element.NgayBu = JSON.parse(element.NgayBu);
        element.LamViecThu7 = JSON.parse(element.LamViecThu7);

        if (element.TableReference === 'CC_ChamCongKyHieu') {
          if (element.MaLoaiDangKy === 'PHEP') {
            // ThoiGian.TaoChamCongKyHieuNghiPhep(req.id_trungTam, element).then(() => {
            //   // Thực hiện tách lại ca
            // });
          } else {
            if (element.CategoryItem.length > 0) {
              await ThoiGian.TaoChamCongKyHieu(req.id_trungTam, element).then(async () => {
                // Thực hiện tách lại ca
                await ThoiGian.XoaDuLieuTachCa(req.id_trungTam, element.ID_CanBo,
                  element.FromDate, element.ToDate).then(async () => {
                    await ThoiGian.TachCaTuDong(req.id_trungTam,
                      element.FromDate, element.ToDate, element.ID_CanBo, '').then();
                  })
              });
            }
          }
        } else if (element.TableReference === 'GiaiTrinhChamCong') {
          await ThoiGian.TaoGiaiTrinhChamCong(req.id_trungTam, element).then(async () => {
            // Thực hiện tách lại ca
            await ThoiGian.XoaDuLieuTachCa(req.id_trungTam, element.ID_CanBo,
              element.FromDate, element.ToDate).then(async () => {
                await ThoiGian.TachCaTuDong(req.id_trungTam,
                  element.FromDate, element.ToDate, element.ID_CanBo, '').then();
              })
          });
        }
      });
      res.send({ data: data, message: "Execute successfully!" });
    }
  });
}

exports.DuyetHuyDangKyNghiVaoNghiepVu = async (req, res) => {
  var json = {
    procedure: 'CC_DangKyNghiLamViec_GetDuyetHuyTaoKyHieu',
    data: {
      ID_TrungTam: req.id_trungTam,
      ID_CC_DangKyNghiLamViec: req.body.ID_CC_DangKyNghiLamViec,
      ID_Portal_TicketDetail: req.body.ID_Portal_TicketDetail
    },
  };

  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      data.forEach(async (element) => {
        if (element.TableReference === 'CC_ChamCongKyHieu') {
          await ThoiGian.XoaChamCongKyHieu(req.body.ID_CC_DangKyNghiLamViec,
            req.body.ID_Portal_TicketDetail).then(async () => {
              // Thực hiện tách lại ca
              await ThoiGian.XoaDuLieuTachCa(req.id_trungTam, element.ID_CanBo,
                element.FromDate, element.ToDate).then(async () => {
                  await ThoiGian.TachCaTuDong(req.id_trungTam,
                    element.FromDate, element.ToDate, element.ID_CanBo, '').then();
                })
            });
        } else if (element.TableReference === 'GiaiTrinhChamCong') {
          await ThoiGian.HuyGiaiTrinhChamCong(req.id_trungTam, element).then(async () => {
            // Thực hiện tách lại ca
            await ThoiGian.XoaDuLieuTachCa(req.id_trungTam, element.ID_CanBo,
              element.FromDate, element.ToDate).then(async () => {
                await ThoiGian.TachCaTuDong(req.id_trungTam,
                  element.FromDate, element.ToDate, element.ID_CanBo, '').then();
              })
          });
        }
      });
      res.send({ data: data, message: "Execute successfully!" });
    }
  });
}

ThoiGian.TaoChamCongKyHieuNghiPhep = (id_trungTam, item) => {

}

ThoiGian.TaoChamCongKyHieu = async (id_trungTam, item) => {
  var json = {
    procedure: 'CC_ThoiGian_TaoChamCongKyHieu',
    data: {
      ID_TrungTam: id_trungTam,
      ID_CanBo: item.ID_CanBo
    }
  }

  if (item.CC_DangKyNghiLamViec_ID) {
    json.data.ID_CC_DangKyNghiLamViec = item.CC_DangKyNghiLamViec_ID;
    json.data.ID_Portal_TicketDetail = null;
  } else if (item.Portal_TicketDetail_ID) {
    json.data.ID_Portal_TicketDetail = item.Portal_TicketDetail_ID;
    json.data.ID_CC_DangKyNghiLamViec = null;
  }

  var ngay = new Date(item.FromDate), denNgay = new Date(item.ToDate);
  var fromDate = new Date(item.FromDate);
  var days = 1, ca = {}, quyDinhThu7 = [];
  while (ngay <= denNgay) {
    if (ngay.getDay() === 0 ||
      item.NgayNghi.filter((x) => new Date(x.TuNgay) <= ngay && new Date(x.DnNgay) >= ngay).length > 0) {
      // Nếu là ngày nghỉ, chủ nhật thì xét nhân viên có làm sản xuất hay không
      if (!item.SanXuat.filter((x) => new Date(x.TuNgay) <= ngay && new Date(x.DnNgay) >= ngay).length > 0) {
        continue;
      }
    } else if (ngay.getDay() === 6) {
      // Nếu là thứ 7 thì xét quy tắc làm việc thứ 7 của nhân viên
      quyDinhThu7 = item.LamViecThu7.filter((x) => new Date(x.Ngay) === ngay);
      if (quyDinhThu7.length === 0) {
        continue;
      }
    }

    days = 1;
    ca = {};
    // Xác định ca làm việc theo ngày
    if (item.DangKyCa.length > 0) {
      var lst = item.DangKyCa.filter((i) => new Date(i.TuNgay) <= ngay && ngay <= new Date(i.DenNgay));
      if (lst.length > 0) {
        ca = lst[0];
      } else {
        ca = null;
      }
    }
    if (ca === null && item.CaMacDinh.length > 0) {
      var lst = item.CaMacDinh.filter((i) => new Date(i.NgayApDung) <= ngay);
      if (lst.length > 0) {
        ca = lst[0];
      } else {
        ca = null;
      }
    }
    if (!ca) {
      continue;
    }

    // Mặc định là nghỉ 1 ngày
    days = 1;
    // console.log(ngay, item, ca);
    if (Common.dateToText(ngay) == Common.dateToText(fromDate) && item.FromTime == ca.NghiGiuaCaDen) {
      // Nếu ngày đang xét bằng từ ngày và thời điểm bắt đầu là sau giờ nghỉ giữa ca thì trừ đi 0.5 ngày
      days = 0.5;
      json.data.TuGio = item.FromTime;
    } else if (Common.dateToText(ngay) == Common.dateToText(denNgay) && item.ToTime == ca.NghiGiuaCaTu) {
      // Nếu ngày đang xét bằng đến ngày và thời điểm kết thúc là giờ nghỉ giữa ca thì trừ đi 0.5 ngày
      days = 0.5;
      json.data.DenGio = item.ToTime;
    }

    // Nếu ngày đang xét khác ngày bắt đầu thì từ giờ là giờ đầu ca
    if (Common.dateToText(ngay) != Common.dateToText(fromDate)) {
      json.data.TuGio = ca.GioVao;
    }

    // Nếu là ngày thứ 7 và có quy định làm sáng thứ 7 tính 1 công thì sẽ tính là 1 công
    if (ngay.getDay() === 6 && days === 0.5 && quyDinhThu7 && quyDinhThu7[0].LamBuoiSang && quyDinhThu7[0].Tinh1Cong) {
      days = 1;
      json.data.TuGio = ca.FromTime;
      json.data.DenGio = ca.NghiGiuaCaDen;
    }

    // Xác định ký hiệu dựa trên số ngày công trừ trong ngày
    var objKyHieu = item.CategoryItem.filter((x) => x.SoNgayTru === days);

    if (objKyHieu.length > 0) {
      // Thực hiện thêm dữ liệu vào bảng chấm công ký hiệu
      json.data.Ngay = ngay;
      json.data.ID_KyHieuChamCong = objKyHieu[0].KyHieuChamCong_ID;
      if (!json.data.TuGio) {
        json.data.TuGio = ca.GioVao;
      }
      if (!json.data.DenGio) {
        json.data.DenGio = ca.GioRa;
      }

      await Common.callProcedure(json, async (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            console.log({ error: { message: "Not found data.", data: json.data } });
            // return { error: { message: "Not found data.", data: json.data } };
          } else {
            console.log({ error: { ...err, message: err.message, data: json.data } });
            // return { error: { ...err, message: err.message, data: json.data } };
          }
        } else {
          // console.log({ data: json.data, message: "Execute successfully!" });
          // return { data: json.data, message: "Execute successfully!" };
        }
      });
    }

    ngay.setDate(ngay.getDate() + 1);
  }
}

ThoiGian.TaoGiaiTrinhChamCong = async (id_trungTam, item) => {
  var json = {
    procedure: 'CC_ThoiGian_UpdateGiaiTrinh',
    data: {
      ID_TrungTam: id_trungTam,
      ID_CanBo: item.ID_CanBo,
      Ngay: item.FromDate,
      Code: item.MaLoaiDangKy
    }
  }

  var ca = {};

  if (item.DangKyCa.length > 0) {
    ca = item.DangKyCa[0];
  } else if (item.CaMacDinh.length > 0) {
    ca = item.CaMacDinh;
  } else {
    ca = null;
  }

  if (ca) {
    if (ca.SoLanChamCong === 2) {
      if (item.MaLoaiDangKy === 'GIAI_TRINH_DI_MUON') {
        json.data.GioGiaiTrinh = ca.GioVao;
      } else if (item.MaLoaiDangKy === 'GIAI_TRINH_VE_SOM') {
        json.data.GioGiaiTrinh = ca.GioRa;
      }
    } else {
      if (item.MaLoaiDangKy === 'GIAI_TRINH_DI_MUON') {
        json.data.GioGiaiTrinh = ca.GioVao;
      } else if (item.MaLoaiDangKy === 'GIAI_TRINH_VE_SOM_GC') {
        json.data.GioGiaiTrinh = ca.NghiGiuaCaTu;
      } else if (item.MaLoaiDangKy === 'GIAI_TRINH_DI_MUON_GC') {
        json.data.GioGiaiTrinh = ca.NghiGiuaCaDen;
      } else if (item.MaLoaiDangKy === 'GIAI_TRINH_VE_SOM') {
        json.data.GioGiaiTrinh = ca.GioRa;
      }
    }

    json.data.ID_Ticket = item.Portal_TicketDetail_ID;
    await Common.callProcedure(json).then();
  }
}

ThoiGian.HuyGiaiTrinhChamCong = async (id_trungTam, item) => {
  var json = {
    procedure: 'CC_ThoiGian_UpdateGiaiTrinh',
    data: {
      ID_TrungTam: id_trungTam,
      ID_CanBo: item.ID_CanBo,
      Ngay: item.FromDate,
      Code: item.MaLoaiDangKy,
      GioGiaiTrinh: null,
      ID_Ticket: null
    }
  }

  await Common.callProcedure(json).then();
}
