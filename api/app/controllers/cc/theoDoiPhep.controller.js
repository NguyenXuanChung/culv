const Common = require('../../models/common_mssql.model');
const TheoDoiPhep = function () { };

exports.TongHopTheoThang = async (req, res) => {
    var json = {
        procedure: 'Canbo_NghiPhepNam_GetTongHopByThang_TheoTrungTam',
        data: {
            ID_TrungTam: req.id_trungTam,
            ID_DonVi: req.body.id_donVi,
            ID_LoaiHopDongs: req.body.id_loaiHopDongs,
            Thang: req.body.thang,
            Nam: req.body.nam,
            MaHoacHoTen: req.body.maHoacHoTen,
            DsNghiViec: req.body.dsNghiViec
        },
    };
    await Common.callProcedure(json, (err, data) => {
        if (err) {
            if (err.kind == "not_found") {
                res.send({ error: { message: "Not found data." } });
            } else {
                res.send({ error: { ...err, message: err.message } });
            }
        } else {
            data.forEach(async (element) => {
                element.Thang_GiaTri = JSON.parse(element.Thang_GiaTri);
                element.NgayVaoTinhPhep = element.NgayVaoTinhPhep ? Common.dateToText(element.NgayVaoTinhPhep) : null
                if (element.QuyetToanPhepTheoThang) {
                    element.SoPhepDuocHuongTrongNam = element.SoPhepDuocNghiDenThangChon - element.SoPhepDuocNghiDenThangTruoc
                    element.SoPhepThucTeDuocHuong = element.SoPhepDuocHuongTrongNam
                    element.SoPhepCon = element.SoPhepDuocHuongTrongNam - element.SoPhepDaNghi
                } else {
                    element.SoPhepDuocHuongTrongNam = element.SoPhepDuocNghiDenThangChon
                    element.SoPhepThucTeDuocHuong = element.SoPhepDuocHuongTrongNam + element.SoPhepDuocUng;
                }
                for (let i = 1; i <= 12; i++) {
                    element['T' + `${i}`] = 0;
                }

            });
            // Update giá trị
            data.forEach(async (element) => {
                for (let i = 1; i <= 12; i++) {
                    if (element.Thang_GiaTri) {
                        element.Thang_GiaTri.forEach((e) => {
                            if (e.Thang === i) {
                                element['T' + `${i}`] = e.GiaTri
                            }
                        })
                    }
                }
            });

            res.send({ data: data, message: "Execute successfully!" });
        }
    });
};

