const Common = require('../../models/common_mssql.model');


exports.getList = async (req, res) => {
  var json = {
    procedure: 'DSCanBo_TimTenCanBo_And_ID_LoaiHopDong',
    data: {
      ID_DonVi: ',' + req.body.id_donVis, TenCanBo: req.body.tenCanBo, Account: 0,
      ngayHienHanh: Common.now(), canBoDaXoa: 0, hinhThucSuDungLaoDong: ',CHINH_THUC,',
      id_loaiHopDongs: ''
    },
  };
  await Common.callProcedure(json, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.send({ error: { message: "Not found data." } });
      } else {
        res.send({ error: { ...err, message: err.message } });
      }
    } else {
      res.send({ data: data, message: "Retrieve data successfully!" });
    }
  });
}
