module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const apiTest = require('../controllers/sys/apiTest.controller');
  
  //#region DM nghề nghiệp
	// Retrieve data with condition in body
  app.post('/dm/nghenghiep/get', VerifyToken, apiTest.get);

  // Create a new item
  app.post('/dm/nghenghiep/insert', VerifyToken, apiTest.create);
  
  // Update a item with id
	app.post('/dm/nghenghiep/update', VerifyToken, apiTest.update);

	// Delete a item with id
	app.post('/dm/nghenghiep/delete', VerifyToken, apiTest.delete);
  //#endregion
}