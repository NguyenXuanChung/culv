module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const quyTrinh = require('../controllers/qt/quyTrinh.controller');
  
  //#region quyTrinh
  app.post('/qt/get', VerifyToken, quyTrinh.get);
  app.post('/qt/dexuat/getlist', VerifyToken, quyTrinh.getListDeXuat);
  app.post('/qt/dexuat/getdetail', VerifyToken, quyTrinh.getDetailDeXuat);
  app.post('/qt/dexuat/get', VerifyToken, quyTrinh.getDeXuat);
  app.post('/qt/dexuat/insert', VerifyToken, quyTrinh.createDeXuat);
  app.post('/qt/dexuat/update', VerifyToken, quyTrinh.updateDeXuat);
  app.post('/qt/dexuat/delete', VerifyToken, quyTrinh.deleteDeXuat);

  app.post('/qt/activity/get', VerifyToken, quyTrinh.getActivity);
  app.post('/qt/activity/insert', VerifyToken, quyTrinh.createActivity);
  app.post('/qt/activity/update', VerifyToken, quyTrinh.updateActivity);
  app.post('/qt/activity/delete', VerifyToken, quyTrinh.deleteActivity);
  //#endregion
}