module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const luong = require('../controllers/luong/luong.controller');
  
  //#region Luong
  app.post('/luong/luongcoban/get', VerifyToken, luong.getLuongCoBan);
  app.post('/luong/luongthoathuan/get', VerifyToken, luong.getLuongThoaThuan);
  app.post('/luong/hotro/get', VerifyToken, luong.getHoTro);
  //#endregion
}