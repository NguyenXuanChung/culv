module.exports = app => {
    const VerifyToken = require('../controllers/VerifyToken');
    const tdvdxbonhiem = require('../controllers/bonhiem/tdvdexuatbonhiem.controller');
    
    //#region TDV de xuat bo nhiem
    app.post('bonhiem/tdvdxbonhiem/get', VerifyToken, tdvdxbonhiem.get);
    app.post('bonhiem/tdvdxbonhiem/insert', VerifyToken, tdvdxbonhiem.create);
    app.post('bonhiem/tdvdxbonhiem/update', VerifyToken, tdvdxbonhiem.update);
    app.post('bonhiem/tdvdxbonhiem/delete', VerifyToken, tdvdxbonhiem.delete);
  }