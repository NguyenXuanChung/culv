module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const info = require('../controllers/info/info.controller');
  
  //#region Info
  app.post('/info/giamtrugiacanh', VerifyToken, info.getGiamTruGiaCanh);
  app.post('/info/khenthuongkyluat', VerifyToken, info.getKhenThuongKyLuat);
  app.post('/info/quatrinhcongtac', VerifyToken, info.getQuaTrinhCongTac);
  app.post('/info/quatrinhhopdong', VerifyToken, info.getQuaTrinhHopDong);
  //#endregion
}