module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const xaHoi = require('../controllers/ttbh/xaHoi.controller');
  const nhanTho = require('../controllers/ttbh/nhanTho.controller');
  const healthCare = require('../controllers/ttbh/healthCare.controller');
  
  //#region TTBH
  app.post('/ttbh/xahoi/getbhxh', VerifyToken, xaHoi.getTrangThaiBhxh);
  app.post('/ttbh/xahoi/getquatrinhbhxh', VerifyToken, xaHoi.getQuaTrinhBhxh);
  app.post('/ttbh/xahoi/getquatrinhbhyt', VerifyToken, xaHoi.getQuaTrinhBhyt);
  app.post('/ttbh/nhantho/get', VerifyToken, nhanTho.getBhnt);
  app.post('/ttbh/nhantho/getquatrinh', VerifyToken, nhanTho.getQuaTrinhBhnt);
  app.post('/ttbh/healthcare/get', VerifyToken, healthCare.getBhhc);
  app.post('/ttbh/healthcare/getquatrinh', VerifyToken, healthCare.getQuaTrinhBhhc);
  //#endregion
}