module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const workflow = require('../controllers/sys/workflow.controller');
  
  //#region VanBan
  app.post('/sys/workflow/get', VerifyToken, workflow.getWorkflow);
  app.post('/sys/workflow/get-state', VerifyToken, workflow.getWorkflowState);
  app.post('/sys/workflow/get-state-action', VerifyToken, workflow.getWorkflowStateAction);
  //#endregion
}