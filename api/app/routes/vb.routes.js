module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const vanBanQuyChe = require('../controllers/vb/vanbanquyche.controller');
  
  //#region VanBan
  app.post('/vb/vanbanquyche/get', VerifyToken, vanBanQuyChe.getVanBanQuyChe);
  //#endregion
}