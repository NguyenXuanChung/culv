module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const thoiGian = require('../controllers/cc/thoiGian.controller');

  //#region CC_ThoiGian
  // Tách ca tự động
  app.post('/cc/thoigian/tachcatudong', VerifyToken, thoiGian.TachCaTuDong);

  // Phân tích dữ liệu chấm máy thành chấm công thời gian
  app.post('/cc/thoigian/getdulieuchammay', VerifyToken, thoiGian.GetDuLieuChamMay);

  // Xóa dữ liệu đã tách ca để thực hiện tách lại ca
  app.post('/cc/thoigian/xoadulieutachca', VerifyToken, thoiGian.XoaDuLieuTachCa);

  // Thực hiện duyệt đăng ký nghỉ vào nghiệp vụ quản lý
  app.post('/cc/thoigian/duyetdangkyvaonghiepvu', VerifyToken, thoiGian.DuyetDangKyNghiVaoNghiepVu);

  // Thực hiện duyệt hủy đăng ký nghỉ vào nghiệp vụ quản lý
  app.post('/cc/thoigian/duyethuydangkyvaonghiepvu', VerifyToken, thoiGian.DuyetHuyDangKyNghiVaoNghiepVu);
  //#endregion
}