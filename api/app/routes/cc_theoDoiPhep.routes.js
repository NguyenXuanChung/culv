module.exports = app => {
    const VerifyToken = require('../controllers/VerifyToken');
    const theoDoiPhep = require('../controllers/cc/theoDoiPhep.controller');

    //#region Tổng hợp phép
    // Tổng hợp phép theo tháng
    app.post('/cc/theodoiphep/tonghoptheothang', VerifyToken, theoDoiPhep.TongHopTheoThang);

    //#endregion
}