module.exports = app => {
  const auth = require("../controllers/sys/auth.controller");
  const authPortal = require("../controllers/sys/auth_portal.controller");
  const VerifyToken = require("../controllers/VerifyToken");
  
  // Generate toke key with username and password in request body
  app.post("/auth/token", auth.login);
  app.post("/auth-portal/token", authPortal.login);

  // Get user info with token key
  app.get("/auth/getUserInfo", VerifyToken, auth.getUserInfo);
  app.get("/auth-portal/getUserInfo", VerifyToken, authPortal.getUserInfo);
};

