module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const quocGia = require('../controllers/dm/dm_quocGia.controller');
  const loaiHopDong = require('../controllers/dm/dm_loaiHopDong.controller');
  const hinhThucSuDungLaoDong = require('../controllers/dm/dm_hinhThucSuDungLaoDong.controller');
  const chucDanh = require('../controllers/dm/dm_chucDanh.controller');
  const loaiDeXuat = require('../controllers/dm/dm_loaiDeXuat.controller');

  //#region quocGia
  app.post('/dm/quocgia/get', VerifyToken, quocGia.get);
  //#endregion
  //#region loaihopdong
  app.post('/dm/dm-loaihopdong/get', VerifyToken, loaiHopDong.get);
  app.post('/dm/dm-loaidexuat/get', VerifyToken, loaiDeXuat.get);
  app.post('/dm/dm-chucdanh/get', VerifyToken, chucDanh.get);
  //#endregion
//#region hinhthucsudunglaodong
app.post('/dm/hinhthucsudunglaodong/get', VerifyToken, hinhThucSuDungLaoDong.get);
//#endregion
}