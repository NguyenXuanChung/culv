module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const canBo = require('../controllers/cb/canBo.controller');
  
  //#region VanBan
  app.post('/cb/canbo/get-list', VerifyToken, canBo.getList);
  //#endregion
}